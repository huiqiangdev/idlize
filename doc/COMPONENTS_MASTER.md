# All components


| Status       | Components | Classes | Functions |
| -----------  | ---------- | ------- | --------- |
| Total        | 121      | 151     | 2265     |
| In Progress  | 17      | 5     | 34     |
| Done         | 56      | 38     | 1415     |
| Blocked      | 36      | 51     | 268     |


| Name | Kind | Owner | Status |
| ---- | ---- | ----- | ------ |
| *AbilityComponent* | *Component* | Tuzhilkin Ivan | deprecated |
| `onConnect` | Function | Tuzhilkin Ivan | deprecated |
| `onDisconnect` | Function | Tuzhilkin Ivan | deprecated |
| *AbstractProperty* | *Class* | | |
| `get` | Function | | |
| `info` | Function | | |
| `set` | Function | | |
| *ActionSheet* | *Class* | Ekaterina Stepanova | blocked IDL |
| `show` | Function | Ekaterina Stepanova | blocked IDL |
| *AlertDialog* | *Class* | Ekaterina Stepanova | blocked IDL |
| `show` | Function | Ekaterina Stepanova | blocked IDL |
| *AlphabetIndexer* | *Component* | Ekaterina Stepanova | done |
| `alignStyle` | Function | Ekaterina Stepanova | done |
| `autoCollapse` | Function | Ekaterina Stepanova | done |
| `color` | Function | Ekaterina Stepanova | done |
| `enableHapticFeedback` | Function | Ekaterina Stepanova | done |
| `font` | Function | Ekaterina Stepanova | done |
| `itemBorderRadius` | Function | Ekaterina Stepanova | done |
| `itemSize` | Function | Ekaterina Stepanova | done |
| `onPopupSelect` | Function | Ekaterina Stepanova | done |
| `onRequestPopupData` | Function | Skroba Gleb | done |
| `onSelect` | Function | Ekaterina Stepanova | done |
| `onSelected` | Function | Ekaterina Stepanova | done |
| `popupBackground` | Function | Ekaterina Stepanova | done |
| `popupBackgroundBlurStyle` | Function | Ekaterina Stepanova | done |
| `popupColor` | Function | Ekaterina Stepanova | done |
| `popupFont` | Function | Ekaterina Stepanova | done |
| `popupItemBackgroundColor` | Function | Ekaterina Stepanova | done |
| `popupItemBorderRadius` | Function | Ekaterina Stepanova | done |
| `popupItemFont` | Function | Ekaterina Stepanova | done |
| `popupPosition` | Function | Ekaterina Stepanova | done |
| `popupSelectedColor` | Function | Ekaterina Stepanova | done |
| `popupTitleBackground` | Function | Ekaterina Stepanova | done |
| `popupUnselectedColor` | Function | Ekaterina Stepanova | done |
| `selected` | Function | Ekaterina Stepanova | done |
| `selectedBackgroundColor` | Function | Ekaterina Stepanova | done |
| `selectedColor` | Function | Ekaterina Stepanova | done |
| `selectedFont` | Function | Ekaterina Stepanova | done |
| `usingPopup` | Function | Ekaterina Stepanova | done |
| *AnimatableArithmetic* | *Class* | | |
| `equals` | Function | | |
| `multiply` | Function | | |
| `plus` | Function | | |
| `subtract` | Function | | |
| *AnimationExtender* | *Class* | | |
| `CloseImplicitAnimation` | Function | | |
| `OpenImplicitAnimation` | Function | | |
| `SetClipRect` | Function | | |
| `StartDoubleAnimation` | Function | | |
| *Animator* | *Component* | Skroba Gleb | blocked |
| `curve` | Function | Skroba Gleb | blocked IDL |
| `delay` | Function | Skroba Gleb | blocked IDL |
| `duration` | Function | Skroba Gleb | blocked IDL |
| `fillMode` | Function | Skroba Gleb | blocked IDL |
| `iterations` | Function | Skroba Gleb | blocked IDL |
| `motion` | Function | Skroba Gleb | blocked IDL |
| `onCancel` | Function | Skroba Gleb | blocked IDL |
| `onFinish` | Function | Skroba Gleb | blocked IDL |
| `onFrame` | Function | Skroba Gleb | blocked IDL |
| `onPause` | Function | Skroba Gleb | blocked IDL |
| `onRepeat` | Function | Skroba Gleb | blocked IDL |
| `onStart` | Function | Skroba Gleb | blocked IDL |
| `playMode` | Function | Skroba Gleb | blocked IDL |
| `state` | Function | Skroba Gleb | blocked IDL |
| *AppStorage* | *Class* | | |
| `Clear` | Function | | |
| `Delete` | Function | | |
| `Get` | Function | | |
| `Has` | Function | | |
| `IsMutable` | Function | | |
| `Keys` | Function | | |
| `Link` | Function | | |
| `Prop` | Function | | |
| `Set` | Function | | |
| `SetAndLink` | Function | | |
| `SetAndProp` | Function | | |
| `SetOrCreate` | Function | | |
| `Size` | Function | | |
| `clear` | Function | | |
| `delete` | Function | | |
| `get` | Function | | |
| `has` | Function | | |
| `keys` | Function | | |
| `link` | Function | | |
| `prop` | Function | | |
| `ref` | Function | | |
| `set` | Function | | |
| `setAndLink` | Function | | |
| `setAndProp` | Function | | |
| `setAndRef` | Function | | |
| `setOrCreate` | Function | | |
| `size` | Function | | |
| `staticClear` | Function | | |
| *AttributeModifier* | *Class* | | |
| `applyDisabledAttribute` | Function | | |
| `applyFocusedAttribute` | Function | | |
| `applyNormalAttribute` | Function | | |
| `applyPressedAttribute` | Function | | |
| `applySelectedAttribute` | Function | | |
| *Badge* | *Component* | Vadim Voronov | done |
| *BaseCustomComponent* | *Component* | | |
| `aboutToAppear` | Function | | |
| `aboutToDisappear` | Function | | |
| `aboutToRecycle` | Function | | |
| `build` | Function | | |
| `getDialogController` | Function | | |
| `getUIContext` | Function | | |
| `getUniqueId` | Function | | |
| `onBackPress` | Function | | |
| `onDidBuild` | Function | | |
| `onFormRecover` | Function | | |
| `onFormRecycle` | Function | | |
| `onMeasureSize` | Function | | |
| `onPageHide` | Function | | |
| `onPageShow` | Function | | |
| `onPlaceChildren` | Function | | |
| `onWillApplyTheme` | Function | | |
| `pageTransition` | Function | | |
| `queryNavDestinationInfo` | Function | | |
| `queryNavigationInfo` | Function | | |
| `queryRouterPageInfo` | Function | | |
| *BaseEvent* | *Class* | Politov Mikhail |  |
| `getModifierKeyState` | Function | Politov Mikhail |  |
| `getAxisHorizontal` | Function | Politov Mikhail |  |
| `setAxisHorizontal` | Function | Politov Mikhail |  |
| `getAxisVertical` | Function | Politov Mikhail |  |
| `setAxisVertical` | Function | Politov Mikhail |  |
| `getDeviceId` | Function | Politov Mikhail |  |
| `setDeviceId` | Function | Politov Mikhail |  |
| `getPressure` | Function | Politov Mikhail |  |
| `setPressure` | Function | Politov Mikhail |  |
| `getSource` | Function | Politov Mikhail |  |
| `setSource` | Function | Politov Mikhail |  |
| `getSourceTool` | Function | Politov Mikhail |  |
| `setSourceTool` | Function | Politov Mikhail |  |
| `getTarget` | Function | | |
| `setTarget` | Function | Politov Mikhail |  |
| `getTargetDisplayId` | Function | | |
| `setTargetDisplayId` | Function | | |
| `getTiltX` | Function | Politov Mikhail |  |
| `setTiltX` | Function | Politov Mikhail |  |
| `getTiltY` | Function | Politov Mikhail |  |
| `setTiltY` | Function | Politov Mikhail |  |
| `getTimestamp` | Function | Politov Mikhail |  |
| `setTimestamp` | Function | Politov Mikhail |  |
| *BaseSpan* | *Component* | Politov Mikhail | done |
| `baselineOffset` | Function | Politov Mikhail | done |
| `textBackgroundStyle` | Function | Politov Mikhail | done |
| *Blank* | *Component* | Skroba Gleb | done |
| `color` | Function | Skroba Gleb | done |
| *BottomTabBarStyle* | *Class* | | |
| `iconStyle` | Function | | |
| `id` | Function | | |
| `labelStyle` | Function | | |
| `layoutMode` | Function | | |
| `of` | Function | | |
| `padding` | Function | | |
| `symmetricExtensible` | Function | | |
| `verticalAlign` | Function | | |
| *Button* | *Component* | Evstigneev Roman | blocked |
| `buttonStyle` | Function | Evstigneev Roman | done |
| `contentModifier` | Function | Evstigneev Roman | blocked IDL |
| `controlSize` | Function | Evstigneev Roman | done |
| `fontColor` | Function | Evstigneev Roman | done |
| `fontFamily` | Function | Evstigneev Roman | done |
| `fontSize` | Function | Evstigneev Roman | done |
| `fontStyle` | Function | Evstigneev Roman | done |
| `fontWeight` | Function | Evstigneev Roman | done |
| `labelStyle` | Function | Evstigneev Roman | done |
| `maxFontScale` | Function |  |  |
| `minFontScale` | Function |  |  |
| `role` | Function | Evstigneev Roman | done |
| `stateEffect` | Function | Evstigneev Roman | done |
| `type` | Function | Evstigneev Roman | done |
| *Calendar* | *Component* | Maksimov Nikita | done |
| `currentDayStyle` | Function | Maksimov Nikita | done |
| `direction` | Function | Maksimov Nikita | done |
| `needSlide` | Function | Maksimov Nikita | done |
| `nonCurrentDayStyle` | Function | Maksimov Nikita | done |
| `offDays` | Function | Maksimov Nikita | done |
| `onRequestData` | Function | Maksimov Nikita | done |
| `onSelectChange` | Function | Maksimov Nikita | done |
| `showHoliday` | Function | Maksimov Nikita | done |
| `showLunar` | Function | Maksimov Nikita | done |
| `startOfWeek` | Function | Maksimov Nikita | done |
| `todayStyle` | Function | Maksimov Nikita | done |
| `weekStyle` | Function | Maksimov Nikita | done |
| `workStateStyle` | Function | Maksimov Nikita | done |
| *CalendarController* | *Class* | Maksimov Nikita | done |
| `backToToday` | Function | Maksimov Nikita | done |
| `goTo` | Function | Maksimov Nikita | done |
| *CalendarPicker* | *Component* | Politov Mikhail | done |
| `edgeAlign` | Function | Politov Mikhail | done |
| `markToday` | Function | | |
| `onChange` | Function | Politov Mikhail | done |
| `textStyle` | Function | Politov Mikhail | done |
| *CalendarPickerDialog* | *Class* | Ekaterina Stepanova | blocked IDL |
| `show` | Function | Ekaterina Stepanova | blocked IDL |
| *Canvas* | *Component* | Vadim Voronov, Evstigneev Roman |  |
| `enableAnalyzer` | Function | Vadim Voronov | done |
| `onReady` | Function | Vadim Voronov | done |
| *CanvasGradient* | *Class* | Vadim Voronov | done |
| `addColorStop` | Function | Vadim Voronov | done |
| *CanvasPath* | *Class* | Kovalev Sergey | done |
| `arc` | Function | Kovalev Sergey | done |
| `arcTo` | Function | Kovalev Sergey | done |
| `bezierCurveTo` | Function | Kovalev Sergey | done |
| `closePath` | Function | Kovalev Sergey | done |
| `ellipse` | Function | Kovalev Sergey | done |
| `lineTo` | Function | Kovalev Sergey | done |
| `moveTo` | Function | Kovalev Sergey | done |
| `quadraticCurveTo` | Function | Kovalev Sergey | done |
| `rect` | Function | Kovalev Sergey | done |
| *CanvasPattern* | *Class* | Andrey Khudenkikh | done |
| `setTransform` | Function | Andrey Khudenkikh | done |
| *CanvasRenderer* | *Class* | Vadim Voronov | in progress |
| `beginPath` | Function | Vadim Voronov | done |
| `clearRect` | Function | Vadim Voronov | done |
| `clip` | Function | Vadim Voronov | done |
| `createConicGradient` | Function | Vadim Voronov | done |
| `createImageData` | Function | Vadim Voronov | done |
| `createLinearGradient` | Function | Vadim Voronov | done |
| `createPattern` | Function | Vadim Voronov | done |
| `createRadialGradient` | Function | Vadim Voronov | done |
| `drawImage` | Function | Vadim Voronov | blocked IDL |
| `fill` | Function | Vadim Voronov | done |
| `fillRect` | Function | Vadim Voronov | done |
| `fillText` | Function | Vadim Voronov | done |
| `getImageData` | Function | Vadim Voronov | done |
| `getLineDash` | Function | Vadim Voronov | blocked IDL |
| `getPixelMap` | Function | Vadim Voronov | blocked AceEngine |
| `getTransform` | Function | Vadim Voronov | done |
| `measureText` | Function | Vadim Voronov | done |
| `putImageData` | Function | Vadim Voronov | done |
| `reset` | Function | Vadim Voronov | done |
| `resetTransform` | Function | Vadim Voronov | done |
| `restore` | Function | Vadim Voronov | done |
| `restoreLayer` | Function | Vadim Voronov | done |
| `rotate` | Function | Vadim Voronov | done |
| `save` | Function | Vadim Voronov | done |
| `saveLayer` | Function | Vadim Voronov | done |
| `scale` | Function | Vadim Voronov | done |
| `setLineDash` | Function | Vadim Voronov | done |
| `setPixelMap` | Function | Vadim Voronov | done |
| `setTransform` | Function | Vadim Voronov | done |
| `stroke` | Function | Vadim Voronov | done |
| `strokeRect` | Function | Vadim Voronov | done |
| `strokeText` | Function | Vadim Voronov | done |
| `transferFromImageBitmap` | Function | Vadim Voronov | done |
| `transform` | Function | Vadim Voronov | done |
| `translate` | Function | Vadim Voronov | done |
| `getDirection` | Function | Vadim Voronov | blocked IDL |
| `setDirection` | Function | Vadim Voronov | done |
| `getFillStyle` | Function | | |
| `setFillStyle` | Function | Vadim Voronov | done |
| `getFilter` | Function | Vadim Voronov | blocked IDL |
| `setFilter` | Function | Vadim Voronov | done |
| `getFont` | Function | Vadim Voronov | blocked IDL |
| `setFont` | Function | Vadim Voronov | done |
| `getGlobalAlpha` | Function | Vadim Voronov | blocked AceEngine |
| `setGlobalAlpha` | Function | Vadim Voronov | done |
| `getGlobalCompositeOperation` | Function | Vadim Voronov | blocked IDL |
| `setGlobalCompositeOperation` | Function | Vadim Voronov | done |
| `getImageSmoothingEnabled` | Function | Vadim Voronov | blocked AceEngine |
| `setImageSmoothingEnabled` | Function | Vadim Voronov | done |
| `getImageSmoothingQuality` | Function | Vadim Voronov | blocked IDL |
| `setImageSmoothingQuality` | Function | Vadim Voronov | done |
| `getLetterSpacing` | Function | | |
| `setLetterSpacing` | Function |  |  |
| `getLineCap` | Function | Vadim Voronov | blocked IDL |
| `setLineCap` | Function | Vadim Voronov | done |
| `getLineDashOffset` | Function | Vadim Voronov | done |
| `setLineDashOffset` | Function | Vadim Voronov | done |
| `getLineJoin` | Function | Vadim Voronov | blocked IDL |
| `setLineJoin` | Function | Vadim Voronov | done |
| `getLineWidth` | Function | Vadim Voronov | blocked AceEngine |
| `setLineWidth` | Function | Vadim Voronov | done |
| `getMiterLimit` | Function | Vadim Voronov | blocked AceEngine |
| `setMiterLimit` | Function | Vadim Voronov | done |
| `getShadowBlur` | Function | Vadim Voronov | blocked AceEngine |
| `setShadowBlur` | Function | Vadim Voronov | done |
| `getShadowColor` | Function | Vadim Voronov | blocked IDL |
| `setShadowColor` | Function | Vadim Voronov | done |
| `getShadowOffsetX` | Function | Vadim Voronov | blocked AceEngine |
| `setShadowOffsetX` | Function | Vadim Voronov | done |
| `getShadowOffsetY` | Function | Vadim Voronov | blocked AceEngine |
| `setShadowOffsetY` | Function | Vadim Voronov | done |
| `getStrokeStyle` | Function | | |
| `setStrokeStyle` | Function | Vadim Voronov | done |
| `getTextAlign` | Function | Vadim Voronov | blocked IDL |
| `setTextAlign` | Function | Vadim Voronov | done |
| `getTextBaseline` | Function | Vadim Voronov | blocked IDL |
| `setTextBaseline` | Function | Vadim Voronov | done |
| *CanvasRenderingContext2D* | *Class* | Vadim Voronov, Evstigneev Roman | in progress |
| `offOnAttach` | Function | Vadim Voronov, Evstigneev Roman | done |
| `offOnDetach` | Function | Vadim Voronov, Evstigneev Roman | done |
| `onOnAttach` | Function | Vadim Voronov, Evstigneev Roman | done |
| `onOnDetach` | Function | Vadim Voronov, Evstigneev Roman | done |
| `startImageAnalyzer` | Function | Vadim Voronov | done |
| `stopImageAnalyzer` | Function | Vadim Voronov | done |
| `toDataURL` | Function | Vadim Voronov | blocked IDL |
| `getCanvas` | Function | | |
| `getHeight` | Function | Vadim Voronov | done |
| `getWidth` | Function | Vadim Voronov | done |
| *Checkbox* | *Component* | Andrey Khudenkikh | blocked |
| `contentModifier` | Function | Andrey Khudenkikh | blocked IDL |
| `mark` | Function | Andrey Khudenkikh | done |
| `onChange` | Function | Andrey Khudenkikh | done |
| `select` | Function | Andrey Khudenkikh | done |
| `selectedColor` | Function | Andrey Khudenkikh | done |
| `shape` | Function | Andrey Khudenkikh | done |
| `unselectedColor` | Function | Andrey Khudenkikh | done |
| *CheckboxGroup* | *Component* | Dudkin Sergey | done |
| `checkboxShape` | Function | Dudkin Sergey | done |
| `mark` | Function | Dudkin Sergey | done |
| `onChange` | Function | Dudkin Sergey | done |
| `selectAll` | Function | Dudkin Sergey | done |
| `selectedColor` | Function | Dudkin Sergey | done |
| `unselectedColor` | Function | Dudkin Sergey | done |
| *ChildrenMainSize* | *Class* | Morozov Sergey | blocked IDL |
| `splice` | Function | Morozov Sergey | done |
| `update` | Function | Morozov Sergey | done |
| `getChildDefaultSize` | Function | Morozov Sergey | blocked IDL |
| `setChildDefaultSize` | Function | Morozov Sergey | done |
| *Circle* | *Component* | Erokhin Ilya | done |
| *ClientAuthenticationHandler* | *Class* | Erokhin Ilya | done |
| `cancel` | Function | Erokhin Ilya | done |
| `confirm` | Function | Erokhin Ilya | done |
| `ignore` | Function | Erokhin Ilya | done |
| *Column* | *Component* | Politov Mikhail | done |
| `alignItems` | Function | Politov Mikhail | done |
| `justifyContent` | Function | Politov Mikhail | done |
| `pointLight` | Function | Evstigneev Roman, Andrey Khudenkikh | done |
| `reverse` | Function | Politov Mikhail | done |
| *ColumnSplit* | *Component* | Dmitry A Smirnov | done |
| `divider` | Function | Dmitry A Smirnov | done |
| `resizeable` | Function | Dmitry A Smirnov | done |
| *Common* | *Component* | Maksimov Nikita | done |
| *CommonMethod* | *Component* | Skroba Gleb,Erokhin Ilya | in progress |
| `accessibilityChecked` | Function | Lobah Mikhail | done |
| `accessibilityDefaultFocus` | Function | | |
| `accessibilityDescription` | Function | Lobah Mikhail | done |
| `accessibilityGroup` | Function | Lobah Mikhail | done |
| `accessibilityLevel` | Function | Lobah Mikhail | done |
| `accessibilityNextFocusId` | Function | | |
| `accessibilityRole` | Function |  |  |
| `accessibilitySelected` | Function | Lobah Mikhail | done |
| `accessibilityText` | Function | Lobah Mikhail | done |
| `accessibilityTextHint` | Function | Lobah Mikhail | done |
| `accessibilityUseSamePage` | Function | | |
| `accessibilityVirtualNode` | Function | Lobah Mikhail | done |
| `advancedBlendMode` | Function | Erokhin Ilya | blocked IDL |
| `align` | Function | Roman Sedaikin | done |
| `alignRules` | Function | Dmitry A Smirnov | done |
| `alignSelf` | Function | Roman Sedaikin | done |
| `allowDrop` | Function | Lobah Mikhail | done |
| `animation` | Function | Erokhin Ilya | blocked IDL |
| `aspectRatio` | Function | Roman Sedaikin | done |
| `attributeModifier` | Function | | |
| `backdropBlur` | Function | Berezin Kirill | done |
| `background` | Function | Lobah Mikhail | done |
| `backgroundBlurStyle` | Function | Skroba Gleb | done |
| `backgroundBrightness` | Function | Skroba Gleb | done |
| `backgroundColor` | Function | Skroba Gleb | done |
| `backgroundEffect` | Function | Skroba Gleb | done |
| `backgroundFilter` | Function | Skroba Gleb | blocked IDL |
| `backgroundImage` | Function | Erokhin Ilya | in progress |
| `backgroundImagePosition` | Function | Erokhin Ilya | done |
| `backgroundImageResizable` | Function | Skroba Gleb | done |
| `backgroundImageSize` | Function | Erokhin Ilya | done |
| `bindContentCover` | Function | Erokhin Ilya | done |
| `bindContextMenu` | Function | Erokhin Ilya | blocked IDL |
| `bindMenu` | Function | Erokhin Ilya | blocked IDL |
| `bindPopup` | Function | Erokhin Ilya | done |
| `bindSheet` | Function | Erokhin Ilya | done |
| `blendMode` | Function | Lobah Mikhail | done |
| `blur` | Function | Roman Sedaikin | done |
| `border` | Function | Roman Sedaikin | done |
| `borderColor` | Function | Roman Sedaikin | done |
| `borderImage` | Function | Roman Sedaikin | done |
| `borderRadius` | Function | Roman Sedaikin | done |
| `borderStyle` | Function | Roman Sedaikin | done |
| `borderWidth` | Function | Roman Sedaikin | done |
| `brightness` | Function | Lobah Mikhail | done |
| `chainMode` | Function | Berezin Kirill | done |
| `chainWeight` | Function | Politov Mikhail | done |
| `clickEffect` | Function | Lobah Mikhail | done |
| `clip` | Function | Skroba Gleb | blocked IDL |
| `clipShape` | Function | Dmitry A Smirnov | blocked IDL |
| `colorBlend` | Function | Lobah Mikhail | done |
| `compositingFilter` | Function | Skroba Gleb | blocked IDL |
| `constraintSize` | Function | Roman Sedaikin | done |
| `contrast` | Function | Lobah Mikhail | done |
| `customProperty` | Function | Dmitry A Smirnov | blocked IDL |
| `defaultFocus` | Function | Dmitry A Smirnov | done |
| `direction` | Function | Roman Sedaikin | done |
| `displayPriority` | Function | Roman Sedaikin | done |
| `dragPreview` | Function | Lobah Mikhail | done |
| `dragPreviewOptions` | Function | Erokhin Ilya | blocked IDL |
| `draggable` | Function | Lobah Mikhail | done |
| `drawModifier` | Function | Erokhin Ilya | done |
| `enabled` | Function | Roman Sedaikin | done |
| `expandSafeArea` | Function | Dmitry A Smirnov | done |
| `flexBasis` | Function | Dmitry A Smirnov | done |
| `flexGrow` | Function | Dmitry A Smirnov | done |
| `flexShrink` | Function | Dmitry A Smirnov | done |
| `focusBox` | Function | Dmitry A Smirnov | blocked IDL |
| `focusOnTouch` | Function | Dmitry A Smirnov | done |
| `focusScopeId` | Function | Dmitry A Smirnov | done |
| `focusScopePriority` | Function | Dmitry A Smirnov | done |
| `focusable` | Function | Roman Sedaikin | done |
| `foregroundBlurStyle` | Function | Roman Sedaikin | done |
| `foregroundColor` | Function | Roman Sedaikin | done |
| `foregroundEffect` | Function | Skroba Gleb | done |
| `foregroundFilter` | Function | Skroba Gleb | blocked IDL |
| `freeze` | Function | Lobah Mikhail | done |
| `geometryTransition` | Function | Lobah Mikhail | done |
| `gesture` | Function | Erokhin Ilya | blocked IDL |
| `gestureModifier` | Function | Erokhin Ilya | blocked IDL |
| `grayscale` | Function | Lobah Mikhail | done |
| `gridOffset` | Function | Lobah Mikhail | done |
| `gridSpan` | Function | Lobah Mikhail | done |
| `groupDefaultFocus` | Function | Dmitry A Smirnov | done |
| `height` | Function | Roman Sedaikin | done |
| `hitTestBehavior` | Function | Roman Sedaikin | done |
| `hoverEffect` | Function | Roman Sedaikin | done |
| `hueRotate` | Function | Lobah Mikhail | done |
| `id` | Function | Erokhin Ilya | done |
| `invert` | Function | Lobah Mikhail | done |
| `key` | Function | Lobah Mikhail | done |
| `keyboardShortcut` | Function | Erokhin Ilya | done |
| `layoutWeight` | Function | Roman Sedaikin | done |
| `lightUpEffect` | Function | Lobah Mikhail | done |
| `linearGradient` | Function | Roman Sedaikin | done |
| `linearGradientBlur` | Function | Lobah Mikhail | done |
| `margin` | Function | Skroba Gleb | done |
| `markAnchor` | Function | Dmitry A Smirnov | done |
| `mask` | Function | Maksimov Nikita | done |
| `maskShape` | Function | Dmitry A Smirnov | blocked IDL |
| `monopolizeEvents` | Function | Erokhin Ilya | done |
| `motionBlur` | Function | Dmitry A Smirnov | done |
| `motionPath` | Function | Lobah Mikhail | done |
| `mouseResponseRegion` | Function | Skroba Gleb | done |
| `nextFocus` | Function | | |
| `obscured` | Function | Dmitry A Smirnov | done |
| `offset` | Function | Skroba Gleb | done |
| `onAccessibilityFocus` | Function |  |  |
| `onAccessibilityHover` | Function | Andrey Khudenkikh | in progress |
| `onAppear` | Function | Roman Sedaikin | done |
| `onAreaChange` | Function | Roman Sedaikin | done |
| `onAttach` | Function | Andrey Khudenkikh | done |
| `onBlur` | Function | Roman Sedaikin | done |
| `onChildTouchTest` | Function | Skroba Gleb | done |
| `onClick` | Function | Roman Sedaikin | in progress |
| `onDetach` | Function | Andrey Khudenkikh | done |
| `onDigitalCrown` | Function |  |  |
| `onDisAppear` | Function | Roman Sedaikin | done |
| `onDragEnd` | Function | Lobah Mikhail | in progress |
| `onDragEnter` | Function | Lobah Mikhail | in progress |
| `onDragLeave` | Function | Lobah Mikhail | in progress |
| `onDragMove` | Function | Lobah Mikhail | in progress |
| `onDragStart` | Function | Skroba Gleb | in progress |
| `onDrop` | Function | Lobah Mikhail | in progress |
| `onFocus` | Function | Roman Sedaikin | done |
| `onFocusAxisEvent` | Function |  |  |
| `onGestureJudgeBegin` | Function | Skroba Gleb | in progress |
| `onGestureRecognizerJudgeBegin` | Function | Skroba Gleb | in progress |
| `onHover` | Function | Andrey Khudenkikh | in progress |
| `onKeyEvent` | Function | Erokhin Ilya | blocked IDL |
| `onKeyEventDispatch` | Function |  |  |
| `onKeyPreIme` | Function | Erokhin Ilya | in progress |
| `onMouse` | Function | Andrey Khudenkikh | in progress |
| `onPreDrag` | Function | Lobah Mikhail | done |
| `onSizeChange` | Function | Dmitry A Smirnov | done |
| `onTouch` | Function | Roman Sedaikin | in progress |
| `onTouchIntercept` | Function | Andrey Khudenkikh | in progress |
| `onVisibleAreaChange` | Function | Erokhin Ilya | done |
| `opacity` | Function | Roman Sedaikin | done |
| `outline` | Function | Skroba Gleb | done |
| `outlineColor` | Function | Skroba Gleb | done |
| `outlineRadius` | Function | Skroba Gleb | done |
| `outlineStyle` | Function | Skroba Gleb | done |
| `outlineWidth` | Function | Skroba Gleb | done |
| `overlay` | Function | Lobah Mikhail | done |
| `padding` | Function | Skroba Gleb | done |
| `parallelGesture` | Function | Erokhin Ilya | blocked IDL |
| `pixelRound` | Function | Skroba Gleb | done |
| `pixelStretchEffect` | Function | Lobah Mikhail | done |
| `position` | Function | Roman Sedaikin | done |
| `priorityGesture` | Function | Erokhin Ilya | blocked IDL |
| `radialGradient` | Function | Erokhin Ilya | done |
| `renderFit` | Function | Dmitry A Smirnov | done |
| `renderGroup` | Function | Lobah Mikhail | done |
| `responseRegion` | Function | Skroba Gleb | done |
| `restoreId` | Function | Lobah Mikhail | done |
| `reuse` | Function |  |  |
| `reuseId` | Function | Dmitry A Smirnov | blocked AceEngine |
| `rotate` | Function | Dmitry A Smirnov | done |
| `safeAreaPadding` | Function | Dmitry A Smirnov | done |
| `saturate` | Function | Lobah Mikhail | done |
| `scale` | Function | Erokhin Ilya | done |
| `sepia` | Function | Lobah Mikhail | done |
| `shadow` | Function | Roman Sedaikin | done |
| `sharedTransition` | Function | Skroba Gleb | done |
| `shouldBuiltInRecognizerParallelWith` | Function | Skroba Gleb | done |
| `size` | Function | Roman Sedaikin | done |
| `sphericalEffect` | Function | Lobah Mikhail | done |
| `stateStyles` | Function | Lobah Mikhail | blocked IDL |
| `sweepGradient` | Function | Roman Sedaikin | done |
| `systemBarEffect` | Function | Lobah Mikhail | done |
| `tabIndex` | Function | Dmitry A Smirnov | done |
| `tabStop` | Function |  |  |
| `touchable` | Function | Roman Sedaikin | done |
| `transform` | Function | Lobah Mikhail | done |
| `transition` | Function | Dmitry A Smirnov | done |
| `translate` | Function | Erokhin Ilya | done |
| `useEffect` | Function | Lobah Mikhail | done |
| `useShadowBatching` | Function | Lobah Mikhail | done |
| `useSizeType` | Function | Dmitry A Smirnov | done |
| `visibility` | Function | Roman Sedaikin | done |
| `visualEffect` | Function | Skroba Gleb | blocked IDL |
| `width` | Function | Roman Sedaikin | done |
| `zIndex` | Function | Roman Sedaikin | done |
| *CommonShapeMethod* | *Component* | Skroba Gleb | blocked AceEngine |
| `antiAlias` | Function | Evstigneev Roman | done |
| `fill` | Function | Skroba Gleb | done |
| `fillOpacity` | Function | Evstigneev Roman | done |
| `stroke` | Function | Skroba Gleb | done |
| `strokeDashArray` | Function | Evstigneev Roman | blocked AceEngine |
| `strokeDashOffset` | Function | Evstigneev Roman | done |
| `strokeLineCap` | Function | Evstigneev Roman | done |
| `strokeLineJoin` | Function | Evstigneev Roman | done |
| `strokeMiterLimit` | Function | Evstigneev Roman | done |
| `strokeOpacity` | Function | Evstigneev Roman | done |
| `strokeWidth` | Function | Evstigneev Roman | done |
| *CommonTransition* | *Component* | | |
| `opacity` | Function | | |
| `scale` | Function | | |
| `slide` | Function | | |
| `translate` | Function | | |
| *Component3D* | *Component* | Kovalev Sergey | blocked |
| `customRender` | Function | Kovalev Sergey | done |
| `environment` | Function | Kovalev Sergey | done |
| `renderHeight` | Function | Kovalev Sergey | done |
| `renderWidth` | Function | Kovalev Sergey | done |
| `shader` | Function | Kovalev Sergey | done |
| `shaderImageTexture` | Function | Kovalev Sergey | done |
| `shaderInputBuffer` | Function | Kovalev Sergey | done |
| *ComponentRoot* | *Component* |  |  |
| *ConsoleMessage* | *Class* | Erokhin Ilya | blocked |
| `getLineNumber` | Function | Erokhin Ilya | done |
| `getMessage` | Function | Erokhin Ilya | blocked IDL |
| `getMessageLevel` | Function | Erokhin Ilya | blocked IDL |
| `getSourceId` | Function | Erokhin Ilya | blocked IDL |
| *ContainerSpan* | *Component* | Tuzhilkin Ivan | done |
| `attributeModifier` | Function | | |
| `textBackgroundStyle` | Function | Tuzhilkin Ivan | done |
| *ContextMenu* | *Class* | Tuzhilkin Ivan | blocked IDL |
| `close` | Function | Tuzhilkin Ivan | blocked IDL |
| *ControllerHandler* | *Class* | Erokhin Ilya | blocked |
| `setWebController` | Function | Erokhin Ilya | blocked IDL |
| *Counter* | *Component* | Erokhin Ilya | done |
| `enableDec` | Function | Erokhin Ilya | done |
| `enableInc` | Function | Erokhin Ilya | done |
| `onDec` | Function | Erokhin Ilya | done |
| `onInc` | Function | Erokhin Ilya | done |
| *CustomComponent* | *Component* | | |
| `aboutToReuse` | Function | | |
| `onLayout` | Function | | |
| `onMeasure` | Function | | |
| *CustomComponentV2* | *Component* | | |
| `aboutToReuse` | Function | | |
| *CustomDialogController* | *Class* | Maksimov Nikita | blocked IDL |
| `close` | Function | Maksimov Nikita | blocked IDL |
| `open` | Function | Maksimov Nikita | blocked IDL |
| *CustomSpan* | *Class* | Politov Mikhail | blocked IDL |
| `invalidate` | Function | Politov Mikhail | blocked IDL |
| `onDraw` | Function | Politov Mikhail | blocked IDL |
| `onMeasure` | Function | Politov Mikhail | blocked IDL |
| *DataChangeListener* | *Class* | | |
| `onDataAdd` | Function | | |
| `onDataAdded` | Function | | |
| `onDataChange` | Function | | |
| `onDataChanged` | Function | | |
| `onDataDelete` | Function | | |
| `onDataDeleted` | Function | | |
| `onDataMove` | Function | | |
| `onDataMoved` | Function | | |
| `onDataReloaded` | Function | | |
| `onDatasetChange` | Function | | |
| *DataPanel* | *Component* | Morozov Sergey | blocked |
| `closeEffect` | Function | Morozov Sergey | done |
| `contentModifier` | Function | Morozov Sergey | blocked IDL |
| `strokeWidth` | Function | Morozov Sergey | done |
| `trackBackgroundColor` | Function | Morozov Sergey | done |
| `trackShadow` | Function | Morozov Sergey | blocked IDL |
| `valueColors` | Function | Morozov Sergey | blocked IDL |
| *DataResubmissionHandler* | *Class* | Erokhin Ilya | done |
| `cancel` | Function | Erokhin Ilya | done |
| `resend` | Function | Erokhin Ilya | done |
| *DatePicker* | *Component* | Vadim Voronov | done |
| `digitalCrownSensitivity` | Function |  |  |
| `disappearTextStyle` | Function | Vadim Voronov | done |
| `enableHapticFeedback` | Function | | |
| `lunar` | Function | Vadim Voronov | done |
| `onChange` | Function | Vadim Voronov | done |
| `onDateChange` | Function | Vadim Voronov | done |
| `selectedTextStyle` | Function | Vadim Voronov | done |
| `textStyle` | Function | Vadim Voronov | done |
| *DatePickerDialog* | *Class* | Ekaterina Stepanova | blocked IDL |
| `show` | Function | Ekaterina Stepanova | blocked IDL |
| *DigitIndicator* | *Class* | | |
| `digitFont` | Function | | |
| `fontColor` | Function | | |
| `selectedDigitFont` | Function | | |
| `selectedFontColor` | Function | | |
| *Divider* | *Component* | Tuzhilkin Ivan | done |
| `color` | Function | Tuzhilkin Ivan | done |
| `lineCap` | Function | Tuzhilkin Ivan | done |
| `strokeWidth` | Function | Tuzhilkin Ivan | done |
| `vertical` | Function | Tuzhilkin Ivan | done |
| *DotIndicator* | *Class* | | |
| `color` | Function | | |
| `itemHeight` | Function | | |
| `itemWidth` | Function | | |
| `mask` | Function | | |
| `maxDisplayCount` | Function | | |
| `selectedColor` | Function | | |
| `selectedItemHeight` | Function | | |
| `selectedItemWidth` | Function | | |
| *DragEvent* | *Class* |  |  |
| `executeDropAnimation` | Function | | |
| `getData` | Function |  |  |
| `getDisplayX` | Function |  |  |
| `getDisplayY` | Function |  |  |
| `getModifierKeyState` | Function |  |  |
| `getPreviewRect` | Function |  |  |
| `getResult` | Function |  |  |
| `getSummary` | Function |  |  |
| `getVelocity` | Function |  |  |
| `getVelocityX` | Function |  |  |
| `getVelocityY` | Function |  |  |
| `getWindowX` | Function |  |  |
| `getWindowY` | Function |  |  |
| `getX` | Function |  |  |
| `getY` | Function |  |  |
| `setData` | Function |  |  |
| `setResult` | Function |  |  |
| `startDataLoading` | Function | | |
| `getDragBehavior` | Function |  |  |
| `setDragBehavior` | Function |  |  |
| `getUseCustomDropAnimation` | Function |  |  |
| `setUseCustomDropAnimation` | Function |  |  |
| *DrawingCanvas* | *Class* | Dudkin Sergey | blocked IDL |
| `drawRect` | Function | Dudkin Sergey | blocked IDL |
| *DrawingRenderingContext* | *Class* | Dudkin Sergey | done |
| `invalidate` | Function | Dudkin Sergey | done |
| `getCanvas` | Function | | |
| `getSize` | Function | | |
| *DrawModifier* | *Class* | Erokhin Ilya | blocked |
| `drawBehind` | Function | Erokhin Ilya | blocked IDL |
| `drawContent` | Function | Erokhin Ilya | blocked IDL |
| `drawFront` | Function | Erokhin Ilya | blocked IDL |
| `invalidate` | Function | Erokhin Ilya | done |
| *DynamicNode* | *Class* | Skroba Gleb | blocked ID |
| `onMove` | Function | Skroba Gleb | blocked IDL |
| *EditMenuOptions* | *Class* | Maksimov Nikita | blocked IDL |
| `onCreateMenu` | Function | Skroba Gleb | blocked IDL |
| `onMenuItemClick` | Function | Skroba Gleb | blocked IDL |
| *EffectComponent* | *Component* | Ekaterina Stepanova | done |
| *Ellipse* | *Component* | Ekaterina Stepanova | done |
| *EmbeddedComponent* | *Component* | Ekaterina Stepanova | blocked IDL |
| `onError` | Function | Ekaterina Stepanova | blocked IDL |
| `onTerminated` | Function | Ekaterina Stepanova | done |
| *Environment* | *Class* | | |
| `EnvProp` | Function | | |
| `EnvProps` | Function | | |
| `Keys` | Function | | |
| `envProp` | Function | | |
| `envProps` | Function | | |
| `keys` | Function | | |
| *EventEmulator* | *Class* | Dmitry A Smirnov | blocked IDL |
| `emitClickEvent` | Function | Dmitry A Smirnov | blocked IDL |
| `emitTextInputEvent` | Function | Dmitry A Smirnov | blocked IDL |
| *EventResult* | *Class* | Erokhin Ilya | done |
| `setGestureEventResult` | Function | Erokhin Ilya | done |
| *EventTargetInfo* | *Class* | Maksimov Nikita | blocked IDL |
| `getId` | Function | Maksimov Nikita | blocked IDL |
| *FileSelectorParam* | *Class* | Erokhin Ilya | blocked |
| `getAcceptType` | Function | Erokhin Ilya | blocked IDL |
| `getMimeTypes` | Function | | |
| `getMode` | Function | Erokhin Ilya | blocked IDL |
| `getTitle` | Function | Erokhin Ilya | blocked IDL |
| `isCapture` | Function | Erokhin Ilya | done |
| *FileSelectorResult* | *Class* | Erokhin Ilya | done |
| `handleFileList` | Function | Erokhin Ilya | done |
| *Flex* | *Component* | Kovalev Sergey | done |
| `pointLight` | Function | Evstigneev Roman, Andrey Khudenkikh | done |
| *FlowItem* | *Component* | Evstigneev Roman | done |
| *FolderStack* | *Component* | Politov Mikhail | done |
| `alignContent` | Function | Politov Mikhail | done |
| `autoHalfFold` | Function | Politov Mikhail | done |
| `enableAnimation` | Function | Politov Mikhail | done |
| `onFolderStateChange` | Function | Politov Mikhail | done |
| `onHoverStatusChange` | Function | Politov Mikhail | done |
| *FormComponent* | *Component* | Vadim Voronov | blocked AceEngine |
| `allowUpdate` | Function | Vadim Voronov | done |
| `dimension` | Function | Vadim Voronov | done |
| `moduleName` | Function | Vadim Voronov | done |
| `onAcquired` | Function | Vadim Voronov | blocked IDL |
| `onError` | Function | Vadim Voronov | blocked IDL |
| `onLoad` | Function | Vadim Voronov | done |
| `onRouter` | Function | Vadim Voronov | deprecated |
| `onUninstall` | Function | Vadim Voronov | blocked IDL |
| `size` | Function | Vadim Voronov | blocked AceEngine |
| `visibility` | Function | Vadim Voronov | done |
| *FormLink* | *Component* | Dmitry A Smirnov | done |
| *FrameNode* | *Class* | Tuzhilkin Ivan | done |
| `appendChild` | Function | Tuzhilkin Ivan | done |
| `clearChildren` | Function | Tuzhilkin Ivan | done |
| `dispose` | Function | Tuzhilkin Ivan | done |
| `getChild` | Function | Tuzhilkin Ivan | done |
| `getChildrenCount` | Function | Tuzhilkin Ivan | done |
| `getFirstChild` | Function | Tuzhilkin Ivan | done |
| `getNextSibling` | Function | Tuzhilkin Ivan | done |
| `getParent` | Function | Tuzhilkin Ivan | done |
| `getPreviousSibling` | Function | Tuzhilkin Ivan | done |
| `insertChildAfter` | Function | Tuzhilkin Ivan | done |
| `isModifiable` | Function | Tuzhilkin Ivan | done |
| `removeChild` | Function | Tuzhilkin Ivan | done |
| *FullScreenExitHandler* | *Class* | Erokhin Ilya | done |
| `exitFullScreen` | Function | Erokhin Ilya | done |
| *Gauge* | *Component* | Maksimov Nikita | blocked IDL |
| `colors` | Function | Maksimov Nikita | blocked IDL |
| `contentModifier` | Function | Maksimov Nikita | blocked IDL |
| `description` | Function | Lobah Mikhail | done |
| `endAngle` | Function | Maksimov Nikita | done |
| `indicator` | Function | Maksimov Nikita | done |
| `privacySensitive` | Function | Maksimov Nikita | done |
| `startAngle` | Function | Maksimov Nikita | done |
| `strokeWidth` | Function | Maksimov Nikita | done |
| `trackShadow` | Function | Maksimov Nikita | done |
| `value` | Function | Maksimov Nikita | done |
| *GestureGroupHandler* | *Class* | | |
| `onCancel` | Function | | |
| *GestureGroupInterface* | *Class* | | |
| `onCancel` | Function | | |
| *GestureHandler* | *Class* | | |
| `allowedTypes` | Function | | |
| `tag` | Function | | |
| *GestureInterface* | *Class* | | |
| `allowedTypes` | Function | | |
| `tag` | Function | | |
| *GestureModifier* | *Class* | Tuzhilkin Ivan | blocked IDL |
| `applyGesture` | Function | Tuzhilkin Ivan | blocked IDL |
| *GestureRecognizer* | *Class* | Kovalev Sergey | blocked IDL |
| `getEventTargetInfo` | Function | Maksimov Nikita | done |
| `getState` | Function | Kovalev Sergey | done |
| `getTag` | Function | Kovalev Sergey | blocked IDL |
| `getType` | Function | Kovalev Sergey | blocked IDL |
| `isBuiltIn` | Function | Kovalev Sergey | done |
| `isEnabled` | Function | Kovalev Sergey | done |
| `isValid` | Function | Kovalev Sergey | done |
| `setEnabled` | Function | Kovalev Sergey | done |
| *GlobalScope_common* | *Class* |  |  |
| `animateTo` | Function |  |  |
| `animateToImmediately` | Function |  |  |
| `dollar_r` | Function |  |  |
| `dollar_rawfile` | Function |  |  |
| `fp2px` | Function |  |  |
| `getContext` | Function |  |  |
| `lpx2px` | Function |  |  |
| `postCardAction` | Function |  |  |
| `px2fp` | Function |  |  |
| `px2lpx` | Function |  |  |
| `px2vp` | Function |  |  |
| `requestFocus` | Function |  |  |
| `restoreDefault` | Function |  |  |
| `setCursor` | Function |  |  |
| `vp2px` | Function |  |  |
| *GlobalScope_inspector* | *Class* |  |  |
| `getInspectorNodeById` | Function |  |  |
| `getInspectorNodes` | Function |  |  |
| `registerVsyncCallback` | Function |  |  |
| `setAppBgColor` | Function |  |  |
| `unregisterVsyncCallback` | Function |  |  |
| *Grid* | *Component* | Erokhin Ilya | blocked |
| `alignItems` | Function | Erokhin Ilya | done |
| `cachedCount` | Function | Erokhin Ilya | done |
| `cellLength` | Function | Erokhin Ilya | done |
| `columnsGap` | Function | Erokhin Ilya | done |
| `columnsTemplate` | Function | Erokhin Ilya | done |
| `edgeEffect` | Function | Erokhin Ilya | done |
| `editMode` | Function | Erokhin Ilya | done |
| `enableScrollInteraction` | Function | Erokhin Ilya | done |
| `friction` | Function | Erokhin Ilya | done |
| `layoutDirection` | Function | Erokhin Ilya | done |
| `maxCount` | Function | Erokhin Ilya | done |
| `minCount` | Function | Erokhin Ilya | done |
| `multiSelectable` | Function | Erokhin Ilya | done |
| `nestedScroll` | Function | Erokhin Ilya | done |
| `onItemDragEnter` | Function | Erokhin Ilya | done |
| `onItemDragLeave` | Function | Erokhin Ilya | done |
| `onItemDragMove` | Function | Erokhin Ilya | done |
| `onItemDragStart` | Function | Skroba Gleb | blocked IDL |
| `onItemDrop` | Function | Erokhin Ilya | done |
| `onReachEnd` | Function | Erokhin Ilya | done |
| `onReachStart` | Function | Erokhin Ilya | done |
| `onScroll` | Function | Erokhin Ilya | done |
| `onScrollBarUpdate` | Function | Skroba Gleb | done |
| `onScrollFrameBegin` | Function | Skroba Gleb | done |
| `onScrollIndex` | Function | Erokhin Ilya | done |
| `onScrollStart` | Function | Erokhin Ilya | done |
| `onScrollStop` | Function | Erokhin Ilya | done |
| `rowsGap` | Function | Erokhin Ilya | done |
| `rowsTemplate` | Function | Erokhin Ilya | done |
| `scrollBar` | Function | Erokhin Ilya | done |
| `scrollBarColor` | Function | Erokhin Ilya | done |
| `scrollBarWidth` | Function | Erokhin Ilya | done |
| `supportAnimation` | Function | Erokhin Ilya | done |
| *GridCol* | *Component* | Lobah Mikhail | done |
| `gridColOffset` | Function | Lobah Mikhail | done |
| `order` | Function | Lobah Mikhail | done |
| `span` | Function | Lobah Mikhail | done |
| *GridContainer* | *Component* | Lobah Mikhail | deprecated |
| *GridItem* | *Component* | Erokhin Ilya | done |
| `columnEnd` | Function | Erokhin Ilya | done |
| `columnStart` | Function | Erokhin Ilya | done |
| `forceRebuild` | Function | Erokhin Ilya | done |
| `onSelect` | Function | Erokhin Ilya | done |
| `rowEnd` | Function | Erokhin Ilya | done |
| `rowStart` | Function | Erokhin Ilya | done |
| `selectable` | Function | Erokhin Ilya | done |
| `selected` | Function | Erokhin Ilya | done |
| *GridRow* | *Component* | Lobah Mikhail | done |
| `alignItems` | Function | Lobah Mikhail | done |
| `onBreakpointChange` | Function | Lobah Mikhail | done |
| *HttpAuthHandler* | *Class* | Erokhin Ilya | done |
| `cancel` | Function | Erokhin Ilya | done |
| `confirm` | Function | Erokhin Ilya | done |
| `isHttpAuthInfoSaved` | Function | Erokhin Ilya | done |
| *Hyperlink* | *Component* | Morozov Sergey | done |
| `color` | Function | Morozov Sergey | done |
| *ICurve* | *Class* | Erokhin Ilya | blocked |
| `interpolate` | Function | Erokhin Ilya | blocked IDL |
| *ImageAnalyzerController* | *Class* | Vadim Voronov | blocked |
| `getImageAnalyzerSupportTypes` | Function | Vadim Voronov | blocked IDL |
| *ImageAnimator* | *Component* | Pavelyev Ivan | in progress |
| `duration` | Function | Pavelyev Ivan | done |
| `fillMode` | Function | Pavelyev Ivan | done |
| `fixedSize` | Function | Pavelyev Ivan | done |
| `images` | Function | Pavelyev Ivan | in progress |
| `iterations` | Function | Pavelyev Ivan | done |
| `monitorInvisibleArea` | Function | | |
| `onCancel` | Function | Pavelyev Ivan | done |
| `onFinish` | Function | Pavelyev Ivan | done |
| `onPause` | Function | Pavelyev Ivan | done |
| `onRepeat` | Function | Pavelyev Ivan | done |
| `onStart` | Function | Pavelyev Ivan | done |
| `preDecode` | Function | Pavelyev Ivan | done |
| `reverse` | Function | Pavelyev Ivan | done |
| `state` | Function | Pavelyev Ivan | done |
| *Image* | *Component* | Evstigneev Roman | in progress |
| `alt` | Function | Evstigneev Roman | in progress |
| `analyzerConfig` | Function | Evstigneev Roman | blocked AceEngine |
| `autoResize` | Function | Evstigneev Roman | done |
| `colorFilter` | Function | Evstigneev Roman | blocked IDL |
| `copyOption` | Function | Evstigneev Roman | blocked AceEngine |
| `draggable` | Function | Evstigneev Roman | blocked AceEngine |
| `dynamicRangeMode` | Function | Evstigneev Roman | blocked AceEngine |
| `edgeAntialiasing` | Function | Evstigneev Roman | done |
| `enableAnalyzer` | Function | Evstigneev Roman | done |
| `enhancedImageQuality` | Function | Evstigneev Roman | blocked IDL |
| `fillColor` | Function | Evstigneev Roman | done |
| `fitOriginalSize` | Function | Evstigneev Roman | done |
| `imageMatrix` | Function |  |  |
| `interpolation` | Function | Evstigneev Roman | done |
| `matchTextDirection` | Function | Evstigneev Roman | done |
| `objectFit` | Function | Berezin Kirill | done |
| `objectRepeat` | Function | Evstigneev Roman | done |
| `onComplete` | Function | Evstigneev Roman | done |
| `onError` | Function | Evstigneev Roman | done |
| `onFinish` | Function | Evstigneev Roman | done |
| `orientation` | Function |  |  |
| `pointLight` | Function | Evstigneev Roman, Andrey Khudenkikh | done |
| `privacySensitive` | Function | Evstigneev Roman | done |
| `renderMode` | Function | Evstigneev Roman | done |
| `resizable` | Function | Evstigneev Roman | in progress |
| `sourceSize` | Function | Evstigneev Roman | done |
| `syncLoad` | Function | Evstigneev Roman | done |
| *ImageBitmap* | *Class* | Pavelyev Ivan | done |
| `close` | Function | Pavelyev Ivan | done |
| `getHeight` | Function | Pavelyev Ivan | done |
| `getWidth` | Function | Pavelyev Ivan | done |
| *ImageSpan* | *Component* | Politov Mikhail | blocked |
| `alt` | Function | Politov Mikhail | done |
| `colorFilter` | Function | Politov Mikhail | blocked IDL |
| `objectFit` | Function | Politov Mikhail | done |
| `onComplete` | Function | Politov Mikhail | done |
| `onError` | Function | Politov Mikhail | done |
| `verticalAlign` | Function | Politov Mikhail | done |
| *Indicator* | *Class* | | |
| `bottom` | Function | | |
| `digit` | Function | | |
| `dot` | Function | | |
| `end` | Function | | |
| `left` | Function | | |
| `right` | Function | | |
| `start` | Function | | |
| `top` | Function | | |
| *IndicatorComponent* | *Component* | | |
| `count` | Function | | |
| `initialIndex` | Function | | |
| `loop` | Function | | |
| `onChange` | Function | | |
| `style` | Function | | |
| `vertical` | Function | | |
| *IndicatorComponentController* | *Class* | | |
| `changeIndex` | Function | | |
| `showNext` | Function | | |
| `showPrevious` | Function | | |
| *IPropertySubscriber* | *Class* | | |
| `aboutToBeDeleted` | Function | | |
| `id` | Function | | |
| *ISinglePropertyChangeSubscriber* | *Class* | | |
| `hasChanged` | Function | | |
| *IsolatedComponent* | *Component* | | |
| `onError` | Function | | |
| *JsGeolocation* | *Class* | Erokhin Ilya | done |
| `invoke` | Function | Erokhin Ilya | done |
| *JsResult* | *Class* | Erokhin Ilya | done |
| `handleCancel` | Function | Erokhin Ilya | done |
| `handleConfirm` | Function | Erokhin Ilya | done |
| `handlePromptConfirm` | Function | Erokhin Ilya | done |
| *KeyEvent* | *Class* |  |  |
| `getModifierKeyState` | Function |  |  |
| `getDeviceId` | Function |  |  |
| `setDeviceId` | Function |  |  |
| `getIntentionCode` | Function | | |
| `setIntentionCode` | Function |  |  |
| `getKeyCode` | Function |  |  |
| `setKeyCode` | Function |  |  |
| `getKeySource` | Function |  |  |
| `setKeySource` | Function |  |  |
| `getKeyText` | Function |  |  |
| `setKeyText` | Function |  |  |
| `getMetaKey` | Function |  |  |
| `setMetaKey` | Function |  |  |
| `getStopPropagation` | Function | | |
| `setStopPropagation` | Function |  |  |
| `getTimestamp` | Function |  |  |
| `setTimestamp` | Function |  |  |
| `getType` | Function |  |  |
| `setType` | Function |  |  |
| `getUnicode` | Function |  |  |
| `setUnicode` | Function |  |  |
| *LayoutManager* | *Class* | Andrey Khudenkikh | blocked IDL |
| `getGlyphPositionAtCoordinate` | Function | Andrey Khudenkikh | blocked IDL |
| `getLineCount` | Function | Andrey Khudenkikh | done |
| `getLineMetrics` | Function | Andrey Khudenkikh | blocked IDL |
| `getRectsForRange` | Function | Andrey Khudenkikh | blocked IDL |
| *LazyForEachOps* | *Class* |  |  |
| `NeedMoreElements` | Function |  |  |
| `OnRangeUpdate` | Function |  |  |
| `Prepare` | Function |  |  |
| `SetCurrentIndex` | Function |  |  |
| *LinearIndicator* | *Component* | Kovalev Sergey | done |
| `indicatorLoop` | Function | Kovalev Sergey | done |
| `indicatorStyle` | Function | Kovalev Sergey | done |
| `onChange` | Function | Kovalev Sergey | done |
| *LinearIndicatorController* | *Class* | Kovalev Sergey | done |
| `pause` | Function | Kovalev Sergey | done |
| `setProgress` | Function | Kovalev Sergey | done |
| `start` | Function | Kovalev Sergey | done |
| `stop` | Function | Kovalev Sergey | done |
| *Line* | *Component* | Dudkin Sergey | blocked |
| `endPoint` | Function | Dudkin Sergey | blocked AceEngine |
| `startPoint` | Function | Dudkin Sergey | blocked AceEngine |
| *List* | *Component* | Morozov Sergey | blocked |
| `alignListItem` | Function | Morozov Sergey | done |
| `cachedCount` | Function | Morozov Sergey | done |
| `chainAnimation` | Function | Morozov Sergey | done |
| `chainAnimationOptions` | Function | Morozov Sergey | done |
| `childrenMainSize` | Function | Morozov Sergey | done |
| `contentEndOffset` | Function | Morozov Sergey | done |
| `contentStartOffset` | Function | Morozov Sergey | done |
| `divider` | Function | Morozov Sergey | done |
| `edgeEffect` | Function | Morozov Sergey | done |
| `editMode` | Function | Morozov Sergey | done |
| `enableScrollInteraction` | Function | Morozov Sergey | done |
| `friction` | Function | Morozov Sergey | done |
| `lanes` | Function | Morozov Sergey | done |
| `listDirection` | Function | Morozov Sergey | done |
| `maintainVisibleContentPosition` | Function | Morozov Sergey | done |
| `multiSelectable` | Function | Morozov Sergey | done |
| `nestedScroll` | Function | Morozov Sergey | done |
| `onItemDelete` | Function | Skroba Gleb | blocked AceEngine |
| `onItemDragEnter` | Function | Morozov Sergey | done |
| `onItemDragLeave` | Function | Morozov Sergey | done |
| `onItemDragMove` | Function | Morozov Sergey | done |
| `onItemDragStart` | Function | Skroba Gleb | blocked IDL |
| `onItemDrop` | Function | Morozov Sergey | done |
| `onItemMove` | Function | Skroba Gleb | done |
| `onReachEnd` | Function | Morozov Sergey | done |
| `onReachStart` | Function | Morozov Sergey | done |
| `onScroll` | Function | Morozov Sergey | done |
| `onScrollFrameBegin` | Function | Skroba Gleb | done |
| `onScrollIndex` | Function | Morozov Sergey | done |
| `onScrollStart` | Function | Morozov Sergey | done |
| `onScrollStop` | Function | Morozov Sergey | done |
| `onScrollVisibleContentChange` | Function | Morozov Sergey | done |
| `scrollBar` | Function | Morozov Sergey | done |
| `scrollSnapAlign` | Function | Morozov Sergey | done |
| `sticky` | Function | Morozov Sergey | done |
| *ListItem* | *Component* | Morozov Sergey | done |
| `editable` | Function | Morozov Sergey | done |
| `onSelect` | Function | Morozov Sergey | done |
| `selectable` | Function | Morozov Sergey | done |
| `selected` | Function | Morozov Sergey | done |
| `sticky` | Function | Morozov Sergey | done |
| `swipeAction` | Function | Samarin Sergey | done |
| *ListItemGroup* | *Component* | Morozov Sergey | done |
| `childrenMainSize` | Function | Morozov Sergey | done |
| `divider` | Function | Morozov Sergey | done |
| *ListScroller* | *Class* | Morozov Sergey | blocked |
| `closeAllSwipeActions` | Function | Morozov Sergey | done |
| `getItemRectInGroup` | Function | Morozov Sergey | blocked IDL |
| `getVisibleListContentInfo` | Function | Morozov Sergey | blocked IDL |
| `scrollToItemInGroup` | Function | Morozov Sergey | done |
| *Literal_Empty* | *Class* | | |
| `indexSignature` | Function | | |
| *LoadingProgress* | *Component* | Samarin Sergey | done |
| `color` | Function | Samarin Sergey | done |
| `contentModifier` | Function | Samarin Sergey | blocked IDL |
| `enableLoading` | Function | Samarin Sergey | done |
| *LocalStorage* | *Class* | | |
| `GetShared` | Function | | |
| `clear` | Function | | |
| `delete` | Function | | |
| `get` | Function | | |
| `getShared` | Function | | |
| `has` | Function | | |
| `keys` | Function | | |
| `link` | Function | | |
| `prop` | Function | | |
| `ref` | Function | | |
| `set` | Function | | |
| `setAndLink` | Function | | |
| `setAndProp` | Function | | |
| `setAndRef` | Function | | |
| `setOrCreate` | Function | | |
| `size` | Function | | |
| *LocationButton* | *Component* | Samarin Sergey | done |
| `onClick` | Function | Samarin Sergey | in progress |
| *LongPressGestureHandler* | *Class* | | |
| `onAction` | Function | | |
| `onActionCancel` | Function | | |
| `onActionEnd` | Function | | |
| *LongPressGestureInterface* | *Class* | | |
| `onAction` | Function | | |
| `onActionCancel` | Function | | |
| `onActionEnd` | Function | | |
| *Marquee* | *Component* | Andrey Khudenkikh | done |
| `allowScale` | Function | Andrey Khudenkikh | done |
| `fontColor` | Function | Andrey Khudenkikh | done |
| `fontFamily` | Function | Andrey Khudenkikh | done |
| `fontSize` | Function | Andrey Khudenkikh | done |
| `fontWeight` | Function | Andrey Khudenkikh | done |
| `marqueeUpdateStrategy` | Function | Andrey Khudenkikh | done |
| `onBounce` | Function | Andrey Khudenkikh | done |
| `onFinish` | Function | Andrey Khudenkikh | done |
| `onStart` | Function | Andrey Khudenkikh | done |
| *Matrix2D* | *Class* | Vadim Voronov | blocked IDL |
| `identity` | Function | Vadim Voronov | done |
| `invert` | Function | Vadim Voronov | done |
| `multiply` | Function | Vadim Voronov | deprecated |
| `rotate` | Function | Vadim Voronov | done |
| `scale` | Function | Vadim Voronov | done |
| `translate` | Function | Vadim Voronov | done |
| `getRotateX` | Function | Vadim Voronov | blocked IDL |
| `setRotateX` | Function | Vadim Voronov | done |
| `getRotateY` | Function | Vadim Voronov | blocked IDL |
| `setRotateY` | Function | Vadim Voronov | done |
| `getScaleX` | Function | Vadim Voronov | blocked IDL |
| `setScaleX` | Function | Vadim Voronov | done |
| `getScaleY` | Function | Vadim Voronov | blocked IDL |
| `setScaleY` | Function | Vadim Voronov | done |
| `getTranslateX` | Function | Vadim Voronov | blocked IDL |
| `setTranslateX` | Function | Vadim Voronov | done |
| `getTranslateY` | Function | Vadim Voronov | blocked IDL |
| `setTranslateY` | Function | Vadim Voronov | done |
| *Measurable* | *Class* | | |
| `getBorderWidth` | Function | | |
| `getMargin` | Function | | |
| `getPadding` | Function | | |
| `measure` | Function | | |
| `getUniqueId` | Function | | |
| `setUniqueId` | Function | | |
| *MediaCachedImage* | *Component* | Skroba Gleb | blocked IDL |
| *Menu* | *Component* | Morozov Sergey | done |
| `font` | Function | Morozov Sergey | done |
| `fontColor` | Function | Morozov Sergey | done |
| `fontSize` | Function | Morozov Sergey | done |
| `menuItemDivider` | Function | Morozov Sergey | done |
| `menuItemGroupDivider` | Function | Morozov Sergey | done |
| `radius` | Function | Morozov Sergey | done |
| `subMenuExpandingMode` | Function | Morozov Sergey | done |
| *MenuItem* | *Component* | Morozov Sergey | blocked |
| `contentFont` | Function | Morozov Sergey | done |
| `contentFontColor` | Function | Morozov Sergey | done |
| `labelFont` | Function | Morozov Sergey | done |
| `labelFontColor` | Function | Morozov Sergey | done |
| `onChange` | Function | Morozov Sergey | done |
| `selectIcon` | Function | Morozov Sergey | in progress |
| `selected` | Function | Morozov Sergey | done |
| *MenuItemConfiguration* | *Class* | | |
| `triggerSelect` | Function | | |
| `getIcon` | Function | | |
| `setIcon` | Function | | |
| `getIndex` | Function | | |
| `setIndex` | Function | | |
| `getSelected` | Function | | |
| `setSelected` | Function | | |
| `getSymbolIcon` | Function | | |
| `setSymbolIcon` | Function | | |
| `getValue` | Function | | |
| `setValue` | Function | | |
| *MenuItemGroup* | *Component* | Morozov Sergey | done |
| *MutableStyledString* | *Class* | Maksimov Nikita | blocked IDL |
| `appendStyledString` | Function | Maksimov Nikita | done |
| `clearStyles` | Function | Maksimov Nikita | done |
| `insertString` | Function | Maksimov Nikita | done |
| `insertStyledString` | Function | Maksimov Nikita | done |
| `removeString` | Function | Maksimov Nikita | done |
| `removeStyle` | Function | Maksimov Nikita | done |
| `removeStyles` | Function | Maksimov Nikita | done |
| `replaceString` | Function | Maksimov Nikita | done |
| `replaceStyle` | Function | Maksimov Nikita | done |
| `replaceStyledString` | Function | Maksimov Nikita | done |
| `setStyle` | Function | Maksimov Nikita | done |
| *NavDestination* | *Component* | Kovalev Sergey | blocked IDL |
| `backButtonIcon` | Function | Kovalev Sergey | blocked IDL |
| `bindToNestedScrollable` | Function |  |  |
| `bindToScrollable` | Function |  |  |
| `customTransition` | Function | | |
| `hideBackButton` | Function | | |
| `hideTitleBar` | Function | Kovalev Sergey | done |
| `hideToolBar` | Function | Kovalev Sergey | blocked IDL |
| `ignoreLayoutSafeArea` | Function | Kovalev Sergey | done |
| `menus` | Function | Kovalev Sergey | blocked IDL |
| `mode` | Function | Kovalev Sergey | done |
| `onActive` | Function | | |
| `onBackPressed` | Function | Kovalev Sergey | blocked IDL |
| `onHidden` | Function | Kovalev Sergey | done |
| `onInactive` | Function | | |
| `onReady` | Function | Kovalev Sergey | blocked IDL |
| `onResult` | Function | | |
| `onShown` | Function | Kovalev Sergey | done |
| `onWillAppear` | Function | Kovalev Sergey | done |
| `onWillDisappear` | Function | Kovalev Sergey | done |
| `onWillHide` | Function | Kovalev Sergey | done |
| `onWillShow` | Function | Kovalev Sergey | done |
| `recoverable` | Function | Kovalev Sergey | done |
| `systemBarStyle` | Function | Kovalev Sergey | blocked IDL |
| `systemTransition` | Function | Kovalev Sergey | blocked IDL |
| `title` | Function | Kovalev Sergey | blocked IDL |
| `toolbarConfiguration` | Function | Kovalev Sergey | blocked IDL |
| *NavDestinationContext* | *Class* | Morozov Sergey | blocked IDL |
| `getConfigInRouteMap` | Function | Morozov Sergey | blocked IDL |
| `getNavDestinationId` | Function | Morozov Sergey | blocked IDL |
| `setNavDestinationId` | Function | Morozov Sergey | done |
| `getPathInfo` | Function | | |
| `setPathInfo` | Function | Morozov Sergey | blocked IDL |
| `getPathStack` | Function | | |
| `setPathStack` | Function | Morozov Sergey | blocked IDL |
| *NavExtender* | *Class* |  |  |
| `setUpdateStackCallback` | Function |  |  |
| *Navigation* | *Component* | | |
| `backButtonIcon` | Function | | |
| `customNavContentTransition` | Function | | |
| `enableDragBar` | Function | | |
| `enableModeChangeAnimation` | Function | | |
| `enableToolBarAdaptation` | Function | | |
| `hideBackButton` | Function | | |
| `hideNavBar` | Function | | |
| `hideTitleBar` | Function | | |
| `hideToolBar` | Function | | |
| `ignoreLayoutSafeArea` | Function | | |
| `menus` | Function | | |
| `minContentWidth` | Function | | |
| `mode` | Function | | |
| `navBarPosition` | Function | | |
| `navBarWidth` | Function | | |
| `navBarWidthRange` | Function | | |
| `navDestination` | Function | | |
| `onNavBarStateChange` | Function | | |
| `onNavigationModeChange` | Function | | |
| `onTitleModeChange` | Function | | |
| `recoverable` | Function | | |
| `subTitle` | Function | | |
| `systemBarStyle` | Function | | |
| `title` | Function | | |
| `titleMode` | Function | | |
| `toolBar` | Function | | |
| `toolbarConfiguration` | Function | | |
| *NavigationTransitionProxy* | *Class* | Morozov Sergey | blocked IDL |
| `cancelTransition` | Function | Morozov Sergey | done |
| `finishTransition` | Function | Morozov Sergey | done |
| `updateTransition` | Function | Morozov Sergey | done |
| `getFrom` | Function | | |
| `setFrom` | Function | Morozov Sergey | blocked IDL |
| `getIsInteractive` | Function | Morozov Sergey | done |
| `setIsInteractive` | Function | Morozov Sergey | done |
| `getTo` | Function | | |
| `setTo` | Function | Morozov Sergey | blocked IDL |
| *Navigator* | *Component* | Skroba Gleb | blocked IDL |
| `active` | Function | Skroba Gleb | done |
| `params` | Function | Skroba Gleb | blocked IDL |
| `target` | Function | Skroba Gleb | done |
| `type` | Function | Skroba Gleb | done |
| *NavPathStack* | *Class* | Morozov Sergey | blocked IDL |
| `clear` | Function | Morozov Sergey | blocked IDL |
| `disableAnimation` | Function | Morozov Sergey | blocked IDL |
| `getAllPathName` | Function | Morozov Sergey | blocked IDL |
| `getIndexByName` | Function | Morozov Sergey | blocked IDL |
| `getParamByIndex` | Function | Morozov Sergey | blocked IDL |
| `getParamByName` | Function | Morozov Sergey, Skroba Gleb | blocked IDL |
| `getParent` | Function | Morozov Sergey | blocked IDL |
| `moveIndexToTop` | Function | Morozov Sergey | blocked IDL |
| `moveToTop` | Function | Morozov Sergey | blocked IDL |
| `pop` | Function | Skroba Gleb | blocked IDL |
| `popToIndex` | Function | Morozov Sergey | blocked IDL |
| `popToName` | Function | Morozov Sergey | blocked IDL |
| `pushDestination` | Function | Morozov Sergey | blocked IDL |
| `pushDestinationByName` | Function | Morozov Sergey | blocked IDL |
| `pushPath` | Function | Skroba Gleb | done |
| `pushPathByName` | Function | Morozov Sergey | blocked IDL |
| `removeByIndexes` | Function | Morozov Sergey | blocked IDL |
| `removeByName` | Function | Morozov Sergey | blocked IDL |
| `removeByNavDestinationId` | Function | Morozov Sergey | blocked IDL |
| `replaceDestination` | Function | Morozov Sergey | blocked IDL |
| `replacePath` | Function | Morozov Sergey | blocked IDL |
| `replacePathByName` | Function | Morozov Sergey | blocked IDL |
| `setInterception` | Function | Morozov Sergey | blocked IDL |
| `size` | Function | Skroba Gleb | done |
| *NavRouter* | *Component* | Evstigneev Roman | blocked IDL |
| `mode` | Function | Evstigneev Roman | done |
| `onStateChange` | Function | Evstigneev Roman | done |
| *NodeContainer* | *Component* | Skroba Gleb | blocked IDL |
| *OffscreenCanvas* | *Class* | | |
| `getContext2d` | Function | | |
| `transferToImageBitmap` | Function | | |
| `getHeight` | Function | | |
| `setHeight` | Function | | |
| `getWidth` | Function | | |
| `setWidth` | Function | | |
| *OffscreenCanvasRenderingContext2D* | *Class* | | |
| `toDataURL` | Function | | |
| `transferToImageBitmap` | Function | | |
| *PageTransitionEnterInterface* | *Class* | | |
| `onEnter` | Function | | |
| *PageTransitionExitInterface* | *Class* | | |
| `onExit` | Function | | |
| *Panel* | *Component* | Pavelyev Ivan | done |
| `backgroundMask` | Function | Pavelyev Ivan | done |
| `customHeight` | Function | Pavelyev Ivan | done |
| `dragBar` | Function | Pavelyev Ivan | done |
| `fullHeight` | Function | Pavelyev Ivan | done |
| `halfHeight` | Function | Pavelyev Ivan | done |
| `miniHeight` | Function | Pavelyev Ivan | done |
| `mode` | Function | Pavelyev Ivan | done |
| `onChange` | Function | Pavelyev Ivan | done |
| `onHeightChange` | Function | Pavelyev Ivan | done |
| `show` | Function | Pavelyev Ivan | done |
| `showCloseIcon` | Function | Pavelyev Ivan | done |
| `type` | Function | Pavelyev Ivan | done |
| *PanGestureHandler* | *Class* | | |
| `onActionCancel` | Function | | |
| `onActionEnd` | Function | | |
| `onActionStart` | Function | | |
| `onActionUpdate` | Function | | |
| *PanGestureInterface* | *Class* | | |
| `onActionCancel` | Function | | |
| `onActionEnd` | Function | | |
| `onActionStart` | Function | | |
| `onActionUpdate` | Function | | |
| *PanGestureOptions* | *Class* | Politov Mikhail | blocked |
| `getDirection` | Function | Politov Mikhail | blocked IDL |
| `setDirection` | Function | Politov Mikhail | done |
| `setDistance` | Function | Politov Mikhail | done |
| `setFingers` | Function | Politov Mikhail | done |
| *PanRecognizer* | *Class* | Politov Mikhail | done |
| `getPanGestureOptions` | Function | Politov Mikhail | done |
| *PasteButton* | *Component* | Samarin Sergey | done |
| `onClick` | Function | Samarin Sergey | in progress |
| *Path2D* | *Class* | Vadim Voronov | done |
| `addPath` | Function | Vadim Voronov | done |
| *Path* | *Component* | Skroba Gleb | done |
| `commands` | Function | Skroba Gleb | done |
| *PatternLock* | *Component* | Dmitry A Smirnov | in progress |
| `activateCircleStyle` | Function | Dmitry A Smirnov | done |
| `activeColor` | Function | Dmitry A Smirnov | done |
| `autoReset` | Function | Dmitry A Smirnov | done |
| `backgroundColor` | Function | Dmitry A Smirnov | done |
| `circleRadius` | Function | Dmitry A Smirnov | done |
| `onDotConnect` | Function | Dmitry A Smirnov | done |
| `onPatternComplete` | Function | Dmitry A Smirnov | done |
| `pathColor` | Function | Dmitry A Smirnov | done |
| `pathStrokeWidth` | Function | Dmitry A Smirnov | done |
| `regularColor` | Function | Dmitry A Smirnov | done |
| `selectedColor` | Function | Dmitry A Smirnov | done |
| `sideLength` | Function | Dmitry A Smirnov | done |
| `skipUnselectedPoint` | Function |  |  |
| *PatternLockController* | *Class* | Dmitry A Smirnov | done |
| `reset` | Function | Dmitry A Smirnov | done |
| `setChallengeResult` | Function | Dmitry A Smirnov | done |
| *PermissionRequest* | *Class* | Erokhin Ilya | blocked |
| `deny` | Function | Erokhin Ilya | done |
| `getAccessibleResource` | Function | Erokhin Ilya | blocked IDL |
| `getOrigin` | Function | Erokhin Ilya | blocked IDL |
| `grant` | Function | Erokhin Ilya | done |
| *PersistentStorage* | *Class* | | |
| `DeleteProp` | Function | | |
| `Keys` | Function | | |
| `PersistProp` | Function | | |
| `PersistProps` | Function | | |
| `deleteProp` | Function | | |
| `keys` | Function | | |
| `persistProp` | Function | | |
| `persistProps` | Function | | |
| *PinchGestureHandler* | *Class* | | |
| `onActionCancel` | Function | | |
| `onActionEnd` | Function | | |
| `onActionStart` | Function | | |
| `onActionUpdate` | Function | | |
| *PinchGestureInterface* | *Class* | | |
| `onActionCancel` | Function | | |
| `onActionEnd` | Function | | |
| `onActionStart` | Function | | |
| `onActionUpdate` | Function | | |
| *PixelMap* | *Class* | Andrey Khudenkikh | blocked |
| `readPixelsToBufferSync` | Function | Andrey Khudenkikh | blocked AceEngine |
| `writeBufferToPixels` | Function | Andrey Khudenkikh | blocked AceEngine |
| `getIsEditable` | Function | Andrey Khudenkikh | blocked AceEngine |
| `setIsEditable` | Function | Andrey Khudenkikh | blocked AceEngine |
| `getIsStrideAlignment` | Function | Andrey Khudenkikh | blocked AceEngine |
| `setIsStrideAlignment` | Function | Andrey Khudenkikh | blocked AceEngine |
| *PixelMapMock* | *Class* | Maksimov Nikita | done |
| `release` | Function | Maksimov Nikita | done |
| *PluginComponent* | *Component* | Evstigneev Roman | in progress |
| `onComplete` | Function | Evstigneev Roman | done |
| `onError` | Function | Evstigneev Roman | done |
| *Polygon* | *Component* | Politov Mikhail | blocked |
| `points` | Function | Politov Mikhail | blocked IDL |
| *Polyline* | *Component* | Politov Mikhail | blocked |
| `points` | Function | Politov Mikhail | blocked IDL |
| *Progress* | *Component* | Erokhin Ilya | blocked |
| `color` | Function | Erokhin Ilya | done |
| `contentModifier` | Function | Erokhin Ilya | blocked IDL |
| `privacySensitive` | Function | Erokhin Ilya | done |
| `style` | Function | Erokhin Ilya | done |
| `value` | Function | Erokhin Ilya | done |
| *ProgressMask* | *Class* | Maksimov Nikita | done |
| `enableBreathingAnimation` | Function | Maksimov Nikita | done |
| `updateColor` | Function | Maksimov Nikita | done |
| `updateProgress` | Function | Maksimov Nikita | done |
| *QRCode* | *Component* | Evstigneev Roman | in progress |
| `backgroundColor` | Function | Evstigneev Roman | done |
| `color` | Function | Evstigneev Roman | done |
| `contentOpacity` | Function | Evstigneev Roman | done |
| *Radio* | *Component* | Evstigneev Roman | done |
| `checked` | Function | Evstigneev Roman | done |
| `contentModifier` | Function | Evstigneev Roman | blocked IDL |
| `onChange` | Function | Evstigneev Roman | done |
| `radioStyle` | Function | Evstigneev Roman | done |
| *Rating* | *Component* | Lobah Mikhail | done |
| `contentModifier` | Function | Lobah Mikhail | blocked IDL |
| `onChange` | Function | Lobah Mikhail | done |
| `starStyle` | Function | Lobah Mikhail | done |
| `stars` | Function | Lobah Mikhail | done |
| `stepSize` | Function | Lobah Mikhail | done |
| *Rect* | *Component* | Dudkin Sergey | done |
| `radius` | Function | Dudkin Sergey | done |
| `radiusHeight` | Function | Dudkin Sergey | done |
| `radiusWidth` | Function | Dudkin Sergey | done |
| *Refresh* | *Component* | Politov Mikhail | blocked |
| `onOffsetChange` | Function | Politov Mikhail | done |
| `onRefreshing` | Function | Politov Mikhail | done |
| `onStateChange` | Function | Politov Mikhail | done |
| `pullDownRatio` | Function | Politov Mikhail | done |
| `pullToRefresh` | Function | Politov Mikhail | done |
| `refreshOffset` | Function | Politov Mikhail | done |
| *RelativeContainer* | *Component* | Dmitry A Smirnov | done |
| `barrier` | Function | Dmitry A Smirnov | done |
| `guideLine` | Function | Dmitry A Smirnov | done |
| *RemoteWindow* | *Component* | Spirin Andrey, Evstigneev Roman | in progress |
| *RichEditor* | *Component* | Dudkin Sergey | in progress |
| `aboutToDelete` | Function | Dudkin Sergey | done |
| `aboutToIMEInput` | Function | Dudkin Sergey | done |
| `barState` | Function | Dudkin Sergey | done |
| `bindSelectionMenu` | Function | Dmitry A Smirnov | done |
| `caretColor` | Function | Dudkin Sergey | done |
| `copyOptions` | Function | Dudkin Sergey | done |
| `customKeyboard` | Function | Dmitry A Smirnov | done |
| `dataDetectorConfig` | Function | Dudkin Sergey | done |
| `editMenuOptions` | Function | Maksimov Nikita | blocked IDL |
| `enableDataDetector` | Function | Dudkin Sergey | done |
| `enableHapticFeedback` | Function | Dudkin Sergey | done |
| `enableKeyboardOnFocus` | Function | Dudkin Sergey | done |
| `enablePreviewText` | Function | Dudkin Sergey | done |
| `enterKeyType` | Function | Dudkin Sergey | done |
| `keyboardAppearance` | Function | | |
| `maxLength` | Function |  |  |
| `maxLines` | Function |  |  |
| `onCopy` | Function | Dudkin Sergey | done |
| `onCut` | Function | Dudkin Sergey | done |
| `onDeleteComplete` | Function | Dudkin Sergey | done |
| `onDidChange` | Function | Dudkin Sergey | done |
| `onDidIMEInput` | Function | Dudkin Sergey | done |
| `onEditingChange` | Function | Dudkin Sergey | done |
| `onIMEInputComplete` | Function | Dudkin Sergey | done |
| `onPaste` | Function | Dudkin Sergey | in progress |
| `onReady` | Function | Dudkin Sergey | done |
| `onSelect` | Function | Dudkin Sergey | done |
| `onSelectionChange` | Function | Dudkin Sergey | done |
| `onSubmit` | Function | Dudkin Sergey | in progress |
| `onWillChange` | Function | Dudkin Sergey | blocked IDL |
| `placeholder` | Function | Dudkin Sergey | done |
| `selectedBackgroundColor` | Function | Dudkin Sergey | done |
| `stopBackPress` | Function | | |
| *RichEditorBaseController* | *Class* | Dudkin Sergey | blocked |
| `closeSelectionMenu` | Function | Dudkin Sergey | done |
| `getCaretOffset` | Function | Dudkin Sergey | done |
| `getCaretRect` | Function |  |  |
| `getLayoutManager` | Function | Dudkin Sergey | done |
| `getPreviewText` | Function | Dudkin Sergey | blocked IDL |
| `getTypingStyle` | Function | Dudkin Sergey | blocked IDL |
| `isEditing` | Function | Dudkin Sergey | done |
| `setCaretOffset` | Function | Dudkin Sergey | done |
| `setSelection` | Function | Dudkin Sergey | done |
| `setTypingStyle` | Function | Dudkin Sergey | done |
| `stopEditing` | Function | Dudkin Sergey | done |
| *RichEditorController* | *Class* | Dudkin Sergey | blocked |
| `addBuilderSpan` | Function | Lobah Mikhail | done |
| `addImageSpan` | Function | Dudkin Sergey | done |
| `addSymbolSpan` | Function | Dudkin Sergey | done |
| `addTextSpan` | Function | Dudkin Sergey | in progress |
| `deleteSpans` | Function | Dudkin Sergey | done |
| `fromStyledString` | Function | Dudkin Sergey | done |
| `getParagraphs` | Function | Dudkin Sergey | blocked IDL |
| `getSelection` | Function | Dudkin Sergey | done |
| `getSpans` | Function | Dudkin Sergey | done |
| `toStyledString` | Function | Dudkin Sergey | done |
| `updateParagraphStyle` | Function | Dudkin Sergey | done |
| `updateSpanStyle` | Function | Dudkin Sergey | done |
| *RichEditorStyledStringController* | *Class* | Dudkin Sergey | blocked IDL |
| `getSelection` | Function | Dudkin Sergey | blocked IDL |
| `getStyledString` | Function | Maksimov Nikita | done |
| `onContentChanged` | Function | Dudkin Sergey | blocked IDL |
| `setStyledString` | Function | Dudkin Sergey | done |
| *RichText* | *Component* | Dudkin Sergey | done |
| `onComplete` | Function | Dudkin Sergey | done |
| `onStart` | Function | Dudkin Sergey | done |
| *Root* | *Component* |  |  |
| *RootScene* | *Component* | Spirin Andrey | done |
| *RotationGestureHandler* | *Class* | | |
| `onActionCancel` | Function | | |
| `onActionEnd` | Function | | |
| `onActionStart` | Function | | |
| `onActionUpdate` | Function | | |
| *RotationGestureInterface* | *Class* | | |
| `onActionCancel` | Function | | |
| `onActionEnd` | Function | | |
| `onActionStart` | Function | | |
| `onActionUpdate` | Function | | |
| *Row* | *Component* | Andrey Khudenkikh | done |
| `alignItems` | Function | Andrey Khudenkikh | done |
| `justifyContent` | Function | Andrey Khudenkikh | done |
| `pointLight` | Function | Evstigneev Roman, Andrey Khudenkikh | done |
| `reverse` | Function | Andrey Khudenkikh | done |
| *RowSplit* | *Component* | Dmitry A Smirnov | done |
| `resizeable` | Function | Dmitry A Smirnov | done |
| *SaveButton* | *Component* | Samarin Sergey | done |
| `onClick` | Function | Samarin Sergey | in progress |
| *Screen* | *Component* | Spirin Andrey | done |
| *ScreenCaptureHandler* | *Class* | Erokhin Ilya | blocked |
| `deny` | Function | Erokhin Ilya | done |
| `getOrigin` | Function | Erokhin Ilya | blocked IDL |
| `grant` | Function | Erokhin Ilya | done |
| *ScrollableCommonMethod* | *Component* | Samarin Sergey | blocked |
| `backToTop` | Function | | |
| `clipContent` | Function | Evstigneev Roman | blocked IDL |
| `digitalCrownSensitivity` | Function |  |  |
| `edgeEffect` | Function | Samarin Sergey | done |
| `enableScrollInteraction` | Function | Samarin Sergey | done |
| `fadingEdge` | Function | Samarin Sergey | done |
| `flingSpeedLimit` | Function | Samarin Sergey | done |
| `friction` | Function | Samarin Sergey | done |
| `nestedScroll` | Function | Samarin Sergey | done |
| `onDidScroll` | Function | Samarin Sergey | blocked IDL |
| `onReachEnd` | Function | Samarin Sergey | done |
| `onReachStart` | Function | Samarin Sergey | done |
| `onScroll` | Function | Samarin Sergey | deprecated |
| `onScrollStart` | Function | Samarin Sergey | done |
| `onScrollStop` | Function | Samarin Sergey | done |
| `onWillScroll` | Function | Skroba Gleb | done |
| `scrollBar` | Function | Samarin Sergey | done |
| `scrollBarColor` | Function | Samarin Sergey | done |
| `scrollBarWidth` | Function | Samarin Sergey | done |
| *ScrollableTargetInfo* | *Class* | Maksimov Nikita | done |
| `isBegin` | Function | Maksimov Nikita | done |
| `isEnd` | Function | Maksimov Nikita | done |
| *Scroll* | *Component* | Berezin Kirill | in progress |
| `edgeEffect` | Function | Berezin Kirill | done |
| `enablePaging` | Function | Berezin Kirill | done |
| `enableScrollInteraction` | Function | Berezin Kirill | done |
| `friction` | Function | Berezin Kirill | done |
| `initialOffset` | Function | Berezin Kirill | done |
| `nestedScroll` | Function | Berezin Kirill | done |
| `onDidScroll` | Function | Berezin Kirill | blocked IDL |
| `onScroll` | Function | Berezin Kirill | done |
| `onScrollEdge` | Function | Berezin Kirill | done |
| `onScrollEnd` | Function | Berezin Kirill | done |
| `onScrollFrameBegin` | Function | Dudkin Sergey | done |
| `onScrollStart` | Function | Berezin Kirill | done |
| `onScrollStop` | Function | Berezin Kirill | done |
| `onWillScroll` | Function | Berezin Kirill | in progress |
| `scrollBar` | Function | Berezin Kirill | done |
| `scrollBarColor` | Function | Berezin Kirill | done |
| `scrollBarWidth` | Function | Berezin Kirill | done |
| `scrollSnap` | Function | Berezin Kirill | done |
| `scrollable` | Function | Berezin Kirill | done |
| *ScrollBar* | *Component* | Maksimov Nikita | done |
| `enableNestedScroll` | Function | Maksimov Nikita | done |
| *Scroller* | *Class* | Erokhin Ilya | blocked |
| `currentOffset` | Function | Erokhin Ilya | blocked IDL |
| `fling` | Function | Erokhin Ilya | done |
| `getItemIndex` | Function | Erokhin Ilya | done |
| `getItemRect` | Function | Erokhin Ilya | blocked IDL |
| `isAtEnd` | Function | Erokhin Ilya | done |
| `scrollBy` | Function | Erokhin Ilya | done |
| `scrollEdge` | Function | Erokhin Ilya | done |
| `scrollPage` | Function | Erokhin Ilya | done |
| `scrollTo` | Function | Erokhin Ilya | done |
| `scrollToIndex` | Function | Erokhin Ilya | done |
| *Search* | *Component* | Evstigneev Roman | In Progress |
| `cancelButton` | Function | Evstigneev Roman | blocked IDL |
| `caretStyle` | Function | Evstigneev Roman | done |
| `copyOption` | Function | Evstigneev Roman | done |
| `customKeyboard` | Function | Lobah Mikhail | done |
| `decoration` | Function | Evstigneev Roman | done |
| `editMenuOptions` | Function | Maksimov Nikita | blocked IDL |
| `enableHapticFeedback` | Function | Evstigneev Roman | done |
| `enableKeyboardOnFocus` | Function | Evstigneev Roman | done |
| `enablePreviewText` | Function | Evstigneev Roman | done |
| `enterKeyType` | Function | Evstigneev Roman | done |
| `fontColor` | Function | Evstigneev Roman | done |
| `fontFeature` | Function | Evstigneev Roman | done |
| `halfLeading` | Function |  |  |
| `inputFilter` | Function | Evstigneev Roman | done |
| `keyboardAppearance` | Function | | |
| `letterSpacing` | Function | Evstigneev Roman | done |
| `lineHeight` | Function | Evstigneev Roman | done |
| `maxFontScale` | Function |  |  |
| `maxFontSize` | Function | Evstigneev Roman | done |
| `maxLength` | Function | Evstigneev Roman | done |
| `minFontScale` | Function |  |  |
| `minFontSize` | Function | Evstigneev Roman | done |
| `onChange` | Function | Evstigneev Roman | done |
| `onContentScroll` | Function | Evstigneev Roman | done |
| `onCopy` | Function | Evstigneev Roman | done |
| `onCut` | Function | Evstigneev Roman | done |
| `onDidDelete` | Function | Evstigneev Roman | done |
| `onDidInsert` | Function | Evstigneev Roman | done |
| `onEditChange` | Function | Evstigneev Roman | done |
| `onPaste` | Function | Evstigneev Roman | done |
| `onSubmit` | Function | Evstigneev Roman | in progress |
| `onTextSelectionChange` | Function | Evstigneev Roman | done |
| `onWillChange` | Function | | |
| `onWillDelete` | Function | Skroba Gleb | done |
| `onWillInsert` | Function | Skroba Gleb | done |
| `placeholderColor` | Function | Evstigneev Roman | done |
| `placeholderFont` | Function | Evstigneev Roman | done |
| `searchButton` | Function | Evstigneev Roman | done |
| `searchIcon` | Function | Evstigneev Roman | blocked AceEngine |
| `selectedBackgroundColor` | Function | Evstigneev Roman | done |
| `selectionMenuHidden` | Function | Evstigneev Roman | done |
| `stopBackPress` | Function |  |  |
| `textAlign` | Function | Evstigneev Roman | done |
| `textFont` | Function | Evstigneev Roman | done |
| `textIndent` | Function | Evstigneev Roman | done |
| `type` | Function | Evstigneev Roman | done |
| *SearchController* | *Class* | Evstigneev Roman | done |
| `caretPosition` | Function | Evstigneev Roman | done |
| `setTextSelection` | Function | Evstigneev Roman | done |
| `stopEditing` | Function | Evstigneev Roman | done |
| *SecurityComponentMethod* | *Component* | Samarin Sergey | in progress |
| `align` | Function |  |  |
| `alignRules` | Function |  |  |
| `backgroundColor` | Function | Samarin Sergey | done |
| `borderColor` | Function | Samarin Sergey | done |
| `borderRadius` | Function | Samarin Sergey | done |
| `borderStyle` | Function | Samarin Sergey | done |
| `borderWidth` | Function | Samarin Sergey | done |
| `chainMode` | Function |  |  |
| `constraintSize` | Function | Samarin Sergey | done |
| `enabled` | Function |  |  |
| `fontColor` | Function | Samarin Sergey | done |
| `fontFamily` | Function | Samarin Sergey | done |
| `fontSize` | Function | Samarin Sergey | done |
| `fontStyle` | Function | Samarin Sergey | done |
| `fontWeight` | Function | Samarin Sergey | done |
| `height` | Function | Samarin Sergey | done |
| `heightAdaptivePolicy` | Function |  |  |
| `iconColor` | Function | Samarin Sergey | done |
| `iconSize` | Function | Samarin Sergey | done |
| `id` | Function |  |  |
| `key` | Function | Samarin Sergey | done |
| `layoutDirection` | Function | Samarin Sergey | done |
| `markAnchor` | Function | Samarin Sergey | done |
| `maxFontScale` | Function |  |  |
| `maxFontSize` | Function |  |  |
| `maxLines` | Function |  |  |
| `minFontScale` | Function |  |  |
| `minFontSize` | Function |  |  |
| `offset` | Function | Samarin Sergey | done |
| `padding` | Function | Samarin Sergey | done |
| `position` | Function | Samarin Sergey | done |
| `size` | Function | Samarin Sergey | done |
| `textIconSpace` | Function | Samarin Sergey | done |
| `width` | Function | Samarin Sergey | done |
| *Select* | *Component* | Samarin Sergey | blocked |
| `arrowModifier` | Function |  |  |
| `arrowPosition` | Function | Samarin Sergey | done |
| `controlSize` | Function | Samarin Sergey | done |
| `divider` | Function | Samarin Sergey | done |
| `font` | Function | Samarin Sergey | done |
| `fontColor` | Function | Samarin Sergey | done |
| `menuAlign` | Function | Samarin Sergey | done |
| `menuBackgroundBlurStyle` | Function | Samarin Sergey | done |
| `menuBackgroundColor` | Function | Samarin Sergey | done |
| `menuItemContentModifier` | Function | Samarin Sergey | blocked IDL |
| `onSelect` | Function | Samarin Sergey | done |
| `optionBgColor` | Function | Samarin Sergey | done |
| `optionFont` | Function | Samarin Sergey | done |
| `optionFontColor` | Function | Samarin Sergey | done |
| `optionHeight` | Function | Samarin Sergey | done |
| `optionWidth` | Function | Samarin Sergey | done |
| `selected` | Function | Samarin Sergey | done |
| `selectedOptionBgColor` | Function | Samarin Sergey | done |
| `selectedOptionFont` | Function | Samarin Sergey | done |
| `selectedOptionFontColor` | Function | Samarin Sergey | done |
| `space` | Function | Samarin Sergey | done |
| `textModifier` | Function |  |  |
| `value` | Function | Samarin Sergey | done |
| *Shape* | *Component* | Dudkin Sergey | done |
| `antiAlias` | Function | Dudkin Sergey | done |
| `fill` | Function | Dudkin Sergey | done |
| `fillOpacity` | Function | Dudkin Sergey | done |
| `mesh` | Function | Dudkin Sergey | blocked AceEngine |
| `stroke` | Function | Dudkin Sergey | done |
| `strokeDashArray` | Function | Dudkin Sergey | blocked AceEngine |
| `strokeDashOffset` | Function | Dudkin Sergey | done |
| `strokeLineCap` | Function | Dudkin Sergey | done |
| `strokeLineJoin` | Function | Dudkin Sergey | done |
| `strokeMiterLimit` | Function | Dudkin Sergey | done |
| `strokeOpacity` | Function | Dudkin Sergey | done |
| `strokeWidth` | Function | Dudkin Sergey | done |
| `viewPort` | Function | Dudkin Sergey | done |
| *SideBarContainer* | *Component* | Dmitry A Smirnov | blocked |
| `autoHide` | Function | Dmitry A Smirnov | done |
| `controlButton` | Function | Dmitry A Smirnov | in progress |
| `divider` | Function | Dmitry A Smirnov | done |
| `maxSideBarWidth` | Function | Dmitry A Smirnov | done |
| `minContentWidth` | Function | Dmitry A Smirnov | done |
| `minSideBarWidth` | Function | Dmitry A Smirnov | done |
| `onChange` | Function | Dmitry A Smirnov | done |
| `showControlButton` | Function | Dmitry A Smirnov | done |
| `showSideBar` | Function | Dmitry A Smirnov | done |
| `sideBarPosition` | Function | Dmitry A Smirnov | done |
| `sideBarWidth` | Function | Dmitry A Smirnov | done |
| *Slider* | *Component* | Morozov Sergey | blocked |
| `blockBorderColor` | Function | Morozov Sergey | done |
| `blockBorderWidth` | Function | Morozov Sergey | done |
| `blockColor` | Function | Morozov Sergey | done |
| `blockSize` | Function | Morozov Sergey | done |
| `blockStyle` | Function | Morozov Sergey | in progress |
| `contentModifier` | Function | Morozov Sergey | blocked IDL |
| `digitalCrownSensitivity` | Function | | |
| `enableHapticFeedback` | Function | | |
| `maxLabel` | Function | Morozov Sergey | done |
| `minLabel` | Function | Morozov Sergey | done |
| `minResponsiveDistance` | Function | Morozov Sergey | done |
| `onChange` | Function | Morozov Sergey | done |
| `selectedBorderRadius` | Function | Morozov Sergey | done |
| `selectedColor` | Function | Morozov Sergey | done |
| `showSteps` | Function | Morozov Sergey | done |
| `showTips` | Function | Morozov Sergey | done |
| `slideRange` | Function | Morozov Sergey | done |
| `sliderInteractionMode` | Function | Morozov Sergey | done |
| `stepColor` | Function | Morozov Sergey | done |
| `stepSize` | Function | Morozov Sergey | done |
| `trackBorderRadius` | Function | Morozov Sergey | done |
| `trackColor` | Function | Morozov Sergey | blocked IDL |
| `trackThickness` | Function | Morozov Sergey | done |
| *Span* | *Component* | Politov Mikhail | done |
| `decoration` | Function | Politov Mikhail | done |
| `font` | Function | Politov Mikhail | done |
| `fontColor` | Function | Politov Mikhail | done |
| `fontFamily` | Function | Politov Mikhail | done |
| `fontSize` | Function | Politov Mikhail | done |
| `fontStyle` | Function | Politov Mikhail | done |
| `fontWeight` | Function | Politov Mikhail | done |
| `letterSpacing` | Function | Politov Mikhail | done |
| `lineHeight` | Function | Politov Mikhail | done |
| `textCase` | Function | Politov Mikhail | done |
| `textShadow` | Function | Politov Mikhail | done |
| *SslErrorHandler* | *Class* | Erokhin Ilya | done |
| `handleCancel` | Function | Erokhin Ilya | done |
| `handleConfirm` | Function | Erokhin Ilya | done |
| *Stack* | *Component* | Korobeinikov Evgeny | done |
| `alignContent` | Function | Korobeinikov Evgeny | done |
| `pointLight` | Function | Evstigneev Roman, Andrey Khudenkikh | done |
| *Stepper* | *Component* | Morozov Sergey | done |
| `onChange` | Function | Morozov Sergey | done |
| `onFinish` | Function | Morozov Sergey | done |
| `onNext` | Function | Morozov Sergey | done |
| `onPrevious` | Function | Morozov Sergey | done |
| `onSkip` | Function | Morozov Sergey | done |
| *StepperItem* | *Component* | Morozov Sergey | done |
| `nextLabel` | Function | Morozov Sergey | done |
| `prevLabel` | Function | Morozov Sergey | done |
| `status` | Function | Morozov Sergey | done |
| *Storage* | *Class* | | |
| `clear` | Function | | |
| `delete` | Function | | |
| `get` | Function | | |
| `set` | Function | | |
| *StyledString* | *Class* | Pavelyev Ivan | in progress |
| `equals` | Function | Pavelyev Ivan | done |
| `fromHtml` | Function | Pavelyev Ivan | blocked IDL |
| `getString` | Function | Pavelyev Ivan | blocked IDL |
| `getStyles` | Function | Pavelyev Ivan | blocked IDL |
| `marshalling` | Function | Pavelyev Ivan | blocked IDL |
| `subStyledString` | Function | Pavelyev Ivan | done |
| `toHtml` | Function | Pavelyev Ivan | blocked IDL |
| `unmarshalling` | Function | Pavelyev Ivan | blocked IDL |
| `getLength` | Function | Pavelyev Ivan | done |
| *StyledStringController* | *Class* | Pavelyev Ivan | done |
| `getStyledString` | Function | Pavelyev Ivan | done |
| `setStyledString` | Function | Pavelyev Ivan | done |
| *SubmitEvent* | *Class* |  |  |
| `keepEditableState` | Function |  |  |
| `getText` | Function |  |  |
| `setText` | Function |  |  |
| *SubscribaleAbstract* | *Class* | | |
| `addOwningProperty` | Function | | |
| `notifyPropertyHasChanged` | Function | | |
| `removeOwningProperty` | Function | | |
| `removeOwningPropertyById` | Function | | |
| *SubscribedAbstractProperty* | *Class* | | |
| `aboutToBeDeleted` | Function | | |
| `createOneWaySync` | Function | | |
| `createTwoWaySync` | Function | | |
| `get` | Function | | |
| `id` | Function | | |
| `info` | Function | | |
| `notifyHasChanged` | Function | | |
| `notifyPropertyRead` | Function | | |
| `numberOfSubscrbers` | Function | | |
| `set` | Function | | |
| `unlinkSuscriber` | Function | | |
| `getSubscribers_` | Function | | |
| `setSubscribers_` | Function | | |
| *SubTabBarStyle* | *Class* | | |
| `board` | Function | | |
| `id` | Function | | |
| `indicator` | Function | | |
| `labelStyle` | Function | | |
| `of` | Function | | |
| `padding` | Function | | |
| `selectedMode` | Function | | |
| *SwipeGestureHandler* | *Class* | | |
| `onAction` | Function | | |
| *SwipeGestureInterface* | *Class* | | |
| `onAction` | Function | | |
| *Swiper* | *Component* | Skroba Gleb | done |
| `autoPlay` | Function | Skroba Gleb | done |
| `cachedCount` | Function | Skroba Gleb | done |
| `curve` | Function | Skroba Gleb | done |
| `customContentTransition` | Function | Skroba Gleb | done |
| `disableSwipe` | Function | Skroba Gleb | done |
| `displayArrow` | Function | Skroba Gleb | done |
| `displayCount` | Function | Skroba Gleb | done |
| `displayMode` | Function | Skroba Gleb | done |
| `duration` | Function | Skroba Gleb | done |
| `effectMode` | Function | Skroba Gleb | done |
| `index` | Function | Skroba Gleb | done |
| `indicator` | Function | Skroba Gleb | done |
| `indicatorInteractive` | Function | Skroba Gleb | done |
| `indicatorStyle` | Function | Skroba Gleb | done |
| `interval` | Function | Skroba Gleb | done |
| `itemSpace` | Function | Skroba Gleb | done |
| `loop` | Function | Skroba Gleb | done |
| `nestedScroll` | Function | Skroba Gleb | done |
| `nextMargin` | Function | Skroba Gleb | done |
| `onAnimationEnd` | Function | Skroba Gleb | done |
| `onAnimationStart` | Function | Skroba Gleb | done |
| `onChange` | Function | Skroba Gleb | done |
| `onContentDidScroll` | Function | Skroba Gleb | done |
| `onContentWillScroll` | Function | | |
| `onGestureSwipe` | Function | Skroba Gleb | done |
| `onSelected` | Function | | |
| `onUnselected` | Function | | |
| `pageFlipMode` | Function |  |  |
| `prevMargin` | Function | Skroba Gleb | done |
| `vertical` | Function | Skroba Gleb | done |
| *SwiperContentTransitionProxy* | *Class* | Skroba Gleb | blocked IDL |
| `finishTransition` | Function | Skroba Gleb | done |
| `getIndex` | Function | Skroba Gleb | blocked IDL |
| `setIndex` | Function | Skroba Gleb | done |
| `getMainAxisLength` | Function | Skroba Gleb | blocked IDL |
| `setMainAxisLength` | Function | Skroba Gleb | done |
| `getPosition` | Function | Skroba Gleb | blocked IDL |
| `setPosition` | Function | Skroba Gleb | done |
| `getSelectedIndex` | Function | Skroba Gleb | blocked IDL |
| `setSelectedIndex` | Function | Skroba Gleb | done |
| *SwiperController* | *Class* | Skroba Gleb | done |
| `changeIndex` | Function | Skroba Gleb | done |
| `finishAnimation` | Function | Skroba Gleb | done |
| `preloadItems` | Function |  |  |
| `showNext` | Function | Skroba Gleb | done |
| `showPrevious` | Function | Skroba Gleb | done |
| *SymbolGlyph* | *Component* | Andrey Khudenkikh | blocked |
| `effectStrategy` | Function | Andrey Khudenkikh | done |
| `fontColor` | Function | Andrey Khudenkikh | done |
| `fontSize` | Function | Andrey Khudenkikh | done |
| `fontWeight` | Function | Andrey Khudenkikh | done |
| `maxFontScale` | Function | | |
| `minFontScale` | Function | | |
| `renderingStrategy` | Function | Andrey Khudenkikh | done |
| `symbolEffect` | Function | Andrey Khudenkikh | blocked AceEngine |
| *SymbolSpan* | *Component* | Dmitry A Smirnov | done |
| `attributeModifier` | Function | | |
| `effectStrategy` | Function | Dmitry A Smirnov | done |
| `fontColor` | Function | Dmitry A Smirnov | done |
| `fontSize` | Function | Dmitry A Smirnov | done |
| `fontWeight` | Function | Dmitry A Smirnov | done |
| `renderingStrategy` | Function | Dmitry A Smirnov | done |
| *SyncedPropertyOneWay* | *Class* | | |
| `aboutToBeDeleted` | Function | | |
| `get` | Function | | |
| `hasChanged` | Function | | |
| `set` | Function | | |
| *SyncedPropertyTwoWay* | *Class* | | |
| `aboutToBeDeleted` | Function | | |
| `get` | Function | | |
| `hasChanged` | Function | | |
| `set` | Function | | |
| *TabContent* | *Component* | Evstigneev Roman | in progress |
| `onWillHide` | Function | Evstigneev Roman | done |
| `onWillShow` | Function | Evstigneev Roman | done |
| `tabBar` | Function | Lobah Mikhail | done |
| *TabContentTransitionProxy* | *Class* | Dudkin Sergey | done |
| `finishTransition` | Function | Dudkin Sergey | done |
| `getFrom` | Function | Dudkin Sergey | done |
| `setFrom` | Function | Dudkin Sergey | done |
| `getTo` | Function | Dudkin Sergey | done |
| `setTo` | Function | Dudkin Sergey | done |
| *Tabs* | *Component* | Tuzhilkin Ivan | done |
| `animationDuration` | Function | Tuzhilkin Ivan | done |
| `animationMode` | Function | Tuzhilkin Ivan | done |
| `barBackgroundBlurStyle` | Function | Tuzhilkin Ivan | done |
| `barBackgroundColor` | Function | Tuzhilkin Ivan | done |
| `barBackgroundEffect` | Function | Tuzhilkin Ivan | done |
| `barGridAlign` | Function | Tuzhilkin Ivan | done |
| `barHeight` | Function | Tuzhilkin Ivan | done |
| `barMode` | Function | Tuzhilkin Ivan | done |
| `barModeScrollable` | Function | Tuzhilkin Ivan | done |
| `barOverlap` | Function | Tuzhilkin Ivan | done |
| `barPosition` | Function | Tuzhilkin Ivan | done |
| `barWidth` | Function | Tuzhilkin Ivan | done |
| `customContentTransition` | Function | Dudkin Sergey | done |
| `divider` | Function | Tuzhilkin Ivan | done |
| `edgeEffect` | Function | Tuzhilkin Ivan | done |
| `fadingEdge` | Function | Tuzhilkin Ivan | done |
| `onAnimationEnd` | Function | Tuzhilkin Ivan | done |
| `onAnimationStart` | Function | Tuzhilkin Ivan | done |
| `onChange` | Function | Tuzhilkin Ivan | done |
| `onContentWillChange` | Function | Dudkin Sergey | done |
| `onGestureSwipe` | Function | Tuzhilkin Ivan | done |
| `onSelected` | Function | | |
| `onTabBarClick` | Function | Tuzhilkin Ivan | done |
| `onUnselected` | Function | | |
| `pageFlipMode` | Function |  |  |
| `scrollable` | Function | Tuzhilkin Ivan | done |
| `vertical` | Function | Tuzhilkin Ivan | done |
| *TabsController* | *Class* | Skroba Gleb | done |
| `changeIndex` | Function | Skroba Gleb | done |
| `preloadItems` | Function | Skroba Gleb | done |
| `setTabBarOpacity` | Function | Skroba Gleb | done |
| `setTabBarTranslate` | Function | Skroba Gleb | done |
| *TapGestureHandler* | *Class* | | |
| `onAction` | Function | | |
| *TapGestureInterface* | *Class* | | |
| `onAction` | Function | | |
| *TextArea* | *Component* | Tuzhilkin Ivan | blocked IDL |
| `barState` | Function | Tuzhilkin Ivan | done |
| `caretColor` | Function | Tuzhilkin Ivan | done |
| `caretStyle` | Function | Tuzhilkin Ivan | done |
| `contentType` | Function | Tuzhilkin Ivan | done |
| `copyOption` | Function | Tuzhilkin Ivan | done |
| `customKeyboard` | Function | Erokhin Ilya | done |
| `decoration` | Function | Tuzhilkin Ivan | done |
| `editMenuOptions` | Function | Maksimov Nikita | blocked IDL |
| `ellipsisMode` | Function |  |  |
| `enableAutoFill` | Function | Tuzhilkin Ivan | done |
| `enableHapticFeedback` | Function | Tuzhilkin Ivan | done |
| `enableKeyboardOnFocus` | Function | Tuzhilkin Ivan | done |
| `enablePreviewText` | Function | Tuzhilkin Ivan | done |
| `enterKeyType` | Function | Tuzhilkin Ivan | done |
| `fontColor` | Function | Tuzhilkin Ivan | done |
| `fontFamily` | Function | Tuzhilkin Ivan | done |
| `fontFeature` | Function | Tuzhilkin Ivan | done |
| `fontSize` | Function | Tuzhilkin Ivan | done |
| `fontStyle` | Function | Tuzhilkin Ivan | done |
| `fontWeight` | Function | Tuzhilkin Ivan | done |
| `halfLeading` | Function |  |  |
| `heightAdaptivePolicy` | Function | Tuzhilkin Ivan | done |
| `inputFilter` | Function | Tuzhilkin Ivan | done |
| `keyboardAppearance` | Function | | |
| `letterSpacing` | Function | Tuzhilkin Ivan | done |
| `lineBreakStrategy` | Function | Tuzhilkin Ivan | done |
| `lineHeight` | Function | Tuzhilkin Ivan | done |
| `lineSpacing` | Function | Tuzhilkin Ivan | done |
| `maxFontScale` | Function |  |  |
| `maxFontSize` | Function | Tuzhilkin Ivan | done |
| `maxLength` | Function | Tuzhilkin Ivan | done |
| `maxLines` | Function | Tuzhilkin Ivan | done |
| `minFontScale` | Function |  |  |
| `minFontSize` | Function | Tuzhilkin Ivan | done |
| `onChange` | Function | Tuzhilkin Ivan | done |
| `onContentScroll` | Function | Tuzhilkin Ivan | done |
| `onCopy` | Function | Tuzhilkin Ivan | done |
| `onCut` | Function | Tuzhilkin Ivan | done |
| `onDidDelete` | Function | Tuzhilkin Ivan | done |
| `onDidInsert` | Function | Tuzhilkin Ivan | done |
| `onEditChange` | Function | Tuzhilkin Ivan | done |
| `onPaste` | Function | Tuzhilkin Ivan | done |
| `onSubmit` | Function | Tuzhilkin Ivan | in progress |
| `onTextSelectionChange` | Function | Tuzhilkin Ivan | done |
| `onWillChange` | Function | | |
| `onWillDelete` | Function | Skroba Gleb | done |
| `onWillInsert` | Function | Skroba Gleb | done |
| `placeholderColor` | Function | Tuzhilkin Ivan | done |
| `placeholderFont` | Function | Tuzhilkin Ivan | done |
| `selectedBackgroundColor` | Function | Tuzhilkin Ivan | done |
| `selectionMenuHidden` | Function | Tuzhilkin Ivan | done |
| `showCounter` | Function | Tuzhilkin Ivan | done |
| `stopBackPress` | Function |  |  |
| `style` | Function | Tuzhilkin Ivan | done |
| `textAlign` | Function | Tuzhilkin Ivan | done |
| `textIndent` | Function | Tuzhilkin Ivan | done |
| `textOverflow` | Function | Tuzhilkin Ivan | done |
| `type` | Function | Tuzhilkin Ivan | done |
| `wordBreak` | Function | Tuzhilkin Ivan | done |
| *TextAreaController* | *Class* | Tuzhilkin Ivan | done |
| `caretPosition` | Function | Tuzhilkin Ivan | done |
| `setTextSelection` | Function | Tuzhilkin Ivan | done |
| `stopEditing` | Function | Tuzhilkin Ivan | done |
| *Text* | *Component* | Samarin Sergey | in progress |
| `baselineOffset` | Function | Samarin Sergey | done |
| `bindSelectionMenu` | Function | Lobah Mikhail | done |
| `caretColor` | Function | Samarin Sergey | done |
| `copyOption` | Function | Samarin Sergey | done |
| `dataDetectorConfig` | Function | Samarin Sergey | done |
| `decoration` | Function | Samarin Sergey | done |
| `draggable` | Function | Samarin Sergey | done |
| `editMenuOptions` | Function | Maksimov Nikita | blocked IDL |
| `ellipsisMode` | Function | Samarin Sergey | done |
| `enableDataDetector` | Function | Kirill Kirichenko | done |
| `enableHapticFeedback` | Function | Samarin Sergey | done |
| `font` | Function | Samarin Sergey | done |
| `fontColor` | Function | Samarin Sergey | done |
| `fontFamily` | Function | Samarin Sergey | done |
| `fontFeature` | Function | Samarin Sergey | done |
| `fontSize` | Function | Samarin Sergey | done |
| `fontStyle` | Function | Samarin Sergey | done |
| `fontWeight` | Function | Samarin Sergey | done |
| `halfLeading` | Function | Samarin Sergey | done |
| `heightAdaptivePolicy` | Function | Samarin Sergey | done |
| `letterSpacing` | Function | Samarin Sergey | done |
| `lineBreakStrategy` | Function | Samarin Sergey | done |
| `lineHeight` | Function | Samarin Sergey | done |
| `lineSpacing` | Function | Samarin Sergey | done |
| `marqueeOptions` | Function |  |  |
| `maxFontScale` | Function | Samarin Sergey | done |
| `maxFontSize` | Function | Samarin Sergey | done |
| `maxLines` | Function | Samarin Sergey | done |
| `minFontScale` | Function | Samarin Sergey | done |
| `minFontSize` | Function | Samarin Sergey | done |
| `onCopy` | Function | Kirill Kirichenko | done |
| `onMarqueeStateChange` | Function |  |  |
| `onTextSelectionChange` | Function | Kirill Kirichenko | done |
| `privacySensitive` | Function | Samarin Sergey | done |
| `selectedBackgroundColor` | Function | Samarin Sergey | done |
| `selection` | Function | Samarin Sergey | done |
| `textAlign` | Function | Samarin Sergey | done |
| `textCase` | Function | Samarin Sergey | done |
| `textIndent` | Function | Samarin Sergey | done |
| `textOverflow` | Function | Samarin Sergey | done |
| `textSelectable` | Function | Samarin Sergey | done |
| `textShadow` | Function | Samarin Sergey | done |
| `wordBreak` | Function | Samarin Sergey | done |
| *TextBaseController* | *Class* | Morozov Sergey | done |
| `closeSelectionMenu` | Function | Morozov Sergey | done |
| `getLayoutManager` | Function | Morozov Sergey | done |
| `setSelection` | Function | Morozov Sergey | done |
| *TextClock* | *Component* | Pavelyev Ivan | in progress |
| `contentModifier` | Function | Pavelyev Ivan | blocked IDL |
| `dateTimeOptions` | Function | Pavelyev Ivan | blocked IDL |
| `fontColor` | Function | Pavelyev Ivan | done |
| `fontFamily` | Function | Pavelyev Ivan | done |
| `fontFeature` | Function | Pavelyev Ivan | done |
| `fontSize` | Function | Pavelyev Ivan | done |
| `fontStyle` | Function | Pavelyev Ivan | done |
| `fontWeight` | Function | Pavelyev Ivan | done |
| `format` | Function | Pavelyev Ivan | done |
| `onDateChange` | Function | Pavelyev Ivan | done |
| `textShadow` | Function | Pavelyev Ivan | done |
| *TextClockController* | *Class* | Pavelyev Ivan | in progress |
| `start` | Function | Pavelyev Ivan | done |
| `stop` | Function | Pavelyev Ivan | done |
| *TextContentControllerBase* | *Class* | Morozov Sergey | blocked IDL |
| `addText` | Function | | |
| `deleteText` | Function | | |
| `getCaretOffset` | Function | Morozov Sergey | blocked IDL |
| `getSelection` | Function | | |
| `getTextContentLineCount` | Function | Morozov Sergey | done |
| `getTextContentRect` | Function | Morozov Sergey | blocked IDL |
| *TextController* | *Class* | Samarin Sergey | done |
| `closeSelectionMenu` | Function | Samarin Sergey | done |
| `getLayoutManager` | Function | Samarin Sergey | done |
| `setStyledString` | Function | Samarin Sergey | done |
| *TextEditControllerEx* | *Class* | Morozov Sergey | blocked IDL |
| `getCaretOffset` | Function | Morozov Sergey | done |
| `getPreviewText` | Function | Morozov Sergey | blocked IDL |
| `isEditing` | Function | Morozov Sergey | done |
| `setCaretOffset` | Function | Morozov Sergey | done |
| `stopEditing` | Function | Morozov Sergey | done |
| *TextInput* | *Component* | Spirin Andrey | in progress |
| `barState` | Function | Spirin Andrey | done |
| `cancelButton` | Function | Spirin Andrey, Andrey Khudenkikh | done |
| `caretColor` | Function | Spirin Andrey | done |
| `caretPosition` | Function | Spirin Andrey | done |
| `caretStyle` | Function | Spirin Andrey | done |
| `contentType` | Function | Spirin Andrey | done |
| `copyOption` | Function | Spirin Andrey | done |
| `customKeyboard` | Function | Lobah Mikhail | done |
| `decoration` | Function | Spirin Andrey | done |
| `editMenuOptions` | Function | Maksimov Nikita | blocked IDL |
| `ellipsisMode` | Function |  |  |
| `enableAutoFill` | Function | Spirin Andrey | done |
| `enableHapticFeedback` | Function | Spirin Andrey | done |
| `enableKeyboardOnFocus` | Function | Spirin Andrey | done |
| `enablePreviewText` | Function | Spirin Andrey | done |
| `enterKeyType` | Function | Spirin Andrey | done |
| `fontColor` | Function | Spirin Andrey | done |
| `fontFamily` | Function | Spirin Andrey | done |
| `fontFeature` | Function | Spirin Andrey | done |
| `fontSize` | Function | Spirin Andrey | done |
| `fontStyle` | Function | Spirin Andrey | done |
| `fontWeight` | Function | Spirin Andrey | done |
| `halfLeading` | Function |  |  |
| `heightAdaptivePolicy` | Function | Spirin Andrey | done |
| `inputFilter` | Function | Spirin Andrey | done |
| `keyboardAppearance` | Function | | |
| `letterSpacing` | Function | Spirin Andrey | done |
| `lineBreakStrategy` | Function | Spirin Andrey | done |
| `lineHeight` | Function | Spirin Andrey | done |
| `maxFontScale` | Function |  |  |
| `maxFontSize` | Function | Spirin Andrey | done |
| `maxLength` | Function | Spirin Andrey | done |
| `maxLines` | Function | Spirin Andrey | done |
| `minFontScale` | Function |  |  |
| `minFontSize` | Function | Spirin Andrey | done |
| `onChange` | Function | Lobah Mikhail | done |
| `onContentScroll` | Function | Spirin Andrey | done |
| `onCopy` | Function | Spirin Andrey | done |
| `onCut` | Function | Spirin Andrey | done |
| `onDidDelete` | Function | Spirin Andrey | done |
| `onDidInsert` | Function | Spirin Andrey | done |
| `onEditChange` | Function | Spirin Andrey | done |
| `onEditChanged` | Function | Spirin Andrey | done |
| `onPaste` | Function | Lobah Mikhail | done |
| `onSecurityStateChange` | Function | Spirin Andrey | done |
| `onSubmit` | Function | Spirin Andrey | in progress |
| `onTextSelectionChange` | Function | Spirin Andrey | done |
| `onWillChange` | Function | | |
| `onWillDelete` | Function | Skroba Gleb | done |
| `onWillInsert` | Function | Skroba Gleb | done |
| `passwordIcon` | Function | Spirin Andrey | done |
| `passwordRules` | Function | Spirin Andrey | done |
| `placeholderColor` | Function | Spirin Andrey | done |
| `placeholderFont` | Function | Spirin Andrey | done |
| `selectAll` | Function | Spirin Andrey | done |
| `selectedBackgroundColor` | Function | Spirin Andrey | done |
| `selectionMenuHidden` | Function | Spirin Andrey | done |
| `showCounter` | Function | Spirin Andrey | blocked AceEngine |
| `showError` | Function | Spirin Andrey | done |
| `showPassword` | Function | Spirin Andrey | done |
| `showPasswordIcon` | Function | Spirin Andrey | done |
| `showUnderline` | Function | Spirin Andrey | done |
| `showUnit` | Function | Erokhin Ilya | done |
| `stopBackPress` | Function |  |  |
| `style` | Function | Spirin Andrey | done |
| `textAlign` | Function | Spirin Andrey | done |
| `textIndent` | Function | Spirin Andrey | done |
| `textOverflow` | Function | Spirin Andrey | blocked AceEngine |
| `type` | Function | Spirin Andrey | done |
| `underlineColor` | Function | Spirin Andrey | done |
| `wordBreak` | Function | Spirin Andrey | done |
| *TextInputController* | *Class* | Spirin Andrey | done |
| `caretPosition` | Function | Spirin Andrey | done |
| `setTextSelection` | Function | Spirin Andrey | done |
| `stopEditing` | Function | Spirin Andrey | done |
| *TextMenuItemId* | *Class* | Maksimov Nikita | done |
| `equals` | Function | Maksimov Nikita | done |
| `of` | Function | Maksimov Nikita | done |
| `getAI_WRITER` | Function | | |
| `getCAMERA_INPUT` | Function | | |
| `getCOLLABORATION_SERVICE` | Function | | |
| `getCOPY` | Function | | |
| `getCUT` | Function | | |
| `getPASTE` | Function | | |
| `getSEARCH` | Function | | |
| `getSELECT_ALL` | Function | | |
| `getSHARE` | Function | | |
| `getTRANSLATE` | Function | | |
| *TextPicker* | *Component* | Ekaterina Stepanova | in progress |
| `canLoop` | Function | Ekaterina Stepanova | done |
| `defaultPickerItemHeight` | Function | Ekaterina Stepanova | done |
| `defaultTextStyle` | Function |  |  |
| `digitalCrownSensitivity` | Function |  |  |
| `disableTextStyleAnimation` | Function |  |  |
| `disappearTextStyle` | Function | Ekaterina Stepanova | done |
| `divider` | Function | Ekaterina Stepanova | done |
| `enableHapticFeedback` | Function |  |  |
| `gradientHeight` | Function | Ekaterina Stepanova | done |
| `onAccept` | Function | Ekaterina Stepanova | done |
| `onCancel` | Function | Ekaterina Stepanova | done |
| `onChange` | Function | Tuzhilkin Ivan | done |
| `onEnterSelectedArea` | Function | | |
| `onScrollStop` | Function |  |  |
| `selectedIndex` | Function | Ekaterina Stepanova | done |
| `selectedTextStyle` | Function | Ekaterina Stepanova | done |
| `textStyle` | Function | Ekaterina Stepanova | done |
| *TextPickerDialog* | *Class* | Ekaterina Stepanova | blocked IDL |
| `show` | Function | Ekaterina Stepanova | blocked IDL |
| *TextTimer* | *Component* | Ekaterina Stepanova | blocked |
| `contentModifier` | Function | Ekaterina Stepanova | blocked IDL |
| `fontColor` | Function | Ekaterina Stepanova | done |
| `fontFamily` | Function | Ekaterina Stepanova | done |
| `fontSize` | Function | Ekaterina Stepanova | done |
| `fontStyle` | Function | Ekaterina Stepanova | done |
| `fontWeight` | Function | Ekaterina Stepanova | done |
| `format` | Function | Ekaterina Stepanova | done |
| `onTimer` | Function | Ekaterina Stepanova | blocked IDL |
| `textShadow` | Function | Ekaterina Stepanova | blocked AceEngine |
| *TextTimerController* | *Class* | Ekaterina Stepanova | done |
| `pause` | Function | Ekaterina Stepanova | done |
| `reset` | Function | Ekaterina Stepanova | done |
| `start` | Function | Ekaterina Stepanova | done |
| *TimePicker* | *Component* | Ekaterina Stepanova | blocked |
| `dateTimeOptions` | Function | Ekaterina Stepanova | blocked IDL |
| `digitalCrownSensitivity` | Function |  |  |
| `disappearTextStyle` | Function | Ekaterina Stepanova | done |
| `enableCascade` | Function |  |  |
| `enableHapticFeedback` | Function | Ekaterina Stepanova | done |
| `loop` | Function | Ekaterina Stepanova | done |
| `onChange` | Function | Ekaterina Stepanova | done |
| `onEnterSelectedArea` | Function | | |
| `selectedTextStyle` | Function | Ekaterina Stepanova | done |
| `textStyle` | Function | Ekaterina Stepanova | done |
| `useMilitaryTime` | Function | Ekaterina Stepanova | done |
| *TimePickerDialog* | *Class* | Ekaterina Stepanova | blocked IDL |
| `show` | Function | Ekaterina Stepanova | blocked IDL |
| *Toggle* | *Component* | Morozov Sergey | blocked |
| `contentModifier` | Function | Morozov Sergey | blocked IDL |
| `onChange` | Function | Morozov Sergey | done |
| `selectedColor` | Function | Morozov Sergey | done |
| `switchPointColor` | Function | Morozov Sergey | done |
| `switchStyle` | Function | Morozov Sergey | done |
| *TouchEvent* | *Class* |  |  |
| `getHistoricalPoints` | Function |  |  |
| `getChangedTouches` | Function | | |
| `setChangedTouches` | Function |  |  |
| `getPreventDefault` | Function | | |
| `setPreventDefault` | Function |  |  |
| `getStopPropagation` | Function | | |
| `setStopPropagation` | Function |  |  |
| `getTouches` | Function | | |
| `setTouches` | Function |  |  |
| `getType` | Function |  |  |
| `setType` | Function |  |  |
| *TransitionEffect* | *Class* | Andrey Khudenkikh | blocked IDL |
| `animation` | Function | Andrey Khudenkikh | blocked IDL |
| `asymmetric` | Function | Andrey Khudenkikh | done |
| `combine` | Function | Andrey Khudenkikh | blocked IDL |
| `move` | Function | Andrey Khudenkikh | done |
| `opacity` | Function | Andrey Khudenkikh | done |
| `rotate` | Function | Andrey Khudenkikh | done |
| `scale` | Function | Andrey Khudenkikh | done |
| `translate` | Function | Andrey Khudenkikh | done |
| `getIDENTITY` | Function | | |
| `getOPACITY` | Function | | |
| `getSLIDE` | Function | | |
| `getSLIDE_SWITCH` | Function | | |
| *UICommonEvent* | *Class* |  |  |
| `setOnAppear` | Function |  |  |
| `setOnBlur` | Function |  |  |
| `setOnClick` | Function |  |  |
| `setOnDisappear` | Function |  |  |
| `setOnFocus` | Function |  |  |
| `setOnHover` | Function |  |  |
| `setOnKeyEvent` | Function |  |  |
| `setOnMouse` | Function |  |  |
| `setOnSizeChange` | Function |  |  |
| `setOnTouch` | Function |  |  |
| `setOnVisibleAreaApproximateChange` | Function |  |  |
| *UIExtensionComponent* | *Component* | Tuzhilkin Ivan | blocked IDL |
| `onDrawReady` | Function |  |  |
| `onError` | Function | Tuzhilkin Ivan | blocked IDL |
| `onReceive` | Function | Tuzhilkin Ivan | blocked IDL |
| `onRelease` | Function | Tuzhilkin Ivan | done |
| `onRemoteReady` | Function | Tuzhilkin Ivan | done |
| `onResult` | Function | Tuzhilkin Ivan | done |
| `onTerminated` | Function | Tuzhilkin Ivan | done |
| *UIExtensionProxy* | *Class* | Tuzhilkin Ivan | blocked IDL |
| `offAsyncReceiverRegister` | Function | Tuzhilkin Ivan | done |
| `offSyncReceiverRegister` | Function | Tuzhilkin Ivan | done |
| `onAsyncReceiverRegister` | Function | Tuzhilkin Ivan | done |
| `onSyncReceiverRegister` | Function | Tuzhilkin Ivan | done |
| `send` | Function | Tuzhilkin Ivan | blocked IDL |
| `sendSync` | Function | Tuzhilkin Ivan | blocked IDL |
| *UIGestureEvent* | *Class* | | |
| `addGesture` | Function | | |
| `addParallelGesture` | Function | | |
| `clearGestures` | Function | | |
| `removeGestureByTag` | Function | | |
| *UnifiedData* | *Class* | Tuzhilkin Ivan | blocked IDL |
| `getTypes` | Function | Tuzhilkin Ivan | blocked IDL |
| `hasType` | Function | Tuzhilkin Ivan | blocked IDL |
| *Video* | *Component* | Erokhin Ilya | blocked |
| `analyzerConfig` | Function | Erokhin Ilya | blocked AceEngine |
| `autoPlay` | Function | Erokhin Ilya | done |
| `controls` | Function | Erokhin Ilya | done |
| `enableAnalyzer` | Function | Erokhin Ilya | done |
| `enableShortcutKey` | Function |  |  |
| `loop` | Function | Erokhin Ilya | done |
| `muted` | Function | Erokhin Ilya | done |
| `objectFit` | Function | Erokhin Ilya | done |
| `onError` | Function | Erokhin Ilya | done |
| `onFinish` | Function | Erokhin Ilya | done |
| `onFullscreenChange` | Function | Erokhin Ilya | done |
| `onPause` | Function | Erokhin Ilya | done |
| `onPrepared` | Function | Erokhin Ilya | done |
| `onSeeked` | Function | Erokhin Ilya | done |
| `onSeeking` | Function | Erokhin Ilya | done |
| `onStart` | Function | Erokhin Ilya | done |
| `onStop` | Function | Erokhin Ilya | done |
| `onUpdate` | Function | Erokhin Ilya | done |
| `surfaceBackgroundColor` | Function |  |  |
| *VideoController* | *Class* | Erokhin Ilya | done |
| `exitFullscreen` | Function | Erokhin Ilya | done |
| `pause` | Function | Erokhin Ilya | done |
| `requestFullscreen` | Function | Erokhin Ilya | done |
| `reset` | Function | Erokhin Ilya | done |
| `setCurrentTime` | Function | Erokhin Ilya | done |
| `start` | Function | Erokhin Ilya | done |
| `stop` | Function | Erokhin Ilya | done |
| *View* | *Class* | Skroba Gleb | blocked IDL |
| `create` | Function | Skroba Gleb | blocked IDL |
| *VirtualScrollOptions* | *Class* | | |
| `onLazyLoading` | Function | | |
| `getReusable` | Function | | |
| `setReusable` | Function | | |
| `getTotalCount` | Function | | |
| `setTotalCount` | Function | | |
| *WaterFlow* | *Component* | Kovalev Sergey | in progress |
| `cachedCount` | Function | Kovalev Sergey | done |
| `columnsGap` | Function | Kovalev Sergey | done |
| `columnsTemplate` | Function | Kovalev Sergey | done |
| `enableScrollInteraction` | Function | Kovalev Sergey | done |
| `friction` | Function | Kovalev Sergey | done |
| `itemConstraintSize` | Function | Kovalev Sergey | done |
| `layoutDirection` | Function | Kovalev Sergey | done |
| `nestedScroll` | Function | Kovalev Sergey | done |
| `onReachEnd` | Function | Kovalev Sergey | done |
| `onReachStart` | Function | Kovalev Sergey | done |
| `onScrollFrameBegin` | Function | Dudkin Sergey | done |
| `onScrollIndex` | Function | Kovalev Sergey | done |
| `rowsGap` | Function | Kovalev Sergey | done |
| `rowsTemplate` | Function | Kovalev Sergey | done |
| *WaterFlowSections* | *Class* | Kovalev Sergey | in progress |
| `length` | Function | Kovalev Sergey | done |
| `push` | Function | Kovalev Sergey | done |
| `splice` | Function | Kovalev Sergey | done |
| `update` | Function | Kovalev Sergey | done |
| `values` | Function | Kovalev Sergey | blocked IDL |
| *Web* | *Component* | Erokhin Ilya | blocked |
| `allowWindowOpenMethod` | Function | Erokhin Ilya | done |
| `bindSelectionMenu` | Function | Lobah Mikhail | done |
| `blockNetwork` | Function | Erokhin Ilya | done |
| `blurOnKeyboardHideMode` | Function |  |  |
| `cacheMode` | Function | Erokhin Ilya | done |
| `copyOptions` | Function | Erokhin Ilya | done |
| `darkMode` | Function | Erokhin Ilya | done |
| `databaseAccess` | Function | Erokhin Ilya | done |
| `defaultFixedFontSize` | Function | Erokhin Ilya | done |
| `defaultFontSize` | Function | Erokhin Ilya | done |
| `defaultTextEncodingFormat` | Function | Erokhin Ilya | done |
| `domStorageAccess` | Function | Erokhin Ilya | done |
| `editMenuOptions` | Function | Maksimov Nikita | blocked IDL |
| `enableFollowSystemFontWeight` | Function |  |  |
| `enableHapticFeedback` | Function | Erokhin Ilya | done |
| `enableNativeEmbedMode` | Function | Erokhin Ilya | done |
| `enableNativeMediaPlayer` | Function | Erokhin Ilya | done |
| `enableSmoothDragResize` | Function | Erokhin Ilya | done |
| `enableWebAVSession` | Function |  |  |
| `fileAccess` | Function | Erokhin Ilya | done |
| `forceDarkAccess` | Function | Erokhin Ilya | done |
| `forceDisplayScrollBar` | Function | Erokhin Ilya | done |
| `geolocationAccess` | Function | Erokhin Ilya | done |
| `horizontalScrollBarAccess` | Function | Erokhin Ilya | done |
| `imageAccess` | Function | Erokhin Ilya | done |
| `initialScale` | Function | Erokhin Ilya | done |
| `javaScriptAccess` | Function | Erokhin Ilya | done |
| `javaScriptOnDocumentEnd` | Function | Erokhin Ilya | done |
| `javaScriptOnDocumentStart` | Function | Erokhin Ilya | done |
| `javaScriptProxy` | Function | Erokhin Ilya | blocked IDL |
| `keyboardAvoidMode` | Function | Erokhin Ilya | done |
| `layoutMode` | Function | Erokhin Ilya | done |
| `mediaOptions` | Function | Erokhin Ilya | done |
| `mediaPlayGestureAccess` | Function | Erokhin Ilya | done |
| `metaViewport` | Function | Erokhin Ilya | done |
| `minFontSize` | Function | Erokhin Ilya | done |
| `minLogicalFontSize` | Function | Erokhin Ilya | done |
| `mixedMode` | Function | Erokhin Ilya | done |
| `multiWindowAccess` | Function | Erokhin Ilya | done |
| `nestedScroll` | Function | Erokhin Ilya | done |
| `onAdsBlocked` | Function | Erokhin Ilya | done |
| `onAlert` | Function | Maksimov Nikita | done |
| `onAudioStateChanged` | Function | Erokhin Ilya | done |
| `onBeforeUnload` | Function | Maksimov Nikita | done |
| `onClientAuthenticationRequest` | Function | Erokhin Ilya | done |
| `onConfirm` | Function | Maksimov Nikita | done |
| `onConsole` | Function | Maksimov Nikita | done |
| `onContextMenuHide` | Function | Erokhin Ilya | done |
| `onContextMenuShow` | Function | Maksimov Nikita | done |
| `onControllerAttached` | Function | Erokhin Ilya | done |
| `onDataResubmitted` | Function | Erokhin Ilya | done |
| `onDownloadStart` | Function | Erokhin Ilya | done |
| `onErrorReceive` | Function | Erokhin Ilya | done |
| `onFaviconReceived` | Function | Erokhin Ilya | in progress |
| `onFileSelectorShow` | Function | Erokhin Ilya | done |
| `onFirstContentfulPaint` | Function | Erokhin Ilya | done |
| `onFirstMeaningfulPaint` | Function | Erokhin Ilya | done |
| `onFullScreenEnter` | Function | Erokhin Ilya | done |
| `onFullScreenExit` | Function | Erokhin Ilya | done |
| `onGeolocationHide` | Function | Erokhin Ilya | done |
| `onGeolocationShow` | Function | Erokhin Ilya | done |
| `onHttpAuthRequest` | Function | Maksimov Nikita | done |
| `onHttpErrorReceive` | Function | Erokhin Ilya | done |
| `onIntelligentTrackingPreventionResult` | Function | Erokhin Ilya | done |
| `onInterceptKeyEvent` | Function | Maksimov Nikita | blocked IDL |
| `onInterceptKeyboardAttach` | Function | Maksimov Nikita | done |
| `onInterceptRequest` | Function | Maksimov Nikita | done |
| `onLargestContentfulPaint` | Function | Erokhin Ilya | done |
| `onLoadIntercept` | Function | Maksimov Nikita | done |
| `onNativeEmbedGestureEvent` | Function | Erokhin Ilya, Andrey Khudenkikh | done |
| `onNativeEmbedLifecycleChange` | Function | Erokhin Ilya, Andrey Khudenkikh | done |
| `onNativeEmbedVisibilityChange` | Function | Erokhin Ilya | done |
| `onNavigationEntryCommitted` | Function | Erokhin Ilya | done |
| `onOverScroll` | Function | Erokhin Ilya | done |
| `onOverrideUrlLoading` | Function | Maksimov Nikita | done |
| `onPageBegin` | Function | Erokhin Ilya | done |
| `onPageEnd` | Function | Erokhin Ilya | done |
| `onPageVisible` | Function | Erokhin Ilya | done |
| `onPermissionRequest` | Function | Erokhin Ilya | done |
| `onProgressChange` | Function | Erokhin Ilya | done |
| `onPrompt` | Function | Maksimov Nikita | done |
| `onRefreshAccessedHistory` | Function | Erokhin Ilya | done |
| `onRenderExited` | Function | Erokhin Ilya | done |
| `onRenderProcessNotResponding` | Function | Erokhin Ilya | done |
| `onRenderProcessResponding` | Function | Erokhin Ilya | done |
| `onRequestSelected` | Function | Erokhin Ilya | done |
| `onResourceLoad` | Function | Erokhin Ilya | done |
| `onSafeBrowsingCheckResult` | Function | Erokhin Ilya | done |
| `onScaleChange` | Function | Erokhin Ilya | done |
| `onScreenCaptureRequest` | Function | Erokhin Ilya | done |
| `onScroll` | Function | Erokhin Ilya | done |
| `onSearchResultReceive` | Function | Erokhin Ilya | done |
| `onShowFileSelector` | Function | Maksimov Nikita | done |
| `onSslErrorEvent` | Function | Erokhin Ilya | done |
| `onSslErrorEventReceive` | Function | Erokhin Ilya | done |
| `onSslErrorReceive` | Function | Erokhin Ilya | done |
| `onTitleReceive` | Function | Erokhin Ilya | done |
| `onTouchIconUrlReceived` | Function | Erokhin Ilya | done |
| `onUrlLoadIntercept` | Function | Maksimov Nikita | done |
| `onViewportFitChanged` | Function | Erokhin Ilya | done |
| `onWindowExit` | Function | Erokhin Ilya | done |
| `onWindowNew` | Function | Erokhin Ilya | done |
| `onlineImageAccess` | Function | Erokhin Ilya | done |
| `optimizeParserBudget` | Function | | |
| `overScrollMode` | Function | Erokhin Ilya | done |
| `overviewModeAccess` | Function | Erokhin Ilya | done |
| `password` | Function | Erokhin Ilya | done |
| `pinchSmooth` | Function | Erokhin Ilya | done |
| `registerNativeEmbedRule` | Function | Erokhin Ilya | done |
| `runJavaScriptOnDocumentEnd` | Function | | |
| `runJavaScriptOnDocumentStart` | Function | | |
| `runJavaScriptOnHeadEnd` | Function | | |
| `selectionMenuOptions` | Function | Erokhin Ilya | done |
| `tableData` | Function | Erokhin Ilya | done |
| `textAutosizing` | Function | Erokhin Ilya | done |
| `textZoomAtio` | Function | Erokhin Ilya | done |
| `textZoomRatio` | Function | Erokhin Ilya | done |
| `userAgent` | Function | Erokhin Ilya | done |
| `verticalScrollBarAccess` | Function | Erokhin Ilya | done |
| `webCursiveFont` | Function | Erokhin Ilya | done |
| `webFantasyFont` | Function | Erokhin Ilya | done |
| `webFixedFont` | Function | Erokhin Ilya | done |
| `webSansSerifFont` | Function | Erokhin Ilya | done |
| `webSerifFont` | Function | Erokhin Ilya | done |
| `webStandardFont` | Function | Erokhin Ilya | done |
| `wideViewModeAccess` | Function | Erokhin Ilya | done |
| `zoomAccess` | Function | Erokhin Ilya | done |
| *WebContextMenuParam* | *Class* | Erokhin Ilya | blocked |
| `existsImageContents` | Function | Erokhin Ilya | done |
| `getEditStateFlags` | Function | Erokhin Ilya | done |
| `getInputFieldType` | Function | Erokhin Ilya | blocked IDL |
| `getLinkUrl` | Function | Erokhin Ilya | blocked IDL |
| `getMediaType` | Function | Erokhin Ilya | blocked IDL |
| `getPreviewHeight` | Function | Erokhin Ilya | done |
| `getPreviewWidth` | Function | Erokhin Ilya | done |
| `getSelectionText` | Function | Erokhin Ilya | blocked IDL |
| `getSourceType` | Function | Erokhin Ilya | blocked IDL |
| `getSourceUrl` | Function | Erokhin Ilya | blocked IDL |
| `getUnfilteredLinkUrl` | Function | Erokhin Ilya | blocked IDL |
| `isEditable` | Function | Erokhin Ilya | done |
| `x` | Function | Erokhin Ilya | done |
| `y` | Function | Erokhin Ilya | done |
| *WebContextMenuResult* | *Class* | Erokhin Ilya | done |
| `closeContextMenu` | Function | Erokhin Ilya | done |
| `copy` | Function | Erokhin Ilya | done |
| `copyImage` | Function | Erokhin Ilya | done |
| `cut` | Function | Erokhin Ilya | done |
| `paste` | Function | Erokhin Ilya | done |
| `selectAll` | Function | Erokhin Ilya | done |
| *WebController* | *Class* | Erokhin Ilya | blocked |
| `accessBackward` | Function | Erokhin Ilya | done |
| `accessForward` | Function | Erokhin Ilya | done |
| `accessStep` | Function | Erokhin Ilya | done |
| `backward` | Function | Erokhin Ilya | done |
| `clearHistory` | Function | Erokhin Ilya | done |
| `deleteJavaScriptRegister` | Function | Erokhin Ilya | done |
| `forward` | Function | Erokhin Ilya | done |
| `getCookieManager` | Function | Erokhin Ilya | done |
| `getHitTest` | Function | Erokhin Ilya | blocked IDL |
| `loadData` | Function | Erokhin Ilya | done |
| `loadUrl` | Function | Erokhin Ilya | done |
| `onActive` | Function | Erokhin Ilya | done |
| `onInactive` | Function | Erokhin Ilya | done |
| `refresh` | Function | Erokhin Ilya | done |
| `registerJavaScriptProxy` | Function | Erokhin Ilya | blocked IDL |
| `requestFocus` | Function | Erokhin Ilya | done |
| `runJavaScript` | Function | Erokhin Ilya | done |
| `stop` | Function | Erokhin Ilya | done |
| `zoom` | Function | Erokhin Ilya | done |
| *WebCookie* | *Class* | Erokhin Ilya | done |
| `saveCookie` | Function | Erokhin Ilya | done |
| `setCookie` | Function | Erokhin Ilya | done |
| *WebKeyboardController* | *Class* | Erokhin Ilya | done |
| `close` | Function | Erokhin Ilya | done |
| `deleteBackward` | Function | Erokhin Ilya | done |
| `deleteForward` | Function | Erokhin Ilya | done |
| `insertText` | Function | Erokhin Ilya | done |
| `sendFunctionKey` | Function | Erokhin Ilya | done |
| *WebResourceError* | *Class* | Erokhin Ilya | blocked |
| `getErrorCode` | Function | Erokhin Ilya | done |
| `getErrorInfo` | Function | Erokhin Ilya | blocked IDL |
| *WebResourceRequest* | *Class* | Erokhin Ilya | blocked |
| `getRequestHeader` | Function | Erokhin Ilya | blocked IDL |
| `getRequestMethod` | Function | Erokhin Ilya | blocked IDL |
| `getRequestUrl` | Function | Erokhin Ilya | blocked IDL |
| `isMainFrame` | Function | Erokhin Ilya | done |
| `isRedirect` | Function | Erokhin Ilya | done |
| `isRequestGesture` | Function | Erokhin Ilya | done |
| *WebResourceResponse* | *Class* | Erokhin Ilya | blocked |
| `getReasonMessage` | Function | Erokhin Ilya | blocked IDL |
| `getResponseCode` | Function | Erokhin Ilya | done |
| `getResponseData` | Function | Erokhin Ilya | blocked IDL |
| `getResponseDataEx` | Function | Erokhin Ilya | blocked IDL |
| `getResponseEncoding` | Function | Erokhin Ilya | blocked IDL |
| `getResponseHeader` | Function | Erokhin Ilya | blocked IDL |
| `getResponseIsReady` | Function | Erokhin Ilya | done |
| `getResponseMimeType` | Function | Erokhin Ilya | blocked IDL |
| `setReasonMessage` | Function | Erokhin Ilya | done |
| `setResponseCode` | Function | Erokhin Ilya | done |
| `setResponseData` | Function | Erokhin Ilya | done |
| `setResponseEncoding` | Function | Erokhin Ilya | done |
| `setResponseHeader` | Function | Erokhin Ilya | done |
| `setResponseIsReady` | Function | Erokhin Ilya | done |
| `setResponseMimeType` | Function | Erokhin Ilya | done |
| *WindowScene* | *Component* | Dudkin Sergey | done |
| `attractionEffect` | Function | Dudkin Sergey | done |
| *XComponent* | *Component* | Tuzhilkin Ivan | blocked IDL |
| `enableAnalyzer` | Function | Tuzhilkin Ivan | done |
| `enableSecure` | Function | Tuzhilkin Ivan | done |
| `enableTransparentLayer` | Function |  |  |
| `hdrBrightness` | Function |  |  |
| `onDestroy` | Function | Tuzhilkin Ivan | done |
| `onLoad` | Function | Tuzhilkin Ivan | blocked IDL |
| *XComponentController* | *Class* | Tuzhilkin Ivan | blocked IDL |
| `getXComponentContext` | Function | Tuzhilkin Ivan | blocked IDL |
| `getXComponentSurfaceId` | Function | Tuzhilkin Ivan | blocked IDL |
| `getXComponentSurfaceRect` | Function | Tuzhilkin Ivan | blocked IDL |
| `getXComponentSurfaceRotation` | Function | Tuzhilkin Ivan | blocked IDL |
| `onSurfaceChanged` | Function | Tuzhilkin Ivan | blocked IDL |
| `onSurfaceCreated` | Function | Tuzhilkin Ivan | blocked IDL |
| `onSurfaceDestroyed` | Function | Tuzhilkin Ivan | blocked IDL |
| `setXComponentSurfaceRect` | Function | Tuzhilkin Ivan | done |
| `setXComponentSurfaceRotation` | Function | Tuzhilkin Ivan | done |
| `setXComponentSurfaceSize` | Function | Tuzhilkin Ivan | done |
| `startImageAnalyzer` | Function | Tuzhilkin Ivan | in progress |
| `stopImageAnalyzer` | Function | Tuzhilkin Ivan | done |