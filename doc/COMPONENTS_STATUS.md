| Component | Function | Owner | Status (done=merged **with** UT, blocked UT=merged **without** UT, blocked=blocked by IDL) | issue/comment | 
| --------- | -------- | ----- | ------ |------|
|*Root*| *Component* | Tuzhilkin Ivan | done | | implementation is created early by Nikolay Pisanov |
|*ComponentRoot*| *Component* | Tuzhilkin Ivan | done | | implementation is created early by Nikolay Pisanov |
|*AbilityComponent*| *Component* | Tuzhilkin Ivan | deprecated |  | deprecated |
|`setAbilityComponentOptions`| Function | Tuzhilkin Ivan | deprecated  |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|`onConnect`| Function | Tuzhilkin Ivan | deprecated |  | deprecated |
|`onDisconnect`| Function | Tuzhilkin Ivan | deprecated |  | deprecated |
|*AlphabetIndexer*| *Component* |Ekaterina Stepanova| done |  |  |
|`setAlphabetIndexerOptions`| Function |Ekaterina Stepanova| done |  | deprecated |
|`onSelected`| Function |Ekaterina Stepanova| done |  |  |
|`color`| Function |Ekaterina Stepanova| done |  |  |
|`selectedColor`| Function |Ekaterina Stepanova| done |  |  |
|`popupColor`| Function |Ekaterina Stepanova| done |  |  |
|`selectedBackgroundColor`| Function |Ekaterina Stepanova| done |  |  |
|`popupBackground`| Function |Ekaterina Stepanova| done |  |  |
|`popupSelectedColor`| Function |Ekaterina Stepanova| done |  |  |
|`popupUnselectedColor`| Function |Ekaterina Stepanova| done |  |  |
|`popupItemBackgroundColor`| Function |Ekaterina Stepanova| done |  |  |
|`usingPopup`| Function |Ekaterina Stepanova| done |  |  |
|`selectedFont`| Function |Ekaterina Stepanova| done |  |  |
|`popupFont`| Function |Ekaterina Stepanova| done |  |  |
|`popupItemFont`| Function |Ekaterina Stepanova| done |  |  |
|`itemSize`| Function |Ekaterina Stepanova| done |  |  |
|`font`| Function |Ekaterina Stepanova| done |  |  |
|`onSelect`| Function |Ekaterina Stepanova| done |  |  |
|`onRequestPopupData`| Function |Skroba Gleb| done |  |  |
|`onPopupSelect`| Function |Ekaterina Stepanova| done |  |  |
|`selected`| Function |Ekaterina Stepanova| done |  |  |
|`popupPosition`| Function |Ekaterina Stepanova| done |  |  |
|`autoCollapse`| Function |Ekaterina Stepanova| done |  |  |
|`popupItemBorderRadius`| Function |Ekaterina Stepanova| done |  |  |
|`itemBorderRadius`| Function |Ekaterina Stepanova| done |  |  |
|`popupBackgroundBlurStyle`| Function |Ekaterina Stepanova| done |  |  |
|`popupTitleBackground`| Function |Ekaterina Stepanova| done |  |  |
|`enableHapticFeedback`| Function |Ekaterina Stepanova| done |  |  |
|`alignStyle`| Function |Ekaterina Stepanova| done |  |  |
|`_onChangeEvent_selected`| Function | Erokhin Ilya | done |  | |
|*Animator*| *Component* | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`setAnimatorOptions`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`state`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`duration`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`curve`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`delay`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`fillMode`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`iterations`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`playMode`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`motion`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`onStart`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`onPause`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`onRepeat`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`onCancel`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`onFinish`| Function | Skroba Gleb | managed side |  |deprecated since 12 https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|`onFrame`| Function | Skroba Gleb | managed side |  |  | deprecated since 12 https://gitee.com/nikolay-igotti/idlize/issues/IB2ATJ |
|*Badge*| *Component* |Vadim Voronov | done |  |  |
|`setBadgeOptions`| Function |Vadim Voronov | done |  | |
|*Blank*| *Component* | Skroba Gleb | done |  |  |
|`setBlankOptions`| Function | Skroba Gleb | done |  |  |
|`color`| Function | Skroba Gleb | done |  |  |
|*Button*| *Component* | Evstigneev Roman | blocked IDL |  |  |
|`setButtonOptions`| Function | Evstigneev Roman | done |  |  |
|`type`| Function | Evstigneev Roman | done |  |  |
|`stateEffect`| Function |Evstigneev Roman | done |  |  |
|`buttonStyle`| Function |Evstigneev Roman | done |  |  |
|`controlSize`| Function |Evstigneev Roman | done |  |  |
|`role`| Function | Evstigneev Roman | done |  |  |
|`fontColor`| Function | Evstigneev Roman | done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IAUX6D, test in progress Evstigneev Roman |
|`fontSize`| Function | Evstigneev Roman | done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IAUX6D, test in progress Evstigneev Roman |
|`fontWeight`| Function |Evstigneev Roman | done |  |https://gitee.com/openharmony/arkui_ace_engine/issues/IAUX6D, test in progress Evstigneev Roman |
|`fontStyle`| Function |Evstigneev Roman | done |  |  |
|`fontFamily`| Function |Evstigneev Roman | done |  |  |
|`contentModifier`| Function |Evstigneev Roman | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG (+) |
|`labelStyle`| Function |Evstigneev Roman | done |  |  |
|`minFontScale`| Function | Kovalev Sergey | in progress |  | |
|`maxFontScale`| Function | Kovalev Sergey | in progress |  | |
|*Calendar*| *Component* | Maksimov Nikita | done |  |  |
|`setCalendarOptions`| Function | Maksimov Nikita | done |  |  |
|`showLunar`| Function | Maksimov Nikita | done |  |  |
|`showHoliday`| Function | Maksimov Nikita | done |  |  |
|`needSlide`| Function | Maksimov Nikita | done |  |  |
|`startOfWeek`| Function | Maksimov Nikita | done |  |  |
|`offDays`| Function | Maksimov Nikita | done |  |  |
|`direction`| Function | Maksimov Nikita | done |  |  |
|`currentDayStyle`| Function | Maksimov Nikita | done |  |  |
|`nonCurrentDayStyle`| Function | Maksimov Nikita | done |  |  |
|`todayStyle`| Function | Maksimov Nikita | done |  |  |
|`weekStyle`| Function | Maksimov Nikita | done |  |  |
|`workStateStyle`| Function | Maksimov Nikita | done |  |  |
|`onSelectChange`| Function | Maksimov Nikita | done |  | |
|`onRequestData`| Function | Maksimov Nikita | done |  | |
|*CalendarPicker*| *Component* |Politov Mikhail | done |  |  |
|`setCalendarPickerOptions`| Function |Politov Mikhail | done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IB7RNZ |
|`textStyle`| Function |Politov Mikhail | done |  |  |
|`onChange`| Function |Politov Mikhail | done |  |  |
|`edgeAlign`| Function |Politov Mikhail | done |  |  |
|*Canvas*| *Component* |Vadim Voronov, Evstigneev Roman | done |  |
|`setCanvasOptions`| Function |Vadim Voronov, Evstigneev Roman | blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IAZ229 (+) |
|`onReady`| Function |Vadim Voronov | done |  |  |
|`enableAnalyzer`| Function |Vadim Voronov | done |  |  |
|*Checkbox*| *Component* | Andrey Khudenkikh | blocked IDL |  |  |
|`setCheckboxOptions`| Function | Samarin Sergey | done |  | |
|`select`| Function | Andrey Khudenkikh | done |  |  |
|`selectedColor`| Function | Andrey Khudenkikh | done |  |  |
|`shape`| Function | Andrey Khudenkikh | done |  |  |
|`unselectedColor`| Function | Andrey Khudenkikh | done |  |  |
|`mark`| Function | Andrey Khudenkikh | done |  |  |
|`onChange`| Function | Andrey Khudenkikh | done |  |  |
|`contentModifier`| Function | Andrey Khudenkikh | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG |
|`_onChangeEvent_select`| Function | Erokhin Ilya | done |  | |
|*CheckboxGroup*| *Component* | Dudkin Sergey| done |  |  |
|`setCheckboxGroupOptions`| Function | Dudkin Sergey| done |  |  |
|`selectAll`| Function | Dudkin Sergey | done |  |  |
|`selectedColor`| Function | Dudkin Sergey | done |  |  |
|`unselectedColor`| Function | Dudkin Sergey | done |  |  |
|`mark`| Function | Dudkin Sergey | done |  |  |
|`onChange`| Function | Dudkin Sergey | done |  |  |
|`checkboxShape`| Function | Dudkin Sergey | done |  |  |
|`_onChangeEvent_selectAll`| Function | Erokhin Ilya | done |  | |
|*Circle*|*Component*| Erokhin Ilya | done |  |  |
|`setCircleOptions`|Function| Erokhin Ilya | done |  |  |
|*Column*| *Component* | Politov Mikhail | done |  |  |
|`setColumnOptions`| Function | Politov Mikhail | done |  |  |
|`alignItems`| Function | Politov Mikhail | done |  |  |
|`justifyContent`| Function | Politov Mikhail | done |  |  |
|`pointLight`| Function | Evstigneev Roman, Andrey Khudenkikh | done |  | UT by Evstigneev Roman |
|`reverse`| Function | Politov Mikhail | done |  |  |
|*ColumnSplit*| *Component* | Dmitry A Smirnov| done |  | |
|`setColumnSplitOptions`| Function | Dmitry A Smirnov| done |  |  |
|`resizeable`| Function | Dmitry A Smirnov| done |  |  |
|`divider`| Function | Dmitry A Smirnov| done |  |  |
|*CommonMethod*|*Component*|Skroba Gleb,Erokhin Ilya | in progress |  |  |
|`width`| Function | Roman Sedaikin | done |  | |
|`height`| Function | Roman Sedaikin | done |  | |
|`drawModifier`| Function | Erokhin Ilya | done |  | |
|`responseRegion`| Function | Skroba Gleb | done |  | |
|`mouseResponseRegion`| Function | Skroba Gleb | done |  | |
|`size`| Function | Roman Sedaikin | done |  | |
|`constraintSize`| Function | Roman Sedaikin | done |  | |
|`touchable`| Function | Roman Sedaikin | done |  | |
|`hitTestBehavior`| Function | Roman Sedaikin | done |  | |
|`onChildTouchTest`| Function | Skroba Gleb | done |  |   |
|`layoutWeight`| Function | Roman Sedaikin | done |  | |
|`chainWeight`| Function | Politov Mikhail | done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IBJW6H |
|`padding`| Function | Skroba Gleb | done |  | |
|`safeAreaPadding`| Function |Dmitry A Smirnov | done |  | |
|`margin`| Function | Skroba Gleb | done |  | |
|`backgroundColor`|Function| Skroba Gleb| done |  |  |
|`pixelRound`| Function | Skroba Gleb | done |  |  |
|`backgroundImageSize`| Function | Erokhin Ilya | done |  |  |
|`backgroundImagePosition`| Function | Erokhin Ilya | done |  |  |
|`backgroundEffect`| Function | Skroba Gleb | done |  |  |
|`backgroundImageResizable`| Function | Skroba Gleb | done |  | |
|`foregroundEffect`| Function | Skroba Gleb | done |  |  |
|`visualEffect`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBOT08 |
|`backgroundFilter`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|`foregroundFilter`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|`compositingFilter`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|`opacity`| Function | Roman Sedaikin | done |  | |
|`border`| Function | Roman Sedaikin | done |  | |
|`borderStyle`| Function | Roman Sedaikin | done |  | |
|`borderWidth`| Function | Roman Sedaikin | done |  | |
|`borderColor`| Function | Roman Sedaikin | done |  | |
|`borderRadius`| Function | Roman Sedaikin | done |  | |
|`borderImage`| Function | Roman Sedaikin | done |  | |
|`outline`| Function | Skroba Gleb | done |  | |
|`outlineStyle`| Function | Skroba Gleb | done |  | |
|`outlineWidth`| Function | Skroba Gleb | done |  | |
|`outlineColor`| Function | Skroba Gleb | done |  | |
|`outlineRadius`| Function | Skroba Gleb | done |  | |
|`foregroundColor`| Function | Roman Sedaikin, Erokhin Ilya | done |  | |
|`onClick`| Function | Roman Sedaikin, Maksimov Nikita, Pavelyev Ivan | done |  | EVENT |
|`onHover`| Function | Andrey Khudenkikh, Tuzhilkin Ivan | done |  | EVENT |
|`onAccessibilityHover`| Function | Andrey Khudenkikh, Pavelyev Ivan | done |  | UT by Vadim Voronov EVENT |
|`hoverEffect`| Function | Roman Sedaikin | done |  | |
|`onMouse`| Function | Kovalev Sergey | done |  | EVENT |
|`onTouch`| Function | Roman Sedaikin, Tuzhilkin Ivan | done |  | EVENT |
|`onKeyEvent`| Function | Erokhin Ilya, Maksimov Nikita, Pavelyev Ivan | done |  |   |
|`onDigitalCrown`| Function | | | | |
|`onKeyPreIme`| Function | Erokhin Ilya, Maksimov Nikita, Pavelyev Ivan | done |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX & https://gitee.com/nikolay-igotti/idlize/issues/IAPDBZ EVENT |
|`onKeyEventDispatch`| Function | | | | |
|`onFocusAxisEvent`| Function | | | | |
|`focusable`| Function | Roman Sedaikin | done |  | |
|`tabStop`| Function | | | | |
|`onFocus`| Function | Roman Sedaikin | done |  | |
|`onBlur`| Function | Roman Sedaikin | done |  | |
|`tabIndex`| Function | Dmitry A Smirnov| done |  | |
|`defaultFocus`| Function | Dmitry A Smirnov| done |  | |
|`groupDefaultFocus`| Function | Dmitry A Smirnov| done |  | |
|`focusOnTouch`| Function | Dmitry A Smirnov| done |  | |
|`focusBox`| Function | Dudkin Sergey | done |  | |
|`animation`| Function | managed side |  | |https://gitee.com/nikolay-igotti/idlize/issues/IAXHJP|
|`transition`| Function | Dmitry A Smirnov| done |  |transition(Ark_TransitionOptions) - deprecated, this case tesskipped|
|`motionBlur`| Function | Dmitry A Smirnov| done |  | |
|`brightness`| Function | Lobah Mikhail | done |  |  |
|`contrast`| Function | Lobah Mikhail | done |  | |
|`grayscale`| Function | Lobah Mikhail | done |  | |
|`colorBlend`| Function | Lobah Mikhail | done |  | |
|`saturate`| Function | Lobah Mikhail | done |  | |
|`sepia`| Function | Lobah Mikhail | done |  | |
|`invert`| Function | Lobah Mikhail |  done | |
|`hueRotate`| Function | Lobah Mikhail |  done | |
|`useShadowBatching`| Function | Lobah Mikhail | done |  | |
|`useEffect`| Function | Lobah Mikhail | done |  | |
|`renderGroup`| Function | Lobah Mikhail | done  | |
|`freeze`| Function | Lobah Mikhail | done |  | |
|`translate`| Function | Erokhin Ilya | done |  |  |
|`scale`| Function | Erokhin Ilya | done |  |  |
|`gridSpan`| Function | Lobah Mikhail | done |  |deprecated? |
|`gridOffset`| Function | Lobah Mikhail | done |  |deprecated? |
|`rotate`| Function | Dmitry A Smirnov| done |  | Dmitry A Smirnov|
|`transform`| Function | Lobah Mikhail | done |  | |
|`onAppear`| Function | Roman Sedaikin | done |  | |
|`onDisAppear`| Function | Roman Sedaikin | done |  | |
|`onAttach`| Function | Andrey Khudenkikh | done |  | |
|`onDetach`| Function | Andrey Khudenkikh | done |  | |
|`onAreaChange`| Function | Roman Sedaikin | done |  | |
|`visibility`| Function | Roman Sedaikin | done |  | |
|`flexGrow`| Function | Dmitry A Smirnov| done |  | |
|`flexShrink`| Function | Dmitry A Smirnov| done |  | |
|`flexBasis`| Function | Dmitry A Smirnov| done |  | |
|`alignSelf`| Function | Roman Sedaikin | done |  | |
|`displayPriority`| Function | Roman Sedaikin | done |  | |
|`zIndex`| Function | Roman Sedaikin | done |  | |
|`direction`| Function | Roman Sedaikin | done |  | |
|`align`| Function | Roman Sedaikin | done |  | |
|`position`| Function | Roman Sedaikin | done |  | |
|`markAnchor`| Function | Dmitry A Smirnov| done |  | |
|`offset`| Function | Skroba Gleb | done |  | |
|`enabled`| Function | Roman Sedaikin | done |  | |
|`useSizeType`| Function | Dmitry A Smirnov| done |  | deprecated, modifier is ready|
|`alignRules`| Function | Dmitry A Smirnov| done |  | |
|`aspectRatio`| Function | Roman Sedaikin | done |  | |
|`clickEffect`| Function | Lobah Mikhail | done |  | |
|`onDragStart`| Function | Skroba Gleb, Evstigneev Roman | done |  | It needs DragEventAccessor implemented to complete Unit tests, but it is empty C-API now. |
|`onDragEnter`| Function | Lobah Mikhail, Evstigneev Roman | done |  | EVENT |
|`onDragMove`| Function | Lobah Mikhail, Evstigneev Roman | done |  | EVENT |
|`onDragLeave`| Function | Lobah Mikhail, Evstigneev Roman | done |  | EVENT |
|`onDrop`| Function | Lobah Mikhail, Evstigneev Roman | done |  | EVENT |
|`onDragEnd`| Function | Lobah Mikhail, Evstigneev Roman | done |  | EVENT |
|`allowDrop`| Function | Lobah Mikhail | done |  | |
|`draggable`| Function | Lobah Mikhail | done |  | |
|`dragPreview`| Function | Lobah Mikhail | done |  | UT done Lobah Mikhail CustomBuilder |
|`onPreDrag`| Function | Lobah Mikhail | done |  | |
|`linearGradient`| Function | Roman Sedaikin | done |  | |
|`sweepGradient`| Function | Roman Sedaikin | done |  | |
|`radialGradient`| Function | Erokhin Ilya | done |  |  |
|`motionPath`| Function | Lobah Mikhail | done |  | |
|`shadow`| Function | Roman Sedaikin | done |  | |
|`clip`| Function | Dudkin Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAW4RH deprecated partially |
|`clipShape`| Function | Tuzhilkin Ivan | in progress |  | done on feature branch |
|`mask`| Function | Maksimov Nikita | done |  |deprecated |
|`maskShape`| Function |Tuzhilkin Ivan| in progress |  | done on feature branch |
|`key`| Function | Lobah Mikhail | done |  | |
|`id`| Function | Erokhin Ilya | done |  |  |
|`geometryTransition`| Function | Lobah Mikhail | done |  | |
|`stateStyles`| Function | Dudkin Sergey | blocked IDL |  | managed side https://gitee.com/rri_opensource/koala_projects/issues/IBOSCF |
|`restoreId`| Function | Lobah Mikhail | done |  | |
|`sphericalEffect`| Function | Lobah Mikhail | done |  | |
|`lightUpEffect`| Function | Lobah Mikhail | done |  | |
|`pixelStretchEffect`| Function | Lobah Mikhail | done |  | |
|`accessibilityGroup`| Function | Lobah Mikhail | done |  | |
|`accessibilityText`| Function | Lobah Mikhail | done |  | |
|`accessibilityNextFocusId`| Function | | | | |
|`accessibilityDefaultFocus`| Function | | | | |
|`accessibilityUseSamePage`| Function | | | | |
|`accessibilityRole`| Function | | | | |
|`onAccessibilityFocus`| Function | | | | |
|`accessibilityTextHint`| Function | Lobah Mikhail | done |  | |
|`accessibilityDescription`| Function | Lobah Mikhail | done |  | |
|`accessibilityLevel`| Function | Lobah Mikhail | done |  | |
|`accessibilityVirtualNode`| Function | Lobah Mikhail | done |  | UT done Lobah Mikhail CustomBuilder https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|`accessibilityChecked`| Function | Lobah Mikhail | done |  | |
|`accessibilitySelected`| Function | Lobah Mikhail | done |  | |
|`obscured`| Function |Dmitry A Smirnov | done |  | |
|`reuseId`| Function |Dmitry A Smirnov | blocked AceEngine |  | not implemented in ace_engine|
|`reuse`| Function | | | | |
|`renderFit`| Function | Dmitry A Smirnov| done |  | |
|`gestureModifier`| Function | Erokhin Ilya | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG |
|`backgroundBrightness`| Function | Skroba Gleb | done |  | |
|`onGestureJudgeBegin`| Function | Skroba Gleb, Maksimov Nikita | done |  | UT by Vadim Voronov  |
|`onGestureRecognizerJudgeBegin`| Function | Skroba Gleb, Maksimov Nikita | done |  | UT by Vadim Voronov  |
|`shouldBuiltInRecognizerParallelWith`| Function | Skroba Gleb | done |  |  |
|`monopolizeEvents`| Function | Erokhin Ilya | done |  | UT by Vadim Voronov |
|`onTouchIntercept`| Function | Andrey Khudenkikh, Tuzhilkin Ivan | done |  | EVENT |
|`onSizeChange`| Function | Dmitry A Smirnov| done |  | |
|`customProperty`| Function | Dmitry A Smirnov| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX + need clrify bridge implementation|
|`expandSafeArea`| Function | Dmitry A Smirnov| done |  |default value https://gitee.com/openharmony/arkui_ace_engine/issues/IBICVG |
|`background`| Function | Lobah Mikhail | done |  | UT done Lobah Mikhail  |
|`backgroundImage`| Function | Erokhin Ilya | done |  | |
|`backgroundBlurStyle`| Function | Skroba Gleb | done |  |  |
|`foregroundBlurStyle`| Function | Roman Sedaikin | done |  | |
|`focusScopeId`| Function | Dmitry A Smirnov| done |  | |
|`focusScopePriority`| Function | Dmitry A Smirnov| done |  | |
|`gesture`| Function | Dudkin Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBLDH2 |
|`priorityGesture`| Function | Dudkin Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBLDH2 |
|`parallelGesture`| Function | Dudkin Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBLDH2 |
|`blur`| Function | Roman Sedaikin | done |  | |
|`linearGradientBlur`| Function | Lobah Mikhail | done |  | |
|`systemBarEffect`| Function | Lobah Mikhail | done |  | |
|`backdropBlur`| Function | Berezin Kirill | done |  | |
|`sharedTransition`|Function|Skroba Gleb | done |  |  |
|`chainMode`| Function | Berezin Kirill | done |  | |
|`dragPreviewOptions`| Function | Erokhin Ilya | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBC7UD |
|`overlay`| Function | Lobah Mikhail | done |  | UT done Lobah Mikhail CustomBuilder |
|`blendMode`| Function | Lobah Mikhail | done |  | |
|`advancedBlendMode`| Function | Erokhin Ilya | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBC80Y |
|`bindPopup`| Function | Erokhin Ilya | done |  | UT in progress Morozov Sergey |
|`bindMenu`| Function | Erokhin Ilya | blocked IDL |  | SymbolGlyphModifier https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG  |
|`bindContextMenu`| Function | Erokhin Ilya, Evstigneev Roman | in progress |  | Ark_ContextMenuAnimationOptions https://gitee.com/nikolay-igotti/idlize/issues/IB6H50 |
|`bindContentCover`| Function | Erokhin Ilya | done |  | UT by Vadim Voronov |
|`bindSheet`| Function | Erokhin Ilya | done |  | UT in progress Vadim Voronov |
|`onVisibleAreaChange`| Function | Erokhin Ilya | done |  | |
|`keyboardShortcut`| Function | Erokhin Ilya | done |  | UT by Vadim Voronov |
|*CommonShapeMethod*|*Component*|Skroba Gleb| done |  |  |
|`stroke`|Function| Skroba Gleb | done |  |  |
|`fill`|Function| Skroba Gleb | done |  |  |
|`strokeDashOffset`| Function | Evstigneev Roman | done |  |  |
|`strokeLineCap`| Function | Evstigneev Roman | done |  |  |
|`strokeLineJoin`| Function | Evstigneev Roman | done |  |  |
|`strokeMiterLimit`| Function | Evstigneev Roman | done |  |  |
|`strokeOpacity`| Function | Evstigneev Roman | done |  |  |
|`fillOpacity`| Function | Evstigneev Roman | done |  |  |
|`strokeWidth`| Function | Evstigneev Roman | done |  |  |
|`antiAlias`| Function | Evstigneev Roman | done |  |  |
|`strokeDashArray`| Function | Evstigneev Roman | blocked AceEngine |  |https://gitee.com/openharmony/interface_sdk-js/issues/IAX8ZZ (+)|
|*Common*| *Component* | Maksimov Nikita | done |  | |
|`setCommonOptions`| Function | Maksimov Nikita | done |  | |
|*ScrollableCommonMethod*| *Component* | Samarin Sergey | blocked IDL |  |  |
|`scrollBar`| Function | Samarin Sergey | done |  |  |
|`scrollBarColor`| Function | Samarin Sergey | done |  |  |
|`scrollBarWidth`| Function | Samarin Sergey | done |  |  |
|`nestedScroll`| Function | Samarin Sergey | done |  |  |
|`enableScrollInteraction`| Function | Samarin Sergey | done |  |  |
|`friction`| Function | Samarin Sergey | done |  |  |
|`onScroll`| Function | Samarin Sergey | deprecated |  |
|`onWillScroll`| Function | Skroba Gleb | done |  |   |
|`onDidScroll`| Function | Berezin Kirill | in progress |  | https://gitee.com/nikolay-igotti/idlize/issues/IBB6U0 (+) |
|`onReachStart`| Function | Samarin Sergey | done |  | |
|`onReachEnd`| Function | Samarin Sergey | done |  | |
|`onScrollStart`| Function | Samarin Sergey | done |  | |
|`onScrollStop`| Function | Samarin Sergey | done |  | |
|`flingSpeedLimit`| Function | Samarin Sergey | done |  |  |
|`clipContent`| Function | Evstigneev Roman | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBBLGP (+) |
|`digitalCrownSensitivity`| Function | | | | |
|`edgeEffect`| Function | Samarin Sergey | done |  | |
|`fadingEdge`| Function | Samarin Sergey | done |  | |
|*Component3D*| *Component* |Kovalev Sergey | blocked IDL |  | |
|`setComponent3DOptions`| Function |Kovalev Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBC7UD (+) |
|`environment`| Function |Kovalev Sergey | done |  |  |
|`shader`| Function |Kovalev Sergey | done |  |  |
|`shaderImageTexture`| Function |Kovalev Sergey | done |  |  |
|`shaderInputBuffer`| Function |Kovalev Sergey | done |  |  |
|`renderWidth`| Function |Kovalev Sergey | done |  |  |
|`renderHeight`| Function |Kovalev Sergey | done |  |  |
|`customRender`| Function |Kovalev Sergey | done |  |  |
|*ContainerSpan*| *Component* | Tuzhilkin Ivan| done |  |  |
|`setContainerSpanOptions`| Function |Tuzhilkin Ivan| done |  |  |
|`textBackgroundStyle`| Function |Tuzhilkin Ivan| done |  |  |
|*Counter*| *Component* | Erokhin Ilya | done |  |  |
|`setCounterOptions`| Function | Erokhin Ilya | done |  |  |
|`onInc`| Function | Erokhin Ilya | done |  |  |
|`onDec`| Function | Erokhin Ilya | done |  |  |
|`enableDec`| Function | Erokhin Ilya | done |  |  |
|`enableInc`| Function | Erokhin Ilya | done |  |  |
|*DataPanel*| *Component* | Morozov Sergey | blocked IDL |  |  |
|`setDataPanelOptions`| Function | Morozov Sergey | done |  |  |
|`closeEffect`| Function | Morozov Sergey | done |  |  |
|`valueColors`| Function |Morozov Sergey | blocked IDL |  |https://gitee.com/nikolay-igotti/idlize/issues/IAW4DU|
|`trackBackgroundColor`| Function |Morozov Sergey | done |  |  |
|`strokeWidth`| Function | Morozov Sergey | done |  |  |
|`trackShadow`| Function |Morozov Sergey | blocked IDL |  |https://gitee.com/nikolay-igotti/idlize/issues/IAW4DU|
|`contentModifier`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG |
|*DatePicker*| *Component* | Vadim Voronov | done |  |  |
|`setDatePickerOptions`| Function | Vadim Voronov| done |  | |
|`lunar`| Function |Vadim Voronov | done |  |  |
|`disappearTextStyle`| Function | Vadim Voronov| done |  |  |
|`textStyle`| Function |Vadim Voronov | done |  |  |
|`selectedTextStyle`| Function |Vadim Voronov | done |  |  |
|`onChange`| Function | Vadim Voronov| done |  |deprecated?  |
|`onDateChange`| Function |Vadim Voronov | done |  | |
|`digitalCrownSensitivity`| Function | Vadim Voronov | done |  | |
|`_onChangeEvent_selected`| Function | Erokhin Ilya | done |  | |
|*Divider*| *Component* | Tuzhilkin Ivan | done |  |  |
|`setDividerOptions`| Function | Tuzhilkin Ivan| done |  |  |
|`vertical`| Function | Tuzhilkin Ivan | done |  |  |
|`color`| Function | Tuzhilkin Ivan | done |  |  |
|`strokeWidth`| Function | Tuzhilkin Ivan | done |  |  |
|`lineCap`| Function | Tuzhilkin Ivan | done |  |  |
|*EffectComponent*| *Component* | Ekaterina Stepanova | done |  | |
|`setEffectComponentOptions`| Function | Ekaterina Stepanova | done |  | |
|*Ellipse*| *Component* | Ekaterina Stepanova | done |  | |
|`setEllipseOptions`| Function | Ekaterina Stepanova | done |  | |
|*EmbeddedComponent*| *Component* | Ekaterina Stepanova | blocked IDL |  | |
|`setEmbeddedComponentOptions`| Function | Ekaterina Stepanova | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|`onTerminated`| Function | Ekaterina Stepanova | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|`onError`| Function |Skroba Gleb | in progress |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|*Flex*| *Component* | Kovalev Sergey | done |  |  |
|`setFlexOptions`| Function | Kovalev Sergey | done |  |  |
|`pointLight`| Function | Evstigneev Roman, Andrey Khudenkikh | done |  | UT by Evstigneev Roman |
|*FlowItem*| *Component* | Evstigneev Roman | done |  |  |
|`setFlowItemOptions`| Function | Evstigneev Roman | done |  |  |
|*FolderStack*| *Component* | Politov Mikhail | done |  |  |
|`setFolderStackOptions`| Function | Politov Mikhail | done |  |  |
|`alignContent`| Function | Politov Mikhail | done |  |  |
|`onFolderStateChange`| Function | Politov Mikhail | done |  |  |
|`onHoverStatusChange`| Function | Politov Mikhail | done |  |  |
|`enableAnimation`| Function | Politov Mikhail | done |  |  |
|`autoHalfFold`| Function | Politov Mikhail | done |  |  |
|*FormComponent*| *Component* | Vadim Voronov | blocked IDL |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IB78HF |
|`setFormComponentOptions`| Function | Vadim Voronov | done |  |   |
|`size`| Function | Vadim Voronov | blocked AceEngine |  | implemented with tests, https://gitee.com/openharmony/arkui_ace_engine/issues/IB78HF |
|`moduleName`| Function | Vadim Voronov | done |  | |
|`dimension`| Function | Vadim Voronov | done |  | |
|`allowUpdate`| Function | Vadim Voronov | done |  | |
|`visibility`| Function | Vadim Voronov | done |  | |
|`onAcquired`| Function | Vadim Voronov | blocked IDL |  | implemented with narrow range of ID, https://gitee.com/nikolay-igotti/idlize/issues/IB8I7X |
|`onError`| Function | Vadim Voronov | blocked IDL |  | implemented with narrow range of erroCode,https://gitee.com/nikolay-igotti/idlize/issues/IB8I7X |
|`onRouter`| Function | Vadim Voronov | deprecated |  | https://gitee.com/nikolay-igotti/idlize/issues/IB6H50, deprecated |
|`onUninstall`| Function | Vadim Voronov | blocked IDL |  | implemented with narrow range of ID, https://gitee.com/nikolay-igotti/idlize/issues/IB8I7X |
|`onLoad`| Function | Vadim Voronov | done |  | |
|*FormLink*| *Component* | Dmitry A Smirnov| done |  |  |
|`setFormLinkOptions`| Function | Dmitry A Smirnov| done |  |  |
|*Gauge*| *Component* | Maksimov Nikita | blocked IDL |  |  |
|`setGaugeOptions`| Function | Maksimov Nikita | done |  | |
|`value`| Function | Maksimov Nikita | done |  | |
|`startAngle`| Function | Maksimov Nikita | done |  | |
|`endAngle`| Function | Maksimov Nikita | done |  | |
|`colors`| Function | Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAW4DU + |
|`strokeWidth`| Function | Maksimov Nikita | done |  | |
|`description`| Function | Lobah Mikhail | done |  | UT done Lobah Mikhail CustomBuilder https://gitee.com/nikolay-igotti/idlize/issues/IAX81Q |
|`trackShadow`| Function | Maksimov Nikita | done |  |  |
|`indicator`| Function | Maksimov Nikita | done |  |  |
|`privacySensitive`| Function | Maksimov Nikita | done |  ||
|`contentModifier`| Function | Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG + |
|*Grid*|*Component*| Erokhin Ilya | blocked IDL |  |  |
|`setGridOptions`|Function| Erokhin Ilya | in progress |  |https://gitee.com/nikolay-igotti/idlize/issues/IAQIXM|
|`columnsTemplate`|Function| Erokhin Ilya | done |  |  |
|`rowsTemplate`|Function| Erokhin Ilya | done |  |  |
|`columnsGap`|Function| Erokhin Ilya | done |  |  |
|`rowsGap`|Function| Erokhin Ilya | done |  |  |
|`scrollBarWidth`|Function| Erokhin Ilya | done |  |  |
|`scrollBarColor`|Function| Erokhin Ilya | done |  |  |
|`scrollBar`|Function| Erokhin Ilya | done |  |  |
|`onScrollBarUpdate`|Function| Skroba Gleb | done |  | |
|`onScrollIndex`|Function| Erokhin Ilya | done |  |  |
|`cachedCount`|Function| Erokhin Ilya | done |  |  |
|`editMode`|Function| Erokhin Ilya | done |  |  |
|`multiSelectable`|Function| Erokhin Ilya | done |  |  |
|`maxCount`|Function| Erokhin Ilya | done |  |  |
|`minCount`|Function| Erokhin Ilya | done |  |  |
|`cellLength`|Function| Erokhin Ilya | done |  |  |
|`layoutDirection`|Function| Erokhin Ilya | done |  |  |
|`supportAnimation`|Function| Erokhin Ilya | done |  |  |
|`onItemDragStart`|Function| Skroba Gleb | done |  |  |
|`onItemDragEnter`|Function| Erokhin Ilya | done |  |  |
|`onItemDragMove`|Function| Erokhin Ilya | done |  |  |
|`onItemDragLeave`|Function| Erokhin Ilya | done |  |  |
|`onItemDrop`|Function| Erokhin Ilya | done |  |  |
|`nestedScroll`|Function| Erokhin Ilya | done |  |  |
|`enableScrollInteraction`|Function| Erokhin Ilya | done |  |  |
|`friction`|Function| Erokhin Ilya | done |  |  |
|`alignItems`| Function | Erokhin Ilya | done |  |  |
|`onScroll`|Function| Erokhin Ilya | done |  |deprecated?  |
|`onReachStart`|Function| Erokhin Ilya | done |  |  |
|`onReachEnd`|Function| Erokhin Ilya | done |  |  |
|`onScrollStart`|Function| Erokhin Ilya | done |  |  |
|`onScrollStop`|Function| Erokhin Ilya | done |  |  |
|`onScrollFrameBegin`|Function| Skroba Gleb | done |  |   |
|`edgeEffect`|Function| Erokhin Ilya | done |  |  |
|*GridItem*|*Component*| Erokhin Ilya | done |  |  |
|`setGridItemOptions`|Function| Erokhin Ilya | done |  |  |
|`rowStart`|Function| Erokhin Ilya | done |  |  |
|`rowEnd`|Function| Erokhin Ilya | done |  |  |
|`columnStart`|Function| Erokhin Ilya | done |  |  |
|`columnEnd`|Function| Erokhin Ilya | done |  |  |
|`forceRebuild`|Function| Erokhin Ilya | done |  |deprecated?  |
|`selectable`|Function| Erokhin Ilya | done |  |  |
|`selected`|Function| Erokhin Ilya | done |  |  |
|`onSelect`|Function| Erokhin Ilya | done |  |  |
|`_onChangeEvent_selected`| Function | Erokhin Ilya | done |  | |
|*GridCol*| *Component* | Lobah Mikhail| done |  |  |
|`setGridColOptions`| Function |Lobah Mikhail| done |  |  |
|`span`| Function |Lobah Mikhail| done |  |  |
|`gridColOffset`| Function |Lobah Mikhail| done |  |  |
|`order`| Function |Lobah Mikhail| done |  |  |
|*GridContainer*| *Component* |Lobah Mikhail| deprecated |  | deprecated |
|`setGridContainerOptions`| Function |Lobah Mikhail|deprecated |  | deprecated |
|*GridRow*| *Component* |Lobah Mikhail| done |  |  |
|`setGridRowOptions`| Function |Lobah Mikhail| done |  |  |
|`onBreakpointChange`| Function |Lobah Mikhail| done |  |  |
|`alignItems`| Function |Lobah Mikhail| done |  |  |
|*Hyperlink*| *Component* | Morozov Sergey | done |  |   |
|`setHyperlinkOptions`| Function | Morozov Sergey | done |  | |
|`color`| Function | Morozov Sergey | done |  | |
|*Image*| *Component* | Evstigneev Roman | in progress |  |  |
|`setImageOptions`| Function | Berezin Kirill | done |  | SetImageOptions2 unavailable https://gitee.com/openharmony/arkui_ace_engine/issues/IAZ229 |
|`alt`| Function | Evstigneev Roman | done |  | UT done Lobah Mikhail   |
|`matchTextDirection`| Function | Evstigneev Roman | done |  | |
|`fitOriginalSize`| Function | Evstigneev Roman | done |  | |
|`fillColor`| Function | Evstigneev Roman | done |  | |
|`objectFit`| Function |Berezin Kirill| done |  | |
|`imageMatrix`| Function | | | | |
|`objectRepeat`| Function | Evstigneev Roman | done |  | |
|`autoResize`| Function | Evstigneev Roman | done |  |   |
|`renderMode`| Function | Evstigneev Roman | done |  | |
|`dynamicRangeMode`| Function | Evstigneev Roman | done |  | test blocked by aceEngine https://gitee.com/openharmony/arkui_ace_engine/issues/IB1IEY, test in progress Evstgneev Roman |
|`interpolation`| Function | Evstigneev Roman | done |  | |
|`sourceSize`| Function | Evstigneev Roman | done |  | |
|`syncLoad`| Function | Evstigneev Roman | done |  | |
|`colorFilter`| Function | Evstigneev Roman | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAW4RH and https://gitee.com/nikolay-igotti/idlize/issues/IB0Y72 (+) |
|`copyOption`| Function | Evstigneev Roman | blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IBEEFF (+) |
|`draggable`| Function | Evstigneev Roman | done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IBEA6V (+) |
|`pointLight`| Function | Evstigneev Roman, Andrey Khudenkikh | done |  |    |
|`edgeAntialiasing`| Function | Evstigneev Roman | done |  | |
|`onComplete`| Function | Evstigneev Roman | done |  | |
|`onError`| Function | Evstigneev Roman | done |  | |
|`onFinish`| Function | Evstigneev Roman | done |  | |
|`enableAnalyzer`| Function | Evstigneev Roman | done |  | |
|`analyzerConfig`| Function | Evstigneev Roman | blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IB0Y51 (+) |
|`resizable`| Function | Evstigneev Roman | in progress |  | https://gitee.com/nikolay-igotti/idlize/issues/IB0Y7V |
|`privacySensitive`| Function | Evstigneev Roman | done |  | |
|`enhancedImageQuality`| Function | Evstigneev Roman | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB1ISB (+) |
|`orientation`| Function | Samarin Sergey | done |  | |
|*ImageAnimator*| *Component* | Pavelyev Ivan | done |  | |
|`setImageAnimatorOptions`| Function | Pavelyev Ivan | done |  | |
|`images`| Function | Pavelyev Ivan | done |  |  |
|`state`| Function | Pavelyev Ivan | done |  | |
|`duration`| Function | Pavelyev Ivan | done |  | |
|`reverse`| Function | Pavelyev Ivan | done |  | |
|`fixedSize`| Function | Pavelyev Ivan | done |  | |
|`preDecode`| Function | Pavelyev Ivan | done |  | deprecated |
|`fillMode`| Function | Pavelyev Ivan | done |  | |
|`iterations`| Function | Pavelyev Ivan | done |  | |
|`onStart`| Function | Pavelyev Ivan | done |  | |
|`onPause`| Function | Pavelyev Ivan | done |  | |
|`onRepeat`| Function | Pavelyev Ivan | done |  | |
|`onCancel`| Function | Pavelyev Ivan | done |  | |
|`onFinish`| Function | Pavelyev Ivan | done |  | |
|*ImageSpan*| *Component* | Politov Mikhail | blocked IDL |  |  |
|`setImageSpanOptions`| Function | Politov Mikhail | done |  | |
|`verticalAlign`| Function | Politov Mikhail | done |  |  |
|`colorFilter`| Function | Politov Mikhail | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB0Y72 | |
|`objectFit`| Function | Politov Mikhail | done |  |  |
|`onComplete`| Function | Politov Mikhail | done |  |  |
|`onError`| Function | Politov Mikhail | done |  |  |
|`alt`| Function | Politov Mikhail | done |  | |
|*Line*|*Component*|Dudkin Sergey| blocked AceEngine|  |  |
|`setLineOptions`|Function|Dudkin Sergey| done |  |  |
|`startPoint`|Function|Dudkin Sergey| blocked AceEngine |  | https://gitee.com/openharmony/interface_sdk-js/issues/IAXCBA + |
|`endPoint`|Function|Dudkin Sergey| blocked AceEngine |  | https://gitee.com/openharmony/interface_sdk-js/issues/IAXCBA + |
|*List*|*Component*|Morozov Sergey| done |  |  |
|`setListOptions`|Function|Morozov Sergey| done |  |  |
|`alignListItem`|Function|Morozov Sergey| done |  |  |
|`listDirection`|Function|Morozov Sergey| done |  |  |
|`scrollBar`|Function|Morozov Sergey| done |  |  |
|`contentStartOffset`|Function|Morozov Sergey| done |  |  |
|`contentEndOffset`|Function|Morozov Sergey| done |  |  |
|`divider`|Function|Morozov Sergey| done |  |  |
|`editMode`|Function|Morozov Sergey| done |  |deprecated  |
|`multiSelectable`|Function|Morozov Sergey| done |  |  |
|`cachedCount`|Function|Morozov Sergey| done |  |  |
|`chainAnimation`|Function|Morozov Sergey| done |  |  |
|`chainAnimationOptions`|Function|Morozov Sergey| done |  |  |
|`sticky`|Function|Morozov Sergey| done |  |  |
|`scrollSnapAlign`|Function|Morozov Sergey| done |  |  |
|`nestedScroll`|Function|Morozov Sergey| done |  |  |
|`enableScrollInteraction`|Function|Morozov Sergey| done |  |  |
|`friction`|Function|Morozov Sergey| done |  |  |
|`childrenMainSize`|Function|Morozov Sergey| done |  |  |
|`maintainVisibleContentPosition`|Function|Morozov Sergey| done |  |  |
|`onScroll`|Function|Morozov Sergey| done |  |deprecated  |
|`onScrollIndex`|Function|Morozov Sergey| done |  |  |
|`onScrollVisibleContentChange`|Function|Morozov Sergey| done |  |  |
|`onReachStart`|Function|Morozov Sergey| done |  |  |
|`onReachEnd`|Function|Morozov Sergey| done |  |  |
|`onScrollStart`|Function|Morozov Sergey| done |  |  |
|`onScrollStop`|Function|Morozov Sergey| done |  |  |
|`onItemDelete`|Function| Skroba Gleb | deprecated |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IBGUML |
|`onItemMove`|Function| Skroba Gleb | done |  |   |
|`onItemDragStart`|Function| Skroba Gleb | done |  |   |
|`onItemDragEnter`|Function|Morozov Sergey| done |  |  |
|`onItemDragMove`|Function|Morozov Sergey| done |  |  |
|`onItemDragLeave`|Function|Morozov Sergey| done |  |  |
|`onItemDrop`|Function|Morozov Sergey| done |  |  |
|`onScrollFrameBegin`|Function| Skroba Gleb | done |  |   |
|`lanes`|Function|Morozov Sergey| done |  |  |
|`edgeEffect`|Function|Morozov Sergey| done |  |  |
|*ListItem*|*Component*|Morozov Sergey| done |  |  |
|`setListItemOptions`|Function|Morozov Sergey| done |  | deprecated for SetListItemOptions1Impl |
|`sticky`|Function|Morozov Sergey| done |  |deprecated  |
|`editable`|Function|Morozov Sergey| done |  |deprecated  |
|`selectable`|Function|Morozov Sergey| done |  |  |
|`selected`|Function|Morozov Sergey| done |  |  |
|`swipeAction`|Function|Samarin Sergey| done |  |  |
|`onSelect`|Function|Morozov Sergey| done |  |  |
|`_onChangeEvent_selected`| Function | Erokhin Ilya | done |  | |
|*ListItemGroup*|*Component*|Morozov Sergey| done |  |  |
|`setListItemGroupOptions`|Function|Dmitry A Smirnov | done |  |   |
|`divider`|Function|Morozov Sergey| done |  |  |
|`childrenMainSize`|Function|Morozov Sergey| done |  |  |
|*LoadingProgress*|*Component*| Samarin Sergey | done |  |  |
|`setLoadingProgressOptions`|Function| Samarin Sergey | done |  |  |
|`color`|Function| Samarin Sergey | done |  |  |
|`enableLoading`|Function| Samarin Sergey | done |  |  |
|`contentModifier`|Function| Samarin Sergey| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG + |
|*LocationButton*| *Component* |Samarin Sergey| done |  |  |
|`setLocationButtonOptions`| Function |Samarin Sergey| done |  |deprecated: icon, text, buttonType  |
|`onClick`| Function | Samarin Sergey, Maksimov Nikita, Pavelyev Ivan | done |  |   |
|*Marquee*| *Component* | Andrey Khudenkikh| done |  |  |
|`setMarqueeOptions`| Function | Andrey Khudenkikh| done |  |  |
|`fontColor`| Function |Andrey Khudenkikh | done |  |  |
|`fontSize`| Function |Andrey Khudenkikh | done |  |  |
|`allowScale`| Function |Andrey Khudenkikh | done |  |  |
|`fontWeight`| Function | Andrey Khudenkikh| done |  |  |
|`fontFamily`| Function | Andrey Khudenkikh| done |  |  |
|`marqueeUpdateStrategy`| Function |Andrey Khudenkikh | done |  |  |
|`onStart`| Function | Andrey Khudenkikh| done |  |  |
|`onBounce`| Function |Andrey Khudenkikh | done |  |  |
|`onFinish`| Function |Andrey Khudenkikh | done |  |  |
|*MediaCachedImage*| *Component* | Skroba Gleb, Evstgneev Roman | in progress |  | |
|`setMediaCachedImageOptions`| Function | Skroba Gleb, Evstgneev Roman | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX, https://gitee.com/nikolay-igotti/idlize/issues/IBC7UD |
|*Menu*|*Component*|Morozov Sergey| done |  |  |
|`setMenuOptions`| Function |Morozov Sergey| done |  |  |
|`fontSize`|Function|Morozov Sergey| done |  |deprecated  |
|`font`|Function|Morozov Sergey| done |  |  |
|`fontColor`|Function|Morozov Sergey| done |  |  |
|`radius`|Function|Morozov Sergey| done |  |  |
|`menuItemDivider`|Function|Morozov Sergey| done |  | |
|`menuItemGroupDivider`|Function|Morozov Sergey| done |  | |
|`subMenuExpandingMode`|Function|Morozov Sergey| done |  |  |
|*MenuItem*| *Component* |Morozov Sergey| blocked IDL |  |  |
|`setMenuItemOptions`| Function |Kovalev Sergey| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBC7UD + |
|`selected`| Function |Morozov Sergey| done |  |  |
|`selectIcon`| Function |Morozov Sergey| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBIKVB, PixelMap on https://gitee.com/nikolay-igotti/idlize/issues/IAU9UR |
|`onChange`| Function |Morozov Sergey| done |  |  |
|`contentFont`| Function |Morozov Sergey| done |  |  |
|`contentFontColor`| Function |Morozov Sergey| done |  |  |
|`labelFont`| Function |Morozov Sergey| done |  |  |
|`labelFontColor`| Function |Morozov Sergey| done |  |  |
|`_onChangeEvent_selected`| Function | Erokhin Ilya | done |  | |
|*MenuItemGroup*| *Component* |Morozov Sergey | done |  |  |
|`setMenuItemGroupOptions`| Function | Dmitry A Smirnov | done |  |   |
|*NavDestination*| *Component* |Kovalev Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`setNavDestinationOptions`| Function |Kovalev Sergey | done |  |   |
|`hideTitleBar`| Function |Kovalev Sergey | done |  |   |
|`hideBackButton`| Function | | | | |
|`onShown`| Function |Kovalev Sergey | done |  |  |
|`onHidden`| Function |Kovalev Sergey | done |  |   |
|`onBackPressed`| Function |Dudkin Sergey | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED |
|`onResult`| Function | | | | |
|`mode`| Function |Kovalev Sergey | done |  |   |
|`backButtonIcon`| Function |Kovalev Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, https://gitee.com/nikolay-igotti/idlize/issues/IAPDBZ |
|`menus`| Function |Kovalev Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, https://gitee.com/nikolay-igotti/idlize/issues/IAX81Q |
|`onReady`| Function |Kovalev Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`onWillAppear`| Function |Kovalev Sergey | done |  |  |
|`onWillDisappear`| Function |Kovalev Sergey | done |  |  |
|`onWillShow`| Function |Kovalev Sergey | done |  |   |
|`onWillHide`| Function | Kovalev Sergey | done |  |   |
|`systemBarStyle`| Function |Kovalev Sergey | blocked IDL |  |  https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG |
|`recoverable`| Function |Kovalev Sergey | done |  |   |
|`systemTransition`| Function |Kovalev Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y,  |
|`bindToScrollable`| Function | | | | |
|`bindToNestedScrollable`| Function | | | | |
|`title`| Function |Kovalev Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, https://gitee.com/nikolay-igotti/idlize/issues/IAX81Q |
|`toolbarConfiguration`| Function |Kovalev Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`hideToolBar`| Function | Kovalev Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`ignoreLayoutSafeArea`| Function |Kovalev Sergey | done |  |   |
|*NavRouter*| *Component* |Evstigneev Roman | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y (+) |
|`setNavRouterOptions`| Function |Evstigneev Roman| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, https://gitee.com/nikolay-igotti/idlize/issues/IAYQZF (+) |
|`onStateChange`| Function |Evstigneev Roman | done |  |deprecated  |
|`mode`| Function |Evstigneev Roman | done |  |deprecated  |
|*Navigator*| *Component* | Skroba Gleb| managed side |  |  |
|`setNavigatorOptions`| Function | Skroba Gleb | done |  |   |
|`active`| Function | Skroba Gleb | done |  |deprecated https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`type`| Function | Skroba Gleb | done |  |deprecated  |
|`target`| Function | Skroba Gleb | done |  |deprecated |
|`params`| Function | Skroba Gleb | blocked IDL |  | deprecated, https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|*NodeContainer*| *Component* | Skroba Gleb | blocked IDL |  | |
|`setNodeContainerOptions`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAYDN |
|*Panel*| *Component* | Pavelyev Ivan| done |  | deprecated  |
|`setPanelOptions`| Function | Pavelyev Ivan| done |  |  |
|`mode`| Function |Pavelyev Ivan| done |  |  |
|`type`| Function | Pavelyev Ivan | done |  | deprecated |
|`dragBar`| Function |Pavelyev Ivan| done |  |  |
|`customHeight`| Function |Pavelyev Ivan| done |  | deprecated |
|`fullHeight`| Function |Pavelyev Ivan| done |  |  |
|`halfHeight`| Function |Pavelyev Ivan| done |  |  |
|`miniHeight`| Function | Pavelyev Ivan| done |  |  |
|`show`| Function |Pavelyev Ivan| done |  |  |
|`backgroundMask`| Function |Pavelyev Ivan| done |  | deprecated |
|`showCloseIcon`| Function |Pavelyev Ivan| done |  |  |
|`onChange`| Function |Pavelyev Ivan| done |  |  |
|`onHeightChange`| Function |Pavelyev Ivan| done |  |  |
|`_onChangeEvent_mode`| Function | Erokhin Ilya | done |  | |
|*PasteButton*| *Component* | Samarin Sergey| done |  |  |
|`setPasteButtonOptions`| Function | Samarin Sergey| done |  |  |
|`onClick`| Function | Samarin Sergey, Maksimov Nikita, Pavelyev Ivan, Evstgneev Roman | done |  | EVENT, reopened after refactoring methods |
|*Path*| *Component* | Skroba Gleb | done |  |  |
|`setPathOptions`| Function | Skroba Gleb | done |  |  |
|`commands`| Function | Skroba Gleb | done |  |  |
|*PatternLock*| *Component* | Dmitry A Smirnov| in progress |  |  |
|`setPatternLockOptions`| Function | Dmitry A Smirnov| done |  |  |
|`sideLength`| Function | Dmitry A Smirnov| done |  |  |
|`circleRadius`| Function | Dmitry A Smirnov| done |  |  |
|`backgroundColor`| Function | Dmitry A Smirnov| done |  |common method |
|`regularColor`| Function | Dmitry A Smirnov| done |  |  |
|`selectedColor`| Function | Dmitry A Smirnov| done |  |  |
|`activeColor`| Function | Dmitry A Smirnov| done |  |  |
|`pathColor`| Function | Dmitry A Smirnov| done |  |  |
|`pathStrokeWidth`| Function | Dmitry A Smirnov| done |  |  |
|`onPatternComplete`| Function | Dmitry A Smirnov| done |  |  |
|`autoReset`| Function | Dmitry A Smirnov| done |  |  |
|`onDotConnect`| Function | Dmitry A Smirnov| done |  |  |
|`activateCircleStyle`| Function | Dmitry A Smirnov| done |  | |
|`skipUnselectedPoint`| Function | | | | |
|*PluginComponent*| *Component* | Evstigneev Roman | blocked IDL |  | |
|`setPluginComponentOptions`| Function | Evstigneev Roman | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB6H50 (+) |
|`onComplete`| Function | Evstigneev Roman | done |  | |
|`onError`| Function | Evstigneev Roman | done |  | |
|*Polygon*| *Component* |Politov Mikhail | blocked IDL |  | |
|`setPolygonOptions`| Function | Politov Mikhail | done |  | |
|`points`| Function | Politov Mikhail | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAGL8 |
|*Polyline*| *Component* | Politov Mikhail | blocked IDL |  |  |
|`setPolylineOptions`| Function | Politov Mikhail | done |  |  |
|`points`| Function | Politov Mikhail | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAGL8 |
|*Progress*| *Component* | Erokhin Ilya | blocked IDL |  | |
|`setProgressOptions`| Function | Erokhin Ilya | done |  | deprecated for `style` property |
|`value`| Function | Erokhin Ilya | done |  | |
|`color`| Function | Erokhin Ilya | done |  | |
|`style`| Function | Erokhin Ilya | done |  | UT by Vadim Voronov linearStyle.strokeRadius need to be tested |
|`privacySensitive`| Function | Erokhin Ilya | done |  | |
|`contentModifier`| Function | Erokhin Ilya | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG |
|*QRCode*| *Component* | Evstigneev Roman | done |  |  |
|`setQRCodeOptions`| Function |Evstigneev Roman | done |  |  |
|`color`| Function |Evstigneev Roman | done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IAZVOQ (+) or https://gitee.com/openharmony/arkui_ace_engine/issues/IBJUC2 |
|`backgroundColor`| Function |Evstigneev Roman | done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IAZVOQ (+) or https://gitee.com/openharmony/arkui_ace_engine/issues/IBJUC2 |
|`contentOpacity`| Function |Evstigneev Roman | done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IAZVOQ (+) or https://gitee.com/openharmony/arkui_ace_engine/issues/IBJUC2 |
|*Radio*| *Component* | Evstigneev Roman | done |  |  |
|`setRadioOptions`| Function | Dmitry A Smirnov | done |  | CustomBuilder |
|`checked`| Function | Evstigneev Roman | done |  |  |
|`onChange`| Function | Evstigneev Roman | done |  |  |
|`radioStyle`| Function | Evstigneev Roman | done |  |  |
|`contentModifier`| Function | Evstigneev Roman | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG (+) |
|`_onChangeEvent_checked`| Function | Erokhin Ilya | done |  | |
|*Rating*| *Component* | Lobah Mikhail| done |  |  |
|`setRatingOptions`| Function | Lobah Mikhail| done |  | |
|`stars`| Function | Lobah Mikhail| done |  | |
|`stepSize`| Function | Lobah Mikhail| done |  | |
|`starStyle`| Function | Lobah Mikhail| done |  | |
|`onChange`| Function | Lobah Mikhail| done |  |  |
|`contentModifier`| Function | Lobah Mikhail| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG |
|`_onChangeEvent_rating`| Function | Erokhin Ilya | done |  | |
|*Rect*|*Component*|Dudkin Sergey| done |  |  |
|`setRectOptions`|Function|Dudkin Sergey| done |  |  |
|`radiusWidth`|Function|Dudkin Sergey| done |  |  |
|`radiusHeight`|Function|Dudkin Sergey| done |  |  |
|`radius`|Function|Dudkin Sergey| done |  |   |
|*Refresh*| *Component* |Politov Mikhail | blocked IDL |  |  |
|`setRefreshOptions`| Function | Samarin Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBC7UD ; offset, friction - deprecated + |
|`onStateChange`| Function |Politov Mikhail | done |  |  |
|`onRefreshing`| Function |Politov Mikhail | done |  |  |
|`refreshOffset`| Function |Politov Mikhail | done |  |  |
|`pullToRefresh`| Function |Politov Mikhail | done |  |  |
|`onOffsetChange`| Function |Politov Mikhail | done |  | |
|`pullDownRatio`| Function |Politov Mikhail | done |  |  |
|`_onChangeEvent_refreshing`| Function | Erokhin Ilya | done |  | |
|*RelativeContainer*| *Component* | Dmitry A Smirnov | done |  |  |
|`setRelativeContainerOptions`| Function | Dmitry A Smirnov | done |  |  |
|`guideLine`| Function | Dmitry A Smirnov | done |  |  |
|`barrier`| Function | Dmitry A Smirnov | done |  |  |
|*RichEditor*| *Component* | Dudkin Sergey| blocked IDL |  |  |
|`setRichEditorOptions`| Function | Dudkin Sergey| done |  |  |
|`onReady`| Function | Dudkin Sergey| done |  |  |
|`onSelect`| Function | Dudkin Sergey| in progress |  | reopened because Ark_RichEditorSelection changed after generations |
|`onSelectionChange`| Function | Dudkin Sergey| done |  |  |
|`aboutToIMEInput`| Function | Dudkin Sergey| done |  |  |
|`onIMEInputComplete`| Function | Dudkin Sergey, Evstigneev Roman | done |  | reopened after refactoring methods |
|`onDidIMEInput`| Function | Dudkin Sergey| done |  |  |
|`aboutToDelete`| Function | Dudkin Sergey| done |  |  |
|`onDeleteComplete`| Function | Dudkin Sergey| done |  |  |
|`copyOptions`| Function | Dudkin Sergey| done |  |  |
|`onPaste`| Function | Dudkin Sergey| done |  |  |
|`enableDataDetector`| Function | Dudkin Sergey| done |  |  |
|`enablePreviewText`| Function | Dudkin Sergey| done |  |  |
|`dataDetectorConfig`| Function | Dudkin Sergey| done |  |  |
|`caretColor`| Function | Dudkin Sergey| done |  |  |
|`selectedBackgroundColor`| Function | Dudkin Sergey| done |  |  |
|`onEditingChange`| Function | Dudkin Sergey| done |  |  |
|`enterKeyType`| Function | Dudkin Sergey| done |  |  |
|`onSubmit`| Function | Dudkin Sergey, Evstigneev Roman | done |  | EVENT, reopened after refactoring methods |
|`onWillChange`| Function | Dudkin Sergey| done |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4G70, reopened beacuse Ark_RichEditorChangeValue changed after generations |
|`onDidChange`| Function | Dudkin Sergey| done |  |  |
|`onCut`| Function | Dudkin Sergey, Evstigneev Roman | done |  | reopened after refactoring methods |
|`onCopy`| Function | Dudkin Sergey, Evstigneev Roman | done |  | reopened after refactoring methods |
|`editMenuOptions`| Function | Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N + |
|`enableKeyboardOnFocus`| Function | Dudkin Sergey| done |  |  |
|`enableHapticFeedback`| Function | Dudkin Sergey| done |  |  |
|`barState`| Function | Dudkin Sergey| done |  |  |
|`maxLength`| Function | | | | |
|`maxLines`| Function | | | | |
|`bindSelectionMenu`| Function | Dmitry A Smirnov| done |  | CustomBuilder |
|`customKeyboard`| Function | Dmitry A Smirnov| done |  | CustomBuilder |
|`placeholder`| Function | Dudkin Sergey| done |  |  |
|*RichText*| *Component* | Dudkin Sergey| done |  |  |
|`setRichTextOptions`| Function | Dudkin Sergey| done |  |  |
|`onStart`| Function | Dudkin Sergey| done |  |  |
|`onComplete`| Function | Dudkin Sergey| done |  |  |
|*RootScene*| *Component* | Spirin Andrey | done |  | |
|`setRootSceneOptions`| Function | Spirin Andrey | done |  | |
|*Row*| *Component* | Andrey Khudenkikh | done |  |  |
|`setRowOptions`| Function | Andrey Khudenkikh | done |  |  |
|`alignItems`| Function | Andrey Khudenkikh | done |  |  |
|`justifyContent`| Function | Andrey Khudenkikh | done |  |  |
|`pointLight`| Function | Evstigneev Roman, Andrey Khudenkikh | done |  | UT by Evstigneev Roman |
|`reverse`| Function | Andrey Khudenkikh | done |  |  |
|*RowSplit*| *Component* | Dmitry A Smirnov| done |  | |
|`setRowSplitOptions`| Function | Dmitry A Smirnov| done |  |  |
|`resizeable`| Function | Dmitry A Smirnov| done |  |  |
|*SaveButton*| *Component* | Samarin Sergey| done |  |  |
|`setSaveButtonOptions`| Function | Samarin Sergey| done |  |  |
|`onClick`| Function | Samarin Sergey, Maksimov Nikita, Pavelyev Ivan | done |  | EVENT |
|*Screen*| *Component* | Dudkin Sergey | done |  | |
|`setScreenOptions`| Function | Dudkin Sergey | done |  | |
|*Scroll*| *Component* | Berezin Kirill | done |  |  |
|`setScrollOptions`| Function | Berezin Kirill | done |  |  |
|`scrollable`| Function | Berezin Kirill | done |  |  |
|`onScroll`| Function | Berezin Kirill | done |  |deprecated  |
|`onWillScroll`| Function | Berezin Kirill | done |  |   |
|`onDidScroll`| Function | Berezin Kirill | done |  |    |
|`onScrollEdge`| Function | Berezin Kirill | done |  |  |
|`onScrollStart`| Function | Berezin Kirill | done |  |  |
|`onScrollEnd`| Function | Berezin Kirill | done |  |deprecated  |
|`onScrollStop`| Function | Berezin Kirill | done |  |  |
|`scrollBar`| Function | Berezin Kirill | done |  |  |
|`scrollBarColor`| Function | Berezin Kirill | done |  |  |
|`scrollBarWidth`| Function | Berezin Kirill | done |  |  |
|`onScrollFrameBegin`| Function | Dudkin Sergey | done |  | |
|`nestedScroll`| Function | Berezin Kirill | done |  |  |
|`enableScrollInteraction`| Function | Berezin Kirill | done |  |  |
|`friction`| Function | Berezin Kirill | done |  |  |
|`scrollSnap`| Function | Berezin Kirill | done |  |  |
|`enablePaging`| Function | Berezin Kirill | done |  |  |
|`initialOffset`| Function | Berezin Kirill | done |  |  |
|`edgeEffect`| Function | Berezin Kirill | done |  |  |
|*ScrollBar*| *Component* | Maksimov Nikita | done |  | |
|`setScrollBarOptions`| Function | Maksimov Nikita | done |  | |
|`enableNestedScroll`| Function | Maksimov Nikita | done |  | |
|*Search*|*Component*| Evstigneev Roman | blocked IDL |  |  |
|`setSearchOptions`|Function| Evstigneev Roman | done |  |   |
|`fontColor`|Function| Evstigneev Roman | done |  |  |
|`searchIcon`|Function| Evstigneev Roman | blocked IDL |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IAWF91, https://gitee.com/nikolay-igotti/idlize/issues/IAYXQ8 (+)|
|`cancelButton`|Function| Evstigneev Roman | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBIKVB |
|`textIndent`|Function| Evstigneev Roman | done |  |  |
|`onEditChange`|Function| Evstigneev Roman | done |  |  |
|`selectedBackgroundColor`|Function| Evstigneev Roman | done |  |  |
|`caretStyle`|Function| Evstigneev Roman | done |  |  |
|`placeholderColor`|Function| Evstigneev Roman | done |  |  |
|`placeholderFont`|Function| Evstigneev Roman | done |  |  |
|`textFont`|Function| Evstigneev Roman | done |  |  |
|`enterKeyType`|Function| Evstigneev Roman | done |  |  |
|`onSubmit`|Function| Evstigneev Roman | done |  | EVENT |
|`onChange`|Function| Evstigneev Roman | done |  |  |
|`onTextSelectionChange`|Function| Evstigneev Roman | done |  |  |
|`onContentScroll`|Function| Evstigneev Roman | done |  |  |
|`onCopy`|Function| Evstigneev Roman | done |  |  |
|`onCut`|Function| Evstigneev Roman | done |  |  |
|`onPaste`|Function| Evstigneev Roman | done |  |  |
|`copyOption`|Function| Evstigneev Roman | done |  |  |
|`maxLength`|Function| Evstigneev Roman | done |  |  |
|`textAlign`|Function| Evstigneev Roman | done |  |  |
|`enableKeyboardOnFocus`|Function| Evstigneev Roman | done |  |  |
|`selectionMenuHidden`|Function| Evstigneev Roman | done |  |  |
|`minFontSize`|Function| Evstigneev Roman | done |  |  |
|`maxFontSize`|Function| Evstigneev Roman | done |  |  |
|`minFontScale`| Function | Kovalev Sergey | done |  | |
|`maxFontScale`| Function | Kovalev Sergey | done |  | |
|`decoration`|Function| Evstigneev Roman | done |  |  |
|`letterSpacing`|Function| Evstigneev Roman | done |  |  |
|`lineHeight`|Function| Evstigneev Roman | done |  |  |
|`type`|Function| Evstigneev Roman | done |  |  |
|`fontFeature`|Function| Evstigneev Roman | done |  |  |
|`onWillInsert`|Function| Skroba Gleb | done |  |   |
|`onDidInsert`|Function| Evstigneev Roman | done |  |  |
|`onWillDelete`|Function| Skroba Gleb | done |  |   |
|`onDidDelete`|Function| Evstigneev Roman | done |  |  |
|`editMenuOptions`|Function| Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N + |
|`enablePreviewText`|Function| Evstigneev Roman | done |  |  |
|`enableHapticFeedback`|Function|Evstigneev Roman| done |  |  |
|`halfLeading`| Function | Kovalev Sergey | done |  | |
|`stopBackPress`| Function | Kovalev Sergey | done |  | |
|`onWillChange`| Function | | | | |
|`searchButton`|Function| Evstigneev Roman | done |  |  |
|`inputFilter`|Function| Evstigneev Roman | in progress |  |  |
|`customKeyboard`|Function| Lobah Mikhail | done |  |   |
|`_onChangeEvent_value`| Function | Erokhin Ilya | done |  | |
|*SecurityComponentMethod*| *Component* | Samarin Sergey| in progress |  |  |
|`iconSize`| Function |Samarin Sergey| done |  |  |
|`layoutDirection`| Function |Samarin Sergey| done |  |  |
|`position`| Function |Samarin Sergey| done |  |  |
|`markAnchor`| Function |Samarin Sergey| done |  |  |
|`offset`| Function |Samarin Sergey| done |  |  |
|`fontSize`| Function |Samarin Sergey| done |  |  |
|`fontStyle`| Function |Samarin Sergey| done |  |  |
|`fontWeight`| Function |Samarin Sergey| done |  |  |
|`fontFamily`| Function |Samarin Sergey| done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IB29LJ |
|`fontColor`| Function |Samarin Sergey| done |  |  |
|`iconColor`| Function |Samarin Sergey| done |  |  |
|`backgroundColor`| Function |Samarin Sergey| done |  |  |
|`borderStyle`| Function |Samarin Sergey| done |  |  |
|`borderWidth`| Function |Samarin Sergey| done |  |  |
|`borderColor`| Function |Samarin Sergey| done |  |  |
|`borderRadius`| Function |Samarin Sergey| done |  |  |
|`padding`| Function |Samarin Sergey| done |  |  |
|`textIconSpace`| Function |Samarin Sergey| done |  |  |
|`key`| Function |Samarin Sergey| done |  |  |
|`width`| Function |Samarin Sergey| done |  |  |
|`height`| Function |Samarin Sergey| done |  |  |
|`size`| Function |Samarin Sergey| done |  |  |
|`constraintSize`| Function |Samarin Sergey| done |  |  |
|`align`| Function | Samarin Sergey | in progress |  | |
|`alignRules`| Function | Samarin Sergey | in progress |  | |
|`id`| Function | Samarin Sergey | in progress |  | |
|`minFontScale`| Function | Samarin Sergey | in progress |  | |
|`maxFontScale`| Function | Samarin Sergey | in progress |  | |
|`maxLines`| Function | Samarin Sergey | in progress |  | |
|`minFontSize`| Function | Samarin Sergey | in progress |  | |
|`maxFontSize`| Function | Samarin Sergey | in progress |  | |
|`heightAdaptivePolicy`| Function | Samarin Sergey | in progress |  | |
|`enabled`| Function | Samarin Sergey | in progress |  | |
|`chainMode`| Function | Samarin Sergey | in progress |  | |
|*Select*| *Component* | Samarin Sergey | blocked IDL |  |  |
|`setSelectOptions` | Function | Samarin Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBC7UD + |
|`selected` | Function | Samarin Sergey | done |  |  |
|`value` | Function | Samarin Sergey | done |  |  |
|`font` | Function | Samarin Sergey | done |  |  |
|`fontColor` | Function | Samarin Sergey | done |  |  |
|`selectedOptionBgColor` | Function | Samarin Sergey | done |  |  |
|`selectedOptionFont` | Function | Samarin Sergey | done |  |  |
|`selectedOptionFontColor` | Function | Samarin Sergey | done |  |  |
|`optionBgColor` | Function | Samarin Sergey | done |  |  |
|`optionFont` | Function | Samarin Sergey | done |  |  |
|`optionFontColor` | Function | Samarin Sergey | done |  |  |
|`onSelect` | Function | Samarin Sergey | done |  |  |
|`space` | Function | Samarin Sergey | done |  |  |
|`arrowPosition` | Function | Samarin Sergey | done |  |  |
|`optionWidth` | Function | Samarin Sergey | done |  |  |
|`optionHeight` | Function | Samarin Sergey | done |  |  |
|`menuBackgroundColor` | Function | Samarin Sergey | done |  |  |
|`menuBackgroundBlurStyle` | Function | Samarin Sergey | done |  |  |
|`controlSize` | Function | Samarin Sergey | done |  |  |
|`menuItemContentModifier` | Function | Samarin Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG + |
|`divider` | Function | Samarin Sergey | done |  |  |
|`textModifier`| Function | Samarin Sergey | in progress |  | |
|`arrowModifier`| Function | Samarin Sergey | in progress |  | |
|`menuAlign` | Function | Samarin Sergey | done |  |  |
|`_onChangeEvent_selected`| Function | Erokhin Ilya | done |  | |
|`_onChangeEvent_value`| Function | Erokhin Ilya | done |  | |
|*Shape*|*Component*|Dudkin Sergey| done |  |  |
|`setShapeOptions`|Function|Samarin Sergey| done |  | |
|`viewPort`|Function|Dudkin Sergey| done |  |  |
|`stroke`|Function|Dudkin Sergey| done |  |  |
|`fill`|Function|Dudkin Sergey| done |  |  |
|`strokeDashOffset`|Function|Dudkin Sergey| done |  |  |
|`strokeDashArray`|Function|Dudkin Sergey| blocked AceEngine |  | https://gitee.com/openharmony/interface_sdk-js/issues/IAX8ZZ |
|`strokeLineCap`|Function|Dudkin Sergey| done |  |  |
|`strokeLineJoin`|Function|Dudkin Sergey| done |  |  |
|`strokeMiterLimit`|Function|Dudkin Sergey| done |  |  |
|`strokeOpacity`|Function|Dudkin Sergey| done |  |  |
|`fillOpacity`|Function|Dudkin Sergey| done |  |  |
|`strokeWidth`|Function|Dudkin Sergey| done |  |  |
|`antiAlias`|Function|Dudkin Sergey| done |  |  |
|`mesh`|Function|Dudkin Sergey| blocked AceEngine |  |  | https://gitee.com/openharmony/interface_sdk-js/issues/IAX8ZZ + |
|*Slider*| *Component* |Morozov Sergey | blocked IDL |  |  |
|`setSliderOptions`| Function |Morozov Sergey | done |  |  |
|`blockColor`| Function |Morozov Sergey | done |  |  |
|`trackColor`| Function |Morozov Sergey | in progress | done on feature branch | |
|`selectedColor`| Function |Morozov Sergey |done  |  |
|`minLabel`| Function |Morozov Sergey | done |  |deprecated  |
|`maxLabel`| Function |Morozov Sergey | done |  |deprecated  |
|`showSteps`| Function |Morozov Sergey | done |  |  |
|`trackThickness`| Function |Morozov Sergey | done |  |  |
|`onChange`| Function |Morozov Sergey | done |  |  |
|`blockBorderColor`| Function |Morozov Sergey | done |  |  |
|`blockBorderWidth`| Function |Morozov Sergey | done |  |  |
|`stepColor`| Function |Morozov Sergey | done |  |  |
|`trackBorderRadius`| Function |Morozov Sergey | done |  |  |
|`selectedBorderRadius`| Function |Morozov Sergey | done |  |  |
|`blockSize`| Function |Morozov Sergey | done |  |  |
|`blockStyle`| Function |Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBLDH2 |
|`stepSize`| Function |Morozov Sergey | done |  |  |
|`sliderInteractionMode`| Function |Morozov Sergey | done |  |  |
|`minResponsiveDistance`| Function |Morozov Sergey | done |  |  |
|`contentModifier`| Function |Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG |
|`slideRange`| Function | Morozov Sergey | done |  |  |
|`digitalCrownSensitivity`| Function | | | | |
|`showTips`| Function |Morozov Sergey | done |  |  |
|`_onChangeEvent_value`| Function | Erokhin Ilya | done |  | |
|*BaseSpan*| *Component* |Politov Mikhail | done |  |  |
|`textBackgroundStyle`| Function | Politov Mikhail | done |  |  |
|`baselineOffset`| Function | Politov Mikhail | done |  |  |
|*Span*| *Component* | Politov Mikhail | done |  |  |
|`setSpanOptions`| Function |Politov Mikhail | done  |  |
|`font`| Function | Politov Mikhail | done |  |  |
|`fontColor`| Function |Politov Mikhail | done |  |  |
|`fontSize`| Function |Politov Mikhail | done |  |  |
|`fontStyle`| Function |Politov Mikhail | done |  |  |
|`fontWeight`| Function |Politov Mikhail | done |  |  |
|`fontFamily`| Function |Politov Mikhail | done |  |  |
|`decoration`| Function | Politov Mikhail | done |  |  |
|`letterSpacing`| Function |Politov Mikhail | done |  |  |
|`textCase`| Function | Politov Mikhail | done |  |  |
|`lineHeight`| Function | Politov Mikhail | done |  |  |
|`textShadow`| Function | Politov Mikhail | done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IB1K3Z |
|*Stack*| *Component* | Korobeinikov Evgeny | done |  |  |
|`setStackOptions` | Function | Korobeinikov Evgeny | done |  |  |
|`alignContent` | Function | Korobeinikov Evgeny | done |  |  |
|`pointLight` | Function | Evstigneev Roman, Andrey Khudenkikh | done |  |  UT by Evstigneev Roman |
|*Stepper*| *Component* | Morozov Sergey | done |  |  |
|`setStepperOptions`| Function | Morozov Sergey | done |  |  |
|`onFinish`| Function | Morozov Sergey | done |  |  |
|`onSkip`| Function | Morozov Sergey | done |  |  |
|`onChange`| Function | Morozov Sergey | done |  |  |
|`onNext`| Function | Morozov Sergey | done |  |  |
|`onPrevious`| Function | Morozov Sergey | done |  |  |
|`_onChangeEvent_index`| Function | Erokhin Ilya | done |  | |
|*StepperItem*| *Component* | Morozov Sergey | done |  | |
|`setStepperItemOptions`| Function | Morozov Sergey | done |  | |
|`prevLabel`| Function | Morozov Sergey | done |  | |
|`nextLabel`| Function | Morozov Sergey | done |  | |
|`status`| Function | Morozov Sergey | done |  | |
|*Swiper*| *Component* | Skroba Gleb | done |  |  |
|`setSwiperOptions`| Function | Skroba Gleb | done |  |  |
|`index`| Function | Skroba Gleb| done |  |  |
|`autoPlay`| Function | Skroba Gleb| done |  |  |
|`interval`| Function | Skroba Gleb| done |  |  |
|`indicator`| Function | Skroba Gleb| done |  |  |
|`loop`| Function | Skroba Gleb| done |  |  |
|`duration`| Function | Skroba Gleb | done |  |  |
|`vertical`| Function | Skroba Gleb | done |  |  |
|`itemSpace`| Function | Skroba Gleb | done |  |  |
|`displayMode`| Function | Skroba Gleb| done |  |  |
|`cachedCount`| Function | Skroba Gleb| done |  |  |
|`effectMode`| Function | Skroba Gleb | done |  |  |
|`disableSwipe`| Function | Skroba Gleb| done |  |  |
|`curve`| Function | Skroba Gleb| done |  |  |
|`onChange`| Function | Skroba Gleb| done |  |  |
|`indicatorStyle`| Function | Skroba Gleb| done |  |deprecated?  |
|`onAnimationStart`| Function | Skroba Gleb| done |  |  |
|`onAnimationEnd`| Function | Skroba Gleb | done |  |  |
|`onGestureSwipe`| Function | Skroba Gleb | done |  |  |
|`nestedScroll`| Function | Skroba Gleb| done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IB3ULZ |
|`customContentTransition`| Function | Skroba Gleb | done |  | |
|`onContentDidScroll`| Function | Skroba Gleb| done |  | |
|`indicatorInteractive`| Function | Skroba Gleb| done |  |  |
|`pageFlipMode`| Function | | | | |
|`displayArrow`| Function | Skroba Gleb| done |  |  |
|`displayCount`| Function |Skroba Gleb | done |  |  |
|`prevMargin`| Function | Skroba Gleb| done |  |  |
|`nextMargin`| Function | Skroba Gleb | done |  |  |
|`_onChangeEvent_index`| Function | Erokhin Ilya | done |  | |
|*IndicatorComponent*| *Component* | Andrey Khudenkikh | done |  |  |
|`setIndicatorComponentOptions`| Function | Andrey Khudenkikh | done |  |  |
|`initialIndex`| Function | Andrey Khudenkikh | done |  |  |
|`count`| Function | Andrey Khudenkikh | done |  |  |
|`style`| Function |  Andrey Khudenkikh | done |  |  |
|`loop`| Function |  Andrey Khudenkikh | done |  |  |
|`vertical`| Function | Andrey Khudenkikh | done |  |  |
|`onChange`| Function | Andrey Khudenkikh | done |  |  |
|*SymbolGlyph*| *Component* |Andrey Khudenkikh | in progress |  |  |
|`setSymbolGlyphOptions`| Function |Andrey Khudenkikh | done |  |  |
|`fontSize`| Function |Andrey Khudenkikh | done |  |  |
|`fontColor`| Function |Andrey Khudenkikh | done |  |  |
|`fontWeight`| Function |Andrey Khudenkikh | done |  |  |
|`effectStrategy`| Function |Andrey Khudenkikh | done |  |  |
|`renderingStrategy`| Function |Andrey Khudenkikh | done |  |  |
|`minFontScale`| Function | Kovalev Sergey | in progress  | |
|`maxFontScale`| Function | Kovalev Sergey | in progress  | |
|`symbolEffect`| Function | Andrey Khudenkikh | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBLU1H|
|*SymbolSpan*| *Component* |Dmitry A Smirnov| done |  |  |
|`setSymbolSpanOptions`| Function |Dmitry A Smirnov| done |  |   |
|`fontSize`| Function |Dmitry A Smirnov| done |  |  |
|`fontColor`| Function |Dmitry A Smirnov| done |  |  |
|`fontWeight`| Function |Dmitry A Smirnov| done |  |  |
|`effectStrategy`| Function |Dmitry A Smirnov| done |  |  |
|`renderingStrategy`| Function |Dmitry A Smirnov| done |  |  |
|*Tabs*| *Component* | Tuzhilkin Ivan | done |  |  |
|`setTabsOptions`| Function | Skroba Gleb | done |  |  |
|`vertical`| Function | Tuzhilkin Ivan | done |  |  |
|`barPosition`| Function | Tuzhilkin Ivan | done |  |  |
|`scrollable`| Function | Tuzhilkin Ivan | done |  |  |
|`barMode`| Function | Tuzhilkin Ivan | done |  |  |
|`barWidth`| Function | Tuzhilkin Ivan | done |  |  |
|`barHeight`| Function | Tuzhilkin Ivan | done  |  |
|`animationDuration`| Function | Tuzhilkin Ivan | done |  | |
|`animationMode`| Function | Tuzhilkin Ivan | done |  |  |
|`edgeEffect`| Function | Tuzhilkin Ivan | done |  |  |
|`onChange`| Function | Tuzhilkin Ivan | done |  |  |
|`onTabBarClick`| Function | Tuzhilkin Ivan | done |  |  |
|`onAnimationStart`| Function | Tuzhilkin Ivan | done |  |  |
|`onAnimationEnd`| Function | Tuzhilkin Ivan | done |  |  |
|`onGestureSwipe`| Function | Tuzhilkin Ivan | done |  |  |
|`fadingEdge`| Function | Tuzhilkin Ivan | done |  |  |
|`divider`| Function | Tuzhilkin Ivan | done |  |  |
|`barOverlap`| Function | Tuzhilkin Ivan | done |  |  |
|`barBackgroundColor`| Function | Tuzhilkin Ivan | done |  |  |
|`barGridAlign`| Function | Tuzhilkin Ivan | done |  |  |
|`customContentTransition`| Function | Dudkin Sergey | done |  |  |
|`barBackgroundBlurStyle`| Function | Tuzhilkin Ivan | done |  |  |
|`barBackgroundEffect`| Function | Tuzhilkin Ivan | done |  | |
|`pageFlipMode`| Function | | | | |
|`onContentWillChange`| Function | Dudkin Sergey | done |  | |
|`barModeScrollable`| Function | Tuzhilkin Ivan | done |  | |
|`_onChangeEvent_index`| Function | Erokhin Ilya | done |  | |
|*TabContent*| *Component* | Evstigneev Roman | done |  |  |
|`setTabContentOptions`| Function | Evstigneev Roman | done |  |  |
|`tabBar`| Function | Lobah Mikhail | in progress |  |  |
|`onWillShow`| Function |Evstigneev Roman | done |  |  |
|`onWillHide`| Function |Evstigneev Roman | done |  |  |
|*Text*| *Component* | Samarin Sergey | blocked IDL |  | |
|`setTextOptions`| Function | Kirill Kirichenko | done |  |  |
|`font`| Function |Samarin Sergey | done |  |  |
|`fontColor`| Function |Samarin Sergey | done |  |  |
|`fontSize`| Function |Samarin Sergey | done |  |  |
|`minFontSize`| Function |Samarin Sergey | done |  |  |
|`maxFontSize`| Function |Samarin Sergey | done |  |  |
|`minFontScale`| Function |Samarin Sergey | done |  |  |
|`maxFontScale`| Function |Samarin Sergey | done |  |  |
|`fontStyle`| Function |Samarin Sergey | done |  |  |
|`fontWeight`| Function |Samarin Sergey | done |  |  |
|`lineSpacing`| Function |Samarin Sergey | done |  |  |
|`textAlign`| Function |Samarin Sergey | done |  |  |
|`lineHeight`| Function |Samarin Sergey | done |  |  |
|`textOverflow`| Function |Samarin Sergey | done |  |  |
|`fontFamily`| Function |Samarin Sergey | done |  |  |
|`maxLines`| Function |Samarin Sergey | done |  |  |
|`decoration`| Function |Samarin Sergey | done |  |  |
|`letterSpacing`| Function |Samarin Sergey | done |  |  |
|`textCase`| Function |Samarin Sergey | done |  |  |
|`baselineOffset`| Function |Samarin Sergey | done |  |  |
|`copyOption`| Function |Samarin Sergey | done |  |  |
|`draggable`| Function |Samarin Sergey | done |  |  |
|`textShadow`| Function |Samarin Sergey | done |  |  |
|`heightAdaptivePolicy`| Function |Samarin Sergey | done |  |  |
|`textIndent`| Function |Samarin Sergey | done |  |  |
|`wordBreak`| Function | Samarin Sergey | done |  |  |
|`lineBreakStrategy`| Function |Samarin Sergey | done |  |  |
|`onCopy`| Function | Kirill Kirichenko | done |  |  |
|`caretColor`| Function |Samarin Sergey | done |  | |
|`selectedBackgroundColor`| Function |Samarin Sergey | done |  | |
|`ellipsisMode`| Function |Samarin Sergey | done |  |  |
|`enableDataDetector`| Function | Kirill Kirichenko | done |  |  |
|`dataDetectorConfig`| Function | Samarin Sergey | done |  | |
|`onTextSelectionChange`| Function | Kirill Kirichenko | done |  |  |
|`fontFeature`| Function |Samarin Sergey | done |  |  |
|`marqueeOptions`| Function | Samarin Sergey | done |  | |
|`onMarqueeStateChange`| Function | Samarin Sergey | done |  | |
|`privacySensitive`| Function |Samarin Sergey | done |  |  |
|`textSelectable`| Function |Samarin Sergey | done |  |  |
|`editMenuOptions`| Function | Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N + |
|`halfLeading`| Function |Samarin Sergey | done |  |  |
|`enableHapticFeedback`| Function |Samarin Sergey | done |  |  |
|`selection`| Function |Samarin Sergey | done |  | |
|`bindSelectionMenu`| Function | Lobah Mikhail | done |  |  |
|*TextArea*|*Component*|Tuzhilkin Ivan| blocked IDL |  |  |
|`setTextAreaOptions`|Function|Tuzhilkin Ivan| done |  | |
|`placeholderColor`|Function|Tuzhilkin Ivan| done |  | |
|`placeholderFont`|Function|Tuzhilkin Ivan| done |  | |
|`enterKeyType`|Function|Tuzhilkin Ivan| done |  |  |
|`textAlign`|Function|Tuzhilkin Ivan| done |  |  |
|`caretColor`|Function|Tuzhilkin Ivan| done |  |  |
|`fontColor`|Function|Tuzhilkin Ivan| done |  |  |
|`fontSize`|Function|Tuzhilkin Ivan| done |  |  |
|`fontStyle`|Function|Tuzhilkin Ivan| done |  |  |
|`fontWeight`|Function|Tuzhilkin Ivan| done |  |  |
|`fontFamily`|Function|Tuzhilkin Ivan| done |  |  |
|`textOverflow`|Function|Tuzhilkin Ivan| done |  |  |
|`textIndent`|Function|Tuzhilkin Ivan| done |  |  |
|`caretStyle`|Function|Tuzhilkin Ivan| done |  |  |
|`selectedBackgroundColor`|Function|Tuzhilkin Ivan| done |  | |
|`onSubmit`|Function|Tuzhilkin Ivan, Spirin Andrey| done |  |  |
|`onChange`|Function|Tuzhilkin Ivan| done |  |  |
|`onTextSelectionChange`|Function|Tuzhilkin Ivan| done |  | |
|`onContentScroll`|Function|Tuzhilkin Ivan| done |  |  |
|`onEditChange`|Function|Tuzhilkin Ivan| done |  |  |
|`onCopy`|Function|Tuzhilkin Ivan| done |  |  |
|`onCut`|Function|Tuzhilkin Ivan| done |  |  |
|`onPaste`|Function|Tuzhilkin Ivan| done |  | |
|`copyOption`|Function|Tuzhilkin Ivan| done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IBI2SU, after fix this issue need disable manual UT and enable generated UT |
|`enableKeyboardOnFocus`|Function|Tuzhilkin Ivan| done |  |  |
|`maxLength`|Function|Tuzhilkin Ivan| done |  |  |
|`style`|Function|Tuzhilkin Ivan| done |  |  |
|`barState`|Function|Tuzhilkin Ivan| done |  |  |
|`selectionMenuHidden`|Function|Tuzhilkin Ivan| done |  |  |
|`minFontSize`|Function|Tuzhilkin Ivan| done |  |  |
|`maxFontSize`|Function|Tuzhilkin Ivan| done |  |  |
|`minFontScale`| Function | Kovalev Sergey | done |  | |
|`maxFontScale`| Function | Kovalev Sergey | done |  | |
|`heightAdaptivePolicy`|Function|Tuzhilkin Ivan| done |  |  |
|`maxLines`|Function|Tuzhilkin Ivan| done |  |  |
|`wordBreak`|Function|Tuzhilkin Ivan| done |  |  |
|`lineBreakStrategy`|Function|Tuzhilkin Ivan| done |  |  |
|`decoration`|Function|Tuzhilkin Ivan| done |  |  |
|`letterSpacing`|Function|Tuzhilkin Ivan| done |  |  |
|`lineSpacing`|Function|Tuzhilkin Ivan| done |  | |
|`lineHeight`|Function|Tuzhilkin Ivan| done |  | |
|`type`|Function|Tuzhilkin Ivan| done |  |  |
|`enableAutoFill`|Function|Tuzhilkin Ivan| done |  |  |
|`contentType`|Function|Tuzhilkin Ivan| done |  |  |
|`fontFeature`|Function|Tuzhilkin Ivan| done |  |  |
|`onWillInsert`|Function| Skroba Gleb | done |  |   |
|`onDidInsert`|Function|Tuzhilkin Ivan| done |  |  |
|`onWillDelete`|Function| Skroba Gleb | done |  |   |
|`onDidDelete`|Function|Tuzhilkin Ivan| done |  |  |
|`editMenuOptions`|Function| Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N + |
|`enablePreviewText`|Function|Tuzhilkin Ivan| done |  |  |
|`enableHapticFeedback`|Function|Tuzhilkin Ivan| done |  | |
|`halfLeading`| Function | Kovalev Sergey | done |  | |
|`ellipsisMode`| Function | Kovalev Sergey | done |  | |
|`stopBackPress`| Function | Kovalev Sergey | done |  | |
|`onWillChange`| Function | | | | |
|`inputFilter`|Function|Tuzhilkin Ivan| done |  | |
|`showCounter`|Function|Tuzhilkin Ivan| done |  |   |
|`customKeyboard`|Function| Erokhin Ilya | done |  | UT by Vadim Voronov  |
|`_onChangeEvent_text`| Function | Erokhin Ilya | done |  | |
|*TextClock*| *Component* |Pavelyev Ivan| blocked IDL |  |  |
|`setTextClockOptions`| Function |Pavelyev Ivan| done |  |  |
|`format`| Function |Pavelyev Ivan| done |  |  |
|`onDateChange`| Function |Pavelyev Ivan| done |  |  |
|`fontColor`| Function |Pavelyev Ivan| done |  |  |
|`fontSize`| Function |Pavelyev Ivan| done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IB1J5L |
|`fontStyle`| Function |Pavelyev Ivan| done |  |  |
|`fontWeight`| Function |Pavelyev Ivan| done |  |  |
|`fontFamily`| Function |Pavelyev Ivan| done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IB1JRR |
|`textShadow`| Function |Pavelyev Ivan| done |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IB1K3Z |
|`fontFeature`| Function |Pavelyev Ivan| done |  |  |
|`contentModifier`| Function |Pavelyev Ivan| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG + |
|`dateTimeOptions`| Function |Pavelyev Ivan| blocked IDL |  | Ark_CustomObject https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX + |
|*TextInput*| *Component* | Spirin Andrey | in progress |  |  |
|`setTextInputOptions`| Function | Spirin Andrey | done |  | |
|`type`| Function | Spirin Andrey | done |  |  |
|`contentType`| Function | Spirin Andrey | done |  |  |
|`placeholderColor`| Function | Spirin Andrey | done |  |  |
|`textOverflow`| Function | Spirin Andrey | blocked AceEngine |  |https://gitee.com/openharmony/arkui_ace_engine/issues/IB57XU|
|`textIndent`| Function | Spirin Andrey | done |  |  |
|`placeholderFont`| Function | Spirin Andrey | done |  | |
|`enterKeyType`| Function | Spirin Andrey | done |  |  |
|`caretColor`| Function | Spirin Andrey | done |  |  |
|`onEditChanged`| Function | Spirin Andrey | done |  |deprecated UT Kovalev Sergey |
|`onEditChange`| Function | Spirin Andrey | done |  | UT Kovalev Sergey |
|`onSubmit`| Function | Spirin Andrey | done |  | EVENT |
|`onChange`| Function | Lobah Mikhail | done |  | UT done Lobah Mikhail  |
|`onTextSelectionChange`| Function | Spirin Andrey | done |  | UT Kovalev Sergey |
|`onContentScroll`| Function | Spirin Andrey | done |  | UT Kovalev Sergey |
|`maxLength`| Function | Spirin Andrey | done |  |  |
|`fontColor`| Function | Spirin Andrey | done |  |  |
|`fontSize`| Function | Spirin Andrey | done |  |  |
|`fontStyle`| Function | Spirin Andrey | done |  |  |
|`fontWeight`| Function | Spirin Andrey | done |  |  |
|`fontFamily`| Function | Spirin Andrey | done |  |  |
|`onCopy`| Function | Spirin Andrey | done |  | UT Kovalev Sergey |
|`onCut`| Function | Spirin Andrey | done |  | UT Kovalev Sergey |
|`onPaste`| Function | Lobah Mikhail | done |  | UT done Lobah Mikhail |
|`copyOption`| Function | Spirin Andrey | done |  |  |
|`showPasswordIcon`| Function | Spirin Andrey | done |  |  |
|`textAlign`| Function | Spirin Andrey | done |  |  |
|`style`| Function | Spirin Andrey | done |  |  |
|`caretStyle`| Function | Spirin Andrey | done |  |  |
|`selectedBackgroundColor`| Function | Spirin Andrey | done |  |  |
|`caretPosition`| Function | Spirin Andrey | done |  |  |
|`enableKeyboardOnFocus`| Function | Spirin Andrey | done |  |  |
|`passwordIcon`| Function | Spirin Andrey | done |  |  |
|`showError`| Function | Spirin Andrey | done |  |  |
|`showUnit`| Function | Erokhin Ilya | done |  | |
|`showUnderline`| Function | Spirin Andrey | done |  |  |
|`underlineColor`| Function | Spirin Andrey | done |  |  |
|`selectionMenuHidden`| Function | Spirin Andrey | done |  |  |
|`barState`| Function | Spirin Andrey | done |  |  |
|`maxLines`| Function | Spirin Andrey | done  |  |
|`wordBreak`| Function | Spirin Andrey | done |  |  |
|`lineBreakStrategy`| Function | Spirin Andrey | done |  |  |
|`cancelButton`| Function | Spirin Andrey, Andrey Khudenkikh | done |  |  |
|`selectAll`| Function | Spirin Andrey | done |  |  |
|`minFontSize`| Function | Spirin Andrey | done  |  |
|`maxFontSize`| Function | Spirin Andrey | done  |  |
|`minFontScale`| Function | Kovalev Sergey | done |  | |
|`maxFontScale`| Function | Kovalev Sergey | done |  | |
|`heightAdaptivePolicy`| Function | Spirin Andrey | done |  |  |
|`enableAutoFill`| Function | Spirin Andrey | done |  |  |
|`decoration`| Function | Spirin Andrey | done |  | |
|`letterSpacing`| Function | Spirin Andrey | done |  | |
|`lineHeight`| Function | Spirin Andrey | done |  | |
|`passwordRules`| Function | Spirin Andrey | done |  |  |
|`fontFeature`| Function | Spirin Andrey | done |  | |
|`showPassword`| Function | Spirin Andrey | done |  |  |
|`onSecurityStateChange`| Function | Spirin Andrey | done |  | UT Kovalev Sergey |
|`onWillInsert`| Function | Skroba Gleb | done |  |   |
|`onDidInsert`| Function | Spirin Andrey | done |  | UT Kovalev Sergey |
|`onWillDelete`| Function | Skroba Gleb | done |  |   |
|`onDidDelete`| Function | Spirin Andrey | done |  | UT Kovalev Sergey |
|`editMenuOptions`| Function | Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N + |
|`enablePreviewText`| Function | Spirin Andrey | done |  |  |
|`enableHapticFeedback`| Function | Spirin Andrey | done |  |  |
|`halfLeading`| Function | Kovalev Sergey | done |  | |
|`ellipsisMode`| Function |  Kovalev Sergey| done |  | |
|`stopBackPress`| Function | Kovalev Sergey | done |  | |
|`onWillChange`| Function | | | | |
|`inputFilter`| Function | Spirin Andrey | done |  | UT Kovalev Sergey |
|`customKeyboard`| Function | Lobah Mikhail | done |  |   |
|`showCounter`| Function | Spirin Andrey | blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IB3V0N |
|`_onChangeEvent_text`| Function | Erokhin Ilya | done |  | |
|*TextPicker*| *Component* |Ekaterina Stepanova | in progress |  |  |
|`setTextPickerOptions`| Function | Tuzhilkin Ivan| done |  |   |
|`defaultPickerItemHeight`| Function |Ekaterina Stepanova | done |  |  |
|`canLoop`| Function |Ekaterina Stepanova | done |  |  |
|`disappearTextStyle`|Function |Ekaterina Stepanova | done |  |  |
|`textStyle`| Function |Ekaterina Stepanova | done |  |  |
|`selectedTextStyle`| Function | Ekaterina Stepanova| done |  |  |
|`disableTextStyleAnimation`| Function | | | | |
|`defaultTextStyle`| Function | | | | |
|`onAccept`| Function |Ekaterina Stepanova | done |  |deprecated  |
|`onCancel`| Function |Ekaterina Stepanova | done |  |deprecated  |
|`onChange`| Function | Tuzhilkin Ivan| done |  |  |
|`onScrollStop`| Function | | | | |
|`onEnterSelectedArea`| Function | | | | |
|`selectedIndex`| Function |Ekaterina Stepanova | done |  |  |
|`divider`| Function |Ekaterina Stepanova | done |  |  |
|`gradientHeight`| Function | Ekaterina Stepanova| done |  |  |
|`enableHapticFeedback`| Function | | | | |
|`digitalCrownSensitivity`| Function | | | | |
|`_onChangeEvent_selected`| Function | Erokhin Ilya | done |  | |
|`_onChangeEvent_value`| Function | Erokhin Ilya | done |  | |
|*TextTimer*| *Component* |Ekaterina Stepanova| blocked IDL |  |  |
|`setTextTimerOptions`| Function |Ekaterina Stepanova| done |  |  |
|`format`| Function |Ekaterina Stepanova| done |  |  |
|`fontColor`| Function |Ekaterina Stepanova| done |  |  |
|`fontSize`| Function |Ekaterina Stepanova| done |  |  |
|`fontStyle`| Function | Ekaterina Stepanova| done |  |  |
|`fontWeight`| Function |Ekaterina Stepanova| done |  |  |
|`fontFamily`| Function |Ekaterina Stepanova| done |  |  |
|`onTimer`| Function |Ekaterina Stepanova| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB3V0H |
|`textShadow`| Function |Ekaterina Stepanova| blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IB2SZK |
|`contentModifier`| Function |Ekaterina Stepanova| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG |
|*TimePicker*| *Component* | Ekaterina Stepanova| blocked IDL |  |  |
|`setTimePickerOptions`| Function |Ekaterina Stepanova| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|`useMilitaryTime`| Function |Ekaterina Stepanova| done |  |  |
|`loop`| Function |Ekaterina Stepanova| done |  |  |
|`disappearTextStyle`| Function |Ekaterina Stepanova| done |  |  |
|`textStyle`| Function |Ekaterina Stepanova| done |  |  |
|`selectedTextStyle`| Function |Ekaterina Stepanova| done |  |  |
|`dateTimeOptions`| Function |Ekaterina Stepanova| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|`onChange`| Function |Ekaterina Stepanova| done |  |  |
|`onEnterSelectedArea`| Function | | | | |
|`enableHapticFeedback`| Function |Ekaterina Stepanova| done |  |  |
|`digitalCrownSensitivity`| Function | | | | |
|`enableCascade`| Function | | | | |
|`_onChangeEvent_selected`| Function | Erokhin Ilya | done |  | |
|*Toggle*| *Component* |Morozov Sergey |blocked IDL |  |
|`setToggleOptions`| Function |Morozov Sergey | done on feature branch | unblocked since AceEngine won't fix it | https://gitee.com/nikolay-igotti/idlize/issues/IB2SVB , https://gitee.com/openharmony/arkui_ace_engine/issues/IB5V55 |
|`onChange`| Function | Morozov Sergey| done |  |  |
|`contentModifier`| Function |Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG |
|`selectedColor`| Function | Morozov Sergey| done |  |  |
|`switchPointColor`| Function | Morozov Sergey| done |  |  |
|`switchStyle`| Function | Morozov Sergey| done |  |  |
|`_onChangeEvent_isOn`| Function | Erokhin Ilya | done |  | |
|*Video*| *Component* | Erokhin Ilya | blocked AceEngine|  |  |
|`setVideoOptions`| Function | Erokhin Ilya | blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IAZ229 |
|`muted`| Function | Erokhin Ilya | done |  |  |
|`autoPlay`| Function | Erokhin Ilya | done |  |  |
|`controls`| Function | Erokhin Ilya | done |  |  |
|`loop`| Function | Erokhin Ilya | done |  |  |
|`objectFit`| Function | Erokhin Ilya | done |  |  |
|`onStart`| Function | Erokhin Ilya | done |  |  |
|`onPause`| Function | Erokhin Ilya | done |  |  |
|`onFinish`| Function | Erokhin Ilya | done |  |  |
|`onFullscreenChange`| Function | Erokhin Ilya | done |  |  |
|`onPrepared`| Function | Erokhin Ilya | done |  |  |
|`onSeeking`| Function | Erokhin Ilya | done |  |  |
|`onSeeked`| Function | Erokhin Ilya | done |  |  |
|`onUpdate`| Function | Erokhin Ilya | done |  |  |
|`onError`| Function | Erokhin Ilya | done |  |  |
|`onStop`| Function | Erokhin Ilya | done |  |  |
|`enableAnalyzer`| Function | Erokhin Ilya | done |  |  |
|`analyzerConfig`| Function | Erokhin Ilya | blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IAZ229 |
|`surfaceBackgroundColor`| Function | | | | |
|`enableShortcutKey`| Function | | | | |
|*Web*| *Component* | Erokhin Ilya | blocked IDL |  | |
|`setWebOptions`| Function | Erokhin Ilya | blocked IDL |  | Ark_CustomObject https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|`javaScriptAccess`| Function | Erokhin Ilya | done |  | |
|`fileAccess`| Function | Erokhin Ilya | done |  | |
|`onlineImageAccess`| Function | Erokhin Ilya | done |  | |
|`domStorageAccess`| Function | Erokhin Ilya | done |  | |
|`imageAccess`| Function | Erokhin Ilya | done |  | |
|`mixedMode`| Function | Erokhin Ilya | done |  | |
|`zoomAccess`| Function | Erokhin Ilya | done |  | |
|`geolocationAccess`| Function | Erokhin Ilya | done |  | |
|`javaScriptProxy`| Function | Erokhin Ilya | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9OV |
|`password`| Function | Erokhin Ilya | done |  |deprecated |
|`cacheMode`| Function | Erokhin Ilya | done |  | |
|`darkMode`| Function | Erokhin Ilya | done |  | |
|`forceDarkAccess`| Function | Erokhin Ilya | done |  | |
|`mediaOptions`| Function | Erokhin Ilya | done |  | |
|`tableData`| Function | Erokhin Ilya | done |  |deprecated |
|`wideViewModeAccess`| Function | Erokhin Ilya | done |  |deprecated |
|`overviewModeAccess`| Function | Erokhin Ilya | done |  | |
|`overScrollMode`| Function | Erokhin Ilya | done |  | |
|`blurOnKeyboardHideMode`| Function | | | | |
|`textZoomAtio`| Function | Erokhin Ilya | done |  |deprecated |
|`textZoomRatio`| Function | Erokhin Ilya | done |  | |
|`databaseAccess`| Function | Erokhin Ilya | done |  | |
|`initialScale`| Function | Erokhin Ilya | done |  | |
|`userAgent`| Function | Erokhin Ilya | done |  |deprecated |
|`metaViewport`| Function | Erokhin Ilya | done |  | |
|`onPageEnd`| Function | Erokhin Ilya | done |  | |
|`onPageBegin`| Function | Erokhin Ilya | done |  | |
|`onProgressChange`| Function | Erokhin Ilya | done |  | |
|`onTitleReceive`| Function | Erokhin Ilya | done |  | |
|`onGeolocationHide`| Function | Erokhin Ilya | done |  | |
|`onGeolocationShow`| Function | Erokhin Ilya | done |  | |
|`onRequestSelected`| Function | Erokhin Ilya | done |  | |
|`onAlert`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`onBeforeUnload`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`onConfirm`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`onPrompt`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`onConsole`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`onErrorReceive`| Function | Erokhin Ilya | done |  | |
|`onHttpErrorReceive`| Function | Erokhin Ilya | done |  | |
|`onDownloadStart`| Function | Erokhin Ilya | done |  | |
|`onRefreshAccessedHistory`| Function | Erokhin Ilya | done |  | |
|`onUrlLoadIntercept`| Function | Erokhin Ilya, Maksimov Nikita | done |  |deprecated |
|`onSslErrorReceive`| Function | Erokhin Ilya | done |  | deprecated |
|`onRenderExited`| Function | Erokhin Ilya | done |  |deprecated |
|`onShowFileSelector`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`onFileSelectorShow`| Function | Erokhin Ilya | done |  | deprecated |
|`onResourceLoad`| Function | Erokhin Ilya | done |  | |
|`onFullScreenExit`| Function | Erokhin Ilya | done |  | |
|`onFullScreenEnter`| Function | Erokhin Ilya | done |  | |
|`onScaleChange`| Function | Erokhin Ilya | done |  | |
|`onHttpAuthRequest`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`onInterceptRequest`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`onPermissionRequest`| Function | Erokhin Ilya | done |  | |
|`onScreenCaptureRequest`| Function | Erokhin Ilya | done |  | |
|`onContextMenuShow`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`onContextMenuHide`| Function | Erokhin Ilya | done |  | deprecated|
|`mediaPlayGestureAccess`| Function | Erokhin Ilya | done |  | |
|`onSearchResultReceive`| Function | Erokhin Ilya | done |  | |
|`onScroll`| Function | Erokhin Ilya | done |  | |
|`onSslErrorEventReceive`| Function | Erokhin Ilya | done |  | |
|`onSslErrorEvent`| Function | Erokhin Ilya | done |  | |
|`onClientAuthenticationRequest`| Function | Erokhin Ilya | done |  | |
|`onWindowNew`| Function | Erokhin Ilya | done |  | |
|`onWindowExit`| Function | Erokhin Ilya | done |  | |
|`multiWindowAccess`| Function | Erokhin Ilya | done |  | |
|`onInterceptKeyEvent`| Function | Erokhin Ilya, Maksimov Nikita, Pavelyev Ivan | done |  |  |
|`webStandardFont`| Function | Erokhin Ilya | done |  | |
|`webSerifFont`| Function | Erokhin Ilya | done |  | |
|`webSansSerifFont`| Function | Erokhin Ilya | done |  | |
|`webFixedFont`| Function | Erokhin Ilya | done |  | |
|`webFantasyFont`| Function | Erokhin Ilya | done |  | |
|`webCursiveFont`| Function | Erokhin Ilya | done |  | |
|`defaultFixedFontSize`| Function | Erokhin Ilya | done |  | |
|`defaultFontSize`| Function | Erokhin Ilya | done |  | |
|`minFontSize`| Function | Erokhin Ilya | done |  | |
|`minLogicalFontSize`| Function | Erokhin Ilya | done |  | |
|`defaultTextEncodingFormat`| Function | Erokhin Ilya | done |  | UT by Vadim Voronov |
|`forceDisplayScrollBar`| Function | Erokhin Ilya | done |  | |
|`blockNetwork`| Function | Erokhin Ilya | done |  | |
|`horizontalScrollBarAccess`| Function | Erokhin Ilya | done |  | |
|`verticalScrollBarAccess`| Function | Erokhin Ilya | done |  | |
|`onTouchIconUrlReceived`| Function | Erokhin Ilya | done |  | |
|`onFaviconReceived`| Function | Erokhin Ilya | in progress |  | PixelMap on https://gitee.com/nikolay-igotti/idlize/issues/IAU9UR |
|`onPageVisible`| Function | Erokhin Ilya | done |  | |
|`onDataResubmitted`| Function | Erokhin Ilya | done |  | |
|`pinchSmooth`| Function | Erokhin Ilya | done |  | |
|`allowWindowOpenMethod`| Function | Erokhin Ilya | done |  | |
|`onAudioStateChanged`| Function | Erokhin Ilya | done |  | |
|`onFirstContentfulPaint`| Function | Erokhin Ilya | done |  | |
|`onFirstMeaningfulPaint`| Function | Erokhin Ilya | done |  | |
|`onLargestContentfulPaint`| Function | Erokhin Ilya | done |  | |
|`onLoadIntercept`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`onControllerAttached`| Function | Erokhin Ilya | done |  | |
|`onOverScroll`| Function | Erokhin Ilya | done |  | |
|`onSafeBrowsingCheckResult`| Function | Erokhin Ilya | done |  | |
|`onNavigationEntryCommitted`| Function | Erokhin Ilya | done |  | |
|`onIntelligentTrackingPreventionResult`| Function | Erokhin Ilya | done |  | |
|`javaScriptOnDocumentStart`| Function | Erokhin Ilya | done |  | UT in progress Andrey Khudenkikh |
|`javaScriptOnDocumentEnd`| Function | Erokhin Ilya | done |  | UT in progress Andrey Khudenkikh |
|`layoutMode`| Function | Erokhin Ilya | done |  | |
|`nestedScroll`| Function | Erokhin Ilya | done |  | UT in progress Sergey Dudkin |
|`enableNativeEmbedMode`| Function | Erokhin Ilya | done |  | |
|`onNativeEmbedLifecycleChange`| Function | Erokhin Ilya, Andrey Khudenkikh | done |  | |
|`onNativeEmbedVisibilityChange`| Function | Erokhin Ilya | done |  | |
|`onNativeEmbedGestureEvent`| Function | Erokhin Ilya, Andrey Khudenkikh, Tuzhilkin Ivan | done |  | Optional EventResult part is not covered in UT |
|`copyOptions`| Function | Erokhin Ilya | done |  | |
|`onOverrideUrlLoading`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`textAutosizing`| Function | Erokhin Ilya | done |  | |
|`enableNativeMediaPlayer`| Function | Erokhin Ilya | done |  | |
|`enableSmoothDragResize`| Function | Erokhin Ilya | done |  | |
|`onRenderProcessNotResponding`| Function | Erokhin Ilya | done |  | |
|`onRenderProcessResponding`| Function | Erokhin Ilya | done |  | |
|`selectionMenuOptions`| Function | Erokhin Ilya | done |  | UT in progress Andrey Khudenkikh |
|`onViewportFitChanged`| Function | Erokhin Ilya | done |  | |
|`onInterceptKeyboardAttach`| Function | Erokhin Ilya, Maksimov Nikita | done |  | |
|`onAdsBlocked`| Function | Erokhin Ilya | done |  | |
|`keyboardAvoidMode`| Function | Erokhin Ilya | done |  | |
|`editMenuOptions`| Function | Erokhin Ilya, Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N + |
|`enableHapticFeedback`| Function | Erokhin Ilya | done |  | |
|`enableFollowSystemFontWeight`| Function | | | | |
|`enableWebAVSession`| Function | | | | |
|`optimizeParserBudget`| Function | | | | |
|`runJavaScriptOnDocumentStart`| Function | | | | |
|`runJavaScriptOnDocumentEnd`| Function | | | | |
|`runJavaScriptOnHeadEnd`| Function | | | | |
|`registerNativeEmbedRule`| Function | Erokhin Ilya | done |  | |
|`bindSelectionMenu`| Function | Lobah Mikhail | done |  |   |
|*WindowScene*| *Component* | Dudkin Sergey | done |  | |
|`setWindowSceneOptions`| Function | Dudkin Sergey | done |  | |
|`attractionEffect`| Function | Dudkin Sergey  | done |  |  |
|*XComponent*| *Component* | Tuzhilkin Ivan | blocked IDL |  | |
|`setXComponentOptions`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB8FFO, https://gitee.com/openharmony/arkui_ace_engine/issues/IAZ229 (+)|
|`onLoad`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB7RSS (+) |
|`onDestroy`| Function | Tuzhilkin Ivan | done |  | |
|`enableAnalyzer`| Function | Tuzhilkin Ivan | done |  | |
|`enableSecure`| Function | Tuzhilkin Ivan | done |  | |
|`hdrBrightness`| Function | | | | |
|`enableTransparentLayer`| Function | | | | |
|*SideBarContainer*| *Component* |Dmitry A Smirnov|in progress|  |
|`setSideBarContainerOptions`| Function |Dmitry A Smirnov| done |  |  |
|`showSideBar`| Function |Dmitry A Smirnov| done |  |  |
|`controlButton`| Function |Dmitry A Smirnov| in progress |  | 1.parse PixelMap on from CustomObject https://gitee.com/nikolay-igotti/idlize/issues/IAU9UR 2. default values https://gitee.com/openharmony/arkui_ace_engine/issues/IAW40V |
|`showControlButton`| Function |Dmitry A Smirnov| done |  |  |
|`onChange`| Function |Dmitry A Smirnov| done |  |  |
|`sideBarWidth`| Function |Dmitry A Smirnov| done |  |  |
|`minSideBarWidth`| Function |Dmitry A Smirnov| done |  |  |
|`maxSideBarWidth`| Function |Dmitry A Smirnov| done |  |  |
|`autoHide`| Function |Dmitry A Smirnov| done |  |  |
|`sideBarPosition`| Function |Dmitry A Smirnov| done |  |  |
|`divider`| Function |Dmitry A Smirnov| done |  |  |
|`minContentWidth`| Function |Dmitry A Smirnov| done |  |  |
|`_onChangeEvent_showSideBar`| Function | Erokhin Ilya | done |  | |
|*RemoteWindow*| *Component* | Spirin Andrey, Evstigneev Roman | done |  | |
|`setRemoteWindowOptions`| Function | Spirin Andrey, Evstigneev Roman | blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IAZ229 (+) |
|*WaterFlow*| *Component* | Kovalev Sergey | done |  |  |
|`setWaterFlowOptions`| Function | Kovalev Sergey | done |  |  |
|`columnsTemplate`| Function | Kovalev Sergey | done |  |  |
|`itemConstraintSize`| Function | Kovalev Sergey | done |  |  |
|`rowsTemplate`| Function | Kovalev Sergey | done |  |  |
|`columnsGap`| Function | Kovalev Sergey | done |  |  |
|`rowsGap`| Function |Kovalev Sergey | done |  |  |
|`layoutDirection`| Function |Kovalev Sergey | done |  |  |
|`nestedScroll`| Function | Kovalev Sergey | done |  |  |
|`enableScrollInteraction`| Function | Kovalev Sergey | done |  |  |
|`friction`| Function | Kovalev Sergey | done |  |  |
|`cachedCount`| Function |Kovalev Sergey | done |  |  |
|`onReachStart`| Function | Kovalev Sergey | done |  | |
|`onReachEnd`| Function | Kovalev Sergey | done |  | |
|`onScrollFrameBegin`| Function | Dudkin Sergey | done |  | |
|`onScrollIndex`| Function | Kovalev Sergey | done |  | |
|*UIExtensionComponent*| *Component* | Tuzhilkin Ivan | blocked IDL |  | |
|`setUIExtensionComponentOptions`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBCGB5 ,  https://gitee.com/nikolay-igotti/idlize/issues/IAYQZF (+)|
|`onRemoteReady`| Function | Tuzhilkin Ivan | done |  | |
|`onReceive`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX (+) - still blocked, but ticket can be incorect after new generation|
|`onResult`| Function | Tuzhilkin Ivan | deprecated |  | done | |
|`onRelease`| Function | Tuzhilkin Ivan | deprecated |  | done | |
|`onError`| Function | Skroba Gleb | in progress |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX (+) - still blocked, but ticket can be incorect after new generation|
|`onTerminated`| Function | Tuzhilkin Ivan | done |  | |
|`onDrawReady`| Function | | | | |
|*LinearIndicator*| *Component* | Kovalev Sergey | done |  | depricated |
|`setLinearIndicatorOptions`| Function | Kovalev Sergey | done |  | depricated |
|`indicatorStyle`| Function | Kovalev Sergey | done |  | depricated |
|`indicatorLoop`| Function | Kovalev Sergey | done |  | depricated |
|`onChange`| Function | Kovalev Sergey | done |  | depricated |
|*AnimationExtender*| *Class* | | | | |
|`SetClipRect`| Function | | | | |
|`OpenImplicitAnimation`| Function | | | | |
|`CloseImplicitAnimation`| Function | | | | |
|`StartDoubleAnimation`| Function | | | | |
|`AnimationTranslate`| Function | | | | |
|*UnifiedData*| *Class* | Tuzhilkin Ivan | blocked IDL |  | |
|`hasType`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBDHFY , https://gitee.com/nikolay-igotti/idlize/issues/IBDHH7 (+)|
|`getTypes`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBDHFY , https://gitee.com/nikolay-igotti/idlize/issues/IBDHH7 (+)|
|*LazyForEachOps*| *Class* | managed side |  | | |
|`NeedMoreElements`| Function | managed side |  |  |
|`OnRangeUpdate`| Function | managed side |  | | |
|`SetCurrentIndex`| Function | managed side |  | | |
|`Prepare`| Function | managed side |  | | |
|`NotifyChange`| Function | | | | |
|*SystemOps*| *Class* | | | | |
|`StartFrame`| Function | | | | |
|`EndFrame`| Function | | | | |
|*DrawingCanvas*| *Class* | Dudkin Sergey | done |  | |
|`drawRect`| Function | Dudkin Sergey | done |  | |
|*LengthMetrics*| *Class* | Evstigneev Roman | in progress | | |
|`px`| Function | Evstigneev Roman | in progress | | done for feature branch |
|`vp`| Function | | | | |
|`fp`| Function | | | | |
|`percent`| Function | | | | |
|`lpx`| Function | | | | |
|`resource`| Function | Evstigneev Roman | in progress | | done for feature branch |
|`getUnit`| Function | Samarin Sergey | in progress | | done for feature branch |
|`setUnit`| Function | Samarin Sergey | in progress | | done for feature branch |
|`getValue`| Function | Samarin Sergey | blocked IDL | | https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setValue`| Function | Samarin Sergey | in progress | | done for feature branch |
|*ColorMetrics*| *Class* | | | | |
|`numeric`| Function | | | | |
|`rgba`| Function | | | | |
|`resourceColor`| Function | | | | |
|`blendColor`| Function | | | | |
|`getColor`| Function | | | | |
|`getRed`| Function | | | | |
|`getGreen`| Function | | | | |
|`getBlue`| Function | | | | |
|`getAlpha`| Function | | | | |
|*WebviewController*| *Class* | Politov Mikhail | in progress | | |
|`initializeWebEngine`| Function | Politov Mikhail | in progress | | |
|`loadUrl`| Function | Politov Mikhail | in progress | | |
|*GlobalScope_ohos_arkui_componentSnapshot*| *Class* | Dudkin Sergey| in progress| | |
|`get`| Function | Dudkin Sergey | in progress | | |
|*GlobalScope_ohos_arkui_performanceMonitor*| *Class* | Vadim Voronov | blocked IDL | | |
|`begin`| Function | Vadim Voronov | done | | |
|`end`| Function | Vadim Voronov | done | | |
|`recordInputEventTime`| Function | Vadim Voronov | blocked IDL | | implemented with UT https://gitee.com/nikolay-igotti/idlize/issues/IB8I7X |
|*CommonShape*| *Class* | Tuzhilkin Ivan | done | | Empty implementation is acceptable now. Can be reworked/deleted in future generated |
|`offset`| Function | Tuzhilkin Ivan | done | | Empty implementation is acceptable now. Can be reworked/deleted in future generated |
|`fill`| Function | Tuzhilkin Ivan | done | | Empty implementation is acceptable now. Can be reworked/deleted in future generated |
|`position`| Function | Tuzhilkin Ivan | done | | Empty implementation is acceptable now. Can be reworked/deleted in future generated |
|*BaseShape*| *Class* | Tuzhilkin Ivan | done | | Empty implementation is acceptable now. Can be reworked/deleted in future generated |
|`width`| Function | Tuzhilkin Ivan | done | | Empty implementation is acceptable now. Can be reworked/deleted in future generated |
|`height`| Function | Tuzhilkin Ivan | done | | Empty implementation is acceptable now. Can be reworked/deleted in future generated |
|`size`| Function | Tuzhilkin Ivan | done | | Empty implementation is acceptable now. Can be reworked/deleted in future generated |
|*RectShape*| *Class* | Samarin Sergey | in progress | | |
|`offset`| Function | Samarin Sergey | in progress | | |
|`fill`| Function | Samarin Sergey | in progress | | |
|`position`| Function | Samarin Sergey | in progress | | |
|`width`| Function | Samarin Sergey | in progress | | |
|`height`| Function | Samarin Sergey | in progress | | |
|`size`| Function | Samarin Sergey | in progress | | |
|`radiusWidth`| Function | Samarin Sergey | in progress | | |
|`radiusHeight`| Function | Samarin Sergey | in progress | | |
|`radius`| Function | Samarin Sergey | in progress | | |
|*CircleShape*| *Class* |Dudkin Sergey |in progress | | |
|`offset`| Function |Dudkin Sergey |in progress | | |
|`fill`| Function | Dudkin Sergey |in progress | | |
|`position`| Function |Dudkin Sergey |in progress | | |
|`width`| Function |Dudkin Sergey |in progress | | |
|`height`| Function |Dudkin Sergey |in progress | | |
|`size`| Function |Dudkin Sergey | in progress | | |
|*EllipseShape*| *Class* | Erokhin Ilya | in progress | | done on feature branch |
|`offset`| Function | Erokhin Ilya | in progress | | done on feature branch |
|`fill`| Function | Erokhin Ilya | in progress | | done on feature branch |
|`position`| Function | Erokhin Ilya | in progress | | done on feature branch |
|`width`| Function | Erokhin Ilya | in progress | | done on feature branch |
|`height`| Function | Erokhin Ilya | in progress | | done on feature branch |
|`size`| Function | Erokhin Ilya | in progress | | done on feature branch |
|*PathShape*| *Class* | Lobah Mikhail | in progress| | |
|`offset`| Function | Lobah Mikhail| in progress| | |
|`fill`| Function | Lobah Mikhail | in progress| | |
|`position`| Function | Lobah Mikhail| in progress| | |
|`commands`| Function | Lobah Mikhail| in progress| | |
|*GlobalScope_ohos_font*| *Class* | Pavelyev Ivan | in progress | | done on feature branch |
|`registerFont`| Function | Pavelyev Ivan | in progress | | done on feature branch |
|`getSystemFontList`| Function | Pavelyev Ivan | in progress | | done on feature branch |
|*ScaleSymbolEffect*| *Class* | Andrey Khudenkikh | in progress | | |
|`getScope`| Function | Andrey Khudenkikh | in progress | | |
|`setScope`| Function | Andrey Khudenkikh | in progress | | |
|`getDirection`| Function | Andrey Khudenkikh | in progress | | |
|`setDirection`| Function | Andrey Khudenkikh | in progress | | |
|*ReplaceSymbolEffect*| *Class* | Andrey Khudenkikh | in progress | | |
|`getScope`| Function | Andrey Khudenkikh | in progress | | |
|`setScope`| Function | Andrey Khudenkikh | in progress | | |
|*FrameNode*| *Class* | Tuzhilkin Ivan | done |  | |
|`isModifiable`| Function | Tuzhilkin Ivan | done |  | |
|`appendChild`| Function | Tuzhilkin Ivan | done |  | |
|`insertChildAfter`| Function | Tuzhilkin Ivan | done |  | |
|`removeChild`| Function | Tuzhilkin Ivan | done |  | |
|`clearChildren`| Function | Tuzhilkin Ivan | done |  | |
|`getChild`| Function | Tuzhilkin Ivan | done |  | |
|`getFirstChild`| Function | Tuzhilkin Ivan | done |  | |
|`getNextSibling`| Function | Tuzhilkin Ivan | done |  | |
|`getPreviousSibling`| Function | Tuzhilkin Ivan | done |  | |
|`getParent`| Function | Tuzhilkin Ivan | done |  | |
|`getChildrenCount`| Function | Tuzhilkin Ivan | done |  | |
|`dispose`| Function | Tuzhilkin Ivan | done |  | |
|`getOpacity`| Function | Morozov Sergey | in progress | |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getPositionToWindowWithTransform`| Function | Morozov Sergey | in progress | |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 https://gitee.com/nikolay-igotti/idlize/issues/IBLA1J|
|*PixelMap*| *Class* | Andrey Khudenkikh | blocked  | |
|`readPixelsToBufferSync`| Function | Andrey Khudenkikh | blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IBE98T |
|`writeBufferToPixels`| Function | Andrey Khudenkikh | blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IBE98T |
|`getIsEditable`| Function | Andrey Khudenkikh | blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IBE98T |
|`getIsStrideAlignment`| Function | Andrey Khudenkikh | blocked AceEngine |  | https://gitee.com/openharmony/arkui_ace_engine/issues/IBE98T |
|*NavExtender*| *Class* | | | | |
|`setUpdateStackCallback`| Function | | | | |
|*EventEmulator*| *Class* | Dmitry A Smirnov | blocked IDL |  |https://gitee.com/nikolay-igotti/idlize/issues/IBDNN0 |
|`emitClickEvent`| Function | Dmitry A Smirnov, Maksimov Nikita | done |  | https://gitee.com/nikolay-igotti/idlize/issues/IBDNN0 + EVENT |
|`emitTextInputEvent`| Function | Dmitry A Smirnov | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBDNN0 |
|*ActionSheet*| *Class* | Ekaterina Stepanova | blocked IDL |  | |
|`show`| Function | Ekaterina Stepanova | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBHHWA |
|*AlertDialog*| *Class* | Ekaterina Stepanova | blocked IDL |  | |
|`show`| Function | Ekaterina Stepanova | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBHHWA |
|*SpringProp*| *Class* | | | | |
|*SpringMotion*| *Class* | | | | |
|*FrictionMotion*| *Class* | | | | |
|*ScrollMotion*| *Class* | | | | |
|*CalendarController*| *Class* | Maksimov Nikita | done |  |  |
|`backToToday`| Function | Maksimov Nikita | done |  |  |
|`goTo`| Function | Maksimov Nikita | done |  |  |
|*CalendarPickerDialog*| *Class* | Ekaterina Stepanova | blocked IDL |  | |
|`show`| Function | Ekaterina Stepanova | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBHHWA |
|*CanvasGradient*| *Class* | Vadim Voronov | done |  | |
|`addColorStop`| Function | Vadim Voronov | done |  | |
|*CanvasPath*| *Class* | Kovalev Sergey | done |  |  |
|`arc`| Function | Kovalev Sergey | done |  |  |
|`arcTo`| Function | Kovalev Sergey | done |  |  |
|`bezierCurveTo`| Function | Kovalev Sergey | done |  |  |
|`closePath`| Function | Kovalev Sergey | done |  |  |
|`ellipse`| Function | Kovalev Sergey | done |  |  |
|`lineTo`| Function | Kovalev Sergey | done |  |  |
|`moveTo`| Function | Kovalev Sergey | done |  |  |
|`quadraticCurveTo`| Function | Kovalev Sergey | done |  |  |
|`rect`| Function | Kovalev Sergey | done |  |  |
|*Path2D*| *Class* | Vadim Voronov | done |  | |
|`addPath`| Function | Vadim Voronov | done |  | |
|*CanvasPattern*| *Class* | Andrey Khudenkikh | done |  |  |
|`setTransform`| Function | Andrey Khudenkikh | done |  |  |
|*ImageBitmap*| *Class* | Pavelyev Ivan | done |  | |
|`close`| Function | Pavelyev Ivan | done |  |  |
|`getHeight`| Function | Pavelyev Ivan | in progress |  | RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getWidth`| Function | Pavelyev Ivan |in progress |  | RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|*ImageData*| *Class* | Morozov Sergey | in progress | | |
|`getHeight`| Function | Morozov Sergey | in progress | | |
|`getWidth`| Function | Morozov Sergey | in progress | | |
|*RenderingContextSettings*| *Class* | Vadim Voronov | in progress | | |
|`getAntialias`| Function | Vadim Voronov | in progress | | implemented with UT on FB, doesn't present in Upstream |
|`setAntialias`| Function | Vadim Voronov | in progress | | implemented with UT on FB, doesn't present in Upstream |
|*CanvasRenderer*| *Class*  | Vadim Voronov | blocked |  | |
|`drawImage`| Function  | Vadim Voronov | blocked IDL |  | done on FB, https://gitee.com/nikolay-igotti/idlize/issues/IB4BTA |
|`beginPath`| Function  | Vadim Voronov | done |  | |
|`clip`| Function  | Vadim Voronov | done |  |   |
|`fill`| Function  | Vadim Voronov | done |  |   |
|`stroke`| Function  | Vadim Voronov | done |  | |
|`createLinearGradient`| Function  | Vadim Voronov | blocked IDL | |RETURN_VALUE, implemented with UT https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`createPattern`| Function  | Vadim Voronov | blocked IDL | |RETURN_VALUE, implemented with UT https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`createRadialGradient`| Function  | Vadim Voronov | blocked IDL | |RETURN_VALUE, implemented with UT https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`createConicGradient`| Function  | Vadim Voronov | blocked IDL | |RETURN_VALUE, implemented with UT https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`createImageData`| Function  | Vadim Voronov | blocked IDL | |RETURN_VALUE, implemented with UT https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getImageData`| Function  | Vadim Voronov | blocked IDL | |RETURN_VALUE, implemented with UT https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getPixelMap`| Function  | Vadim Voronov | blocked AceEngine |  |RETURN_VALUE, https://gitee.com/openharmony/arkui_ace_engine/issues/IBE98T && https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`putImageData`| Function  | Vadim Voronov | done  |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4BTA |
|`getLineDash`| Function  | Dudkin Sergey |  in progress | |RETURN_VALUE, RET_VAL_UNBLOCKED |
|`setLineDash`| Function  | Vadim Voronov | done |  | |
|`clearRect`| Function  | Vadim Voronov | done |  | |
|`fillRect`| Function  | Vadim Voronov | done |  | |
|`strokeRect`| Function  | Vadim Voronov | done |  | |
|`restore`| Function  | Vadim Voronov | done |  | |
|`save`| Function  | Vadim Voronov | done |  | |
|`fillText`| Function  | Vadim Voronov | done |  | |
|`measureText`| Function  | Dudkin Sergey | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED |
|`strokeText`| Function  | Vadim Voronov | done |  | |
|`getTransform`| Function  | Vadim Voronov | blocked IDL | |RETURN_VALUE, implemented with UT https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`resetTransform`| Function  | Vadim Voronov | done |  | |
|`rotate`| Function  | Vadim Voronov | done |  | |
|`scale`| Function  | Vadim Voronov | done |  | |
|`setTransform`| Function  | Vadim Voronov | done |  | |
|`transform`| Function  | Vadim Voronov | done |  | |
|`translate`| Function  | Vadim Voronov | done |  | |
|`setPixelMap`| Function  | Vadim Voronov | done |  |  |
|`transferFromImageBitmap`| Function  | Vadim Voronov | done |  | |
|`saveLayer`| Function  | Vadim Voronov | done |  | |
|`restoreLayer`| Function  | Vadim Voronov | done |  | |
|`reset`| Function  | Vadim Voronov | done |  | |
|`setLetterSpacing`| Function | | | | |
|`getGlobalAlpha`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setGlobalAlpha`| Function  | Vadim Voronov | done |  | |
|`getGlobalCompositeOperation`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setGlobalCompositeOperation`| Function  | Vadim Voronov | done |  | |
|`setFillStyle`| Function  | Vadim Voronov | done |  | |
|`setStrokeStyle`| Function  | Vadim Voronov | done |  | |
|`getFilter`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setFilter`| Function  | Vadim Voronov | done |  | |
|`getImageSmoothingEnabled`| Function  | Vadim Voronov | in progress |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setImageSmoothingEnabled`| Function  | Vadim Voronov | done |  | |
|`getImageSmoothingQuality`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setImageSmoothingQuality`| Function  | Vadim Voronov | done |  |   |
|`getLineCap`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setLineCap`| Function  | Vadim Voronov | done |  |  |
|`getLineDashOffset`| Function  | Vadim Voronov | blocked IDL |  | in progress on FB, to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setLineDashOffset`| Function  | Vadim Voronov | done |  | |
|`getLineJoin`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setLineJoin`| Function  | Vadim Voronov | done |  |   |
|`getLineWidth`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setLineWidth`| Function  | Vadim Voronov | done |  | |
|`getMiterLimit`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setMiterLimit`| Function  | Vadim Voronov | done |  | |
|`getShadowBlur`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setShadowBlur`| Function  | Vadim Voronov | done |  | |
|`getShadowColor`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setShadowColor`| Function  | Vadim Voronov | done |  | |
|`getShadowOffsetX`| Function  | Vadim Voronov blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setShadowOffsetX`| Function  | Vadim Voronov | done |  | |
|`getShadowOffsetY`| Function  | Vadim Voronov blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setShadowOffsetY`| Function  | Vadim Voronov | done |  | |
|`getDirection`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setDirection`| Function  | Vadim Voronov | done |  |   |
|`getFont`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setFont`| Function  | Vadim Voronov | done |  | |
|`getTextAlign`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setTextAlign`| Function  | Vadim Voronov | done |  |   |
|`getTextBaseline`| Function  | Vadim Voronov | blocked IDL |  | to be removed from generation, https://gitee.com/nikolay-igotti/idlize/issues/IBP7O2 |
|`setTextBaseline`| Function  | Vadim Voronov | done |  |   |
|*CanvasRenderingContext2D*| *Class* | Vadim Voronov, Evstigneev Roman | done |  |  |
|`toDataURL`| Function |Vadim Voronov| done |  |  |
|`startImageAnalyzer`| Function | Vadim Voronov, Tuzhilkin Ivan | done |  | |
|`stopImageAnalyzer`| Function | Vadim Voronov| done |  | |
|`onOnAttach`| Function | Vadim Voronov, Evstigneev Roman | done |  |  |
|`offOnAttach`| Function | Vadim Voronov, Evstigneev Roman | done |  |  |
|`onOnDetach`| Function | Vadim Voronov, Evstigneev Roman | done |  | |
|`offOnDetach`| Function | Vadim Voronov, Evstigneev Roman | done |  |  |
|`getHeight`| Function |Vadim Voronov| done |  | |
|`getWidth`| Function |Vadim Voronov| done |  | |
|`getCanvas`| Function | | | | |
|*OffscreenCanvasRenderingContext2D*| *Class* | Vadim Voronov | blocked IDL | | |
|`toDataURL`| Function | Vadim Voronov | in progress | | done on FB, doesn't present in Upstream |
|`transferToImageBitmap`| Function | Vadim Voronov | blocked AceEngine | | doesn't present in Upstream  https://gitee.com/openharmony/arkui_ace_engine/issues/IBE98T |
|*OffscreenCanvas*| *Class* | Vadim Voronov | in progress | | doesn't present in Upstream |
|`transferToImageBitmap`| Function | Vadim Voronov | in progress | | doesn't present in Upstream |
|`getContext2d`| Function | Vadim Voronov | in progress | | doesn't present in Upstream |
|`getHeight`| Function | Vadim Voronov | in progress | | doesn't present in Upstream |
|`setHeight`| Function | Vadim Voronov | in progress | | doesn't present in Upstream |
|`getWidth`| Function | Vadim Voronov | in progress | | doesn't present in Upstream |
|`setWidth`| Function | Vadim Voronov | in progress | | doesn't present in Upstream |
|*DrawingRenderingContext*| *Class* | Dudkin Sergey | done |  | |
|`invalidate`| Function | Dudkin Sergey | done |  | |
|`getCanvas`| Function | | | | |
|*ICurve*| *Class* | Erokhin Ilya | blocked IDL |  | |
|`interpolate`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|*DrawModifier*| *Class* | Erokhin Ilya | blocked IDL|  | |
|`drawBehind`| Function | Erokhin Ilya | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAFYT |
|`drawContent`| Function | Erokhin Ilya | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAFYT |
|`drawFront`| Function | Erokhin Ilya | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAFYT |
|`invalidate`| Function | Erokhin Ilya | done |  | |
|*TransitionEffect*| *Class* | Andrey Khudenkikh | done |  | |
|`translate`| Function | Andrey Khudenkikh | done |  | |
|`rotate`| Function | Andrey Khudenkikh | done |  | |
|`scale`| Function | Andrey Khudenkikh | done |  | |
|`opacity`| Function | Andrey Khudenkikh | done |  | |
|`move`| Function | Andrey Khudenkikh | done |  | |
|`asymmetric`| Function | Andrey Khudenkikh | done |  | |
|`animation`| Function | Andrey Khudenkikh | done |  | |
|`combine`| Function | Andrey Khudenkikh | done |  | |
|`getIDENTITY`| Function | | | |
|`setCursor`| Function | | | | | 
|`restoreDefault`| Function | | | | |
|`getSLIDE_SWITCH`| Function | | | |
|*BaseEvent*| *Class* | Politov Mikhail | blocked IDL |  | |
|`getModifierKeyState`| Function | Politov Mikhail | done |  | |
|`setTarget`| Function | Politov Mikhail | done |  | |
|`getTimestamp`| Function | Politov Mikhail | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBKP3E |
|`setTimestamp`| Function | Politov Mikhail | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBKP3E |
|`getSource`| Function | Tuzzhilkin Ivan | in progress |  | done on feature_branch |
|`setSource`| Function | Politov Mikhail | done |  | |
|`getAxisHorizontal`| Function | Politov Mikhail | done |  | |
|`setAxisHorizontal`| Function | Politov Mikhail | done |  | |
|`getAxisVertical`| Function | Politov Mikhail | done |  | |
|`setAxisVertical`| Function | Politov Mikhail | done |  | |
|`getPressure`| Function | Politov Mikhail | done |  | |
|`setPressure`| Function | Politov Mikhail | done |  | |
|`getTiltX`| Function | Politov Mikhail | done |  | |
|`setTiltX`| Function | Politov Mikhail | done |  | |
|`getTiltY`| Function | Politov Mikhail | done |  | |
|`setTiltY`| Function | Politov Mikhail | done |  | |
|`getSourceTool`| Function | Tuzhilkin Ivan | in progress |  |done on feature_branch |
|`setSourceTool`| Function | Politov Mikhail | done |  | |
|`getDeviceId`| Function | Politov Mikhail | done |  | |
|`setDeviceId`| Function | Politov Mikhail | done |  | |
|`getTargetDisplayId`| Function | Politov Mikhail | in progress |  | |
|`setTargetDisplayId`| Function | Politov Mikhail | in progress |  | |
|*ClickEvent*| *Class* | Maksimov Nikita, Pavelyev Ivan | in progress |  | |
|`getDisplayX`| Function | Maksimov Nikita, Pavelyev Ivan | in progress |  |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setDisplayX`| Function | Maksimov Nikita, Pavelyev Ivan | done |  | |
|`getDisplayY`| Function | Maksimov Nikita, Pavelyev Ivan | in progress |  |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setDisplayY`| Function | Maksimov Nikita, Pavelyev Ivan | done |  | |
|`getWindowX`| Function | Maksimov Nikita, Pavelyev Ivan | in progress |  |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setWindowX`| Function | Maksimov Nikita, Pavelyev Ivan | done |  | |
|`getWindowY`| Function | Maksimov Nikita, Pavelyev Ivan | in progress |  |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setWindowY`| Function | Maksimov Nikita, Pavelyev Ivan | done |  | |
|`getScreenX`| Function | Maksimov Nikita, Pavelyev Ivan | done |  |deprecated? |
|`setScreenX`| Function | Maksimov Nikita, Pavelyev Ivan | done |  |deprecated? |
|`getScreenY`| Function | Maksimov Nikita, Pavelyev Ivan | done |  |deprecated? |
|`setScreenY`| Function | Maksimov Nikita, Pavelyev Ivan | done |  |deprecated? |
|`getX`| Function | Maksimov Nikita, Pavelyev Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setX`| Function | Maksimov Nikita, Pavelyev Ivan | done |  | |
|`getY`| Function | Maksimov Nikita, Pavelyev Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setY`| Function | Maksimov Nikita, Pavelyev Ivan | done |  | |
|`getPreventDefault`| Function | Samarin Sergey | in progress | | done for feature branch |
|`setPreventDefault`| Function | Maksimov Nikita, Pavelyev Ivan | done |  | |
|*HoverEvent*| *Class* | Tuzhilkin Ivan | blocked IDL |  | |
|`getStopPropagation`| Function | Samarin Sergey | in progress | | done for feature branch |
|`setStopPropagation`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBK1OL |
|*MouseEvent*| *Class* | Kovalev Sergey | done |  | |
|`getButton`| Function | Kovalev Sergey | done |  | |
|`setButton`| Function | Kovalev Sergey | done |  | |
|`getAction`| Function | Kovalev Sergey | done |  | |
|`setAction`| Function | Kovalev Sergey | done |  | |
|`getDisplayX`| Function | Kovalev Sergey | done |  | |
|`setDisplayX`| Function | Kovalev Sergey | done |  | |
|`getDisplayY`| Function | Kovalev Sergey | done |  | |
|`setDisplayY`| Function | Kovalev Sergey | done |  | |
|`getWindowX`| Function | Kovalev Sergey | done |  | |
|`setWindowX`| Function | Kovalev Sergey | done |  | |
|`getWindowY`| Function | Kovalev Sergey | done |  | |
|`setWindowY`| Function | Kovalev Sergey | done |  | |
|`getScreenX`| Function | Kovalev Sergey | done |  |deprecated? |
|`setScreenX`| Function | Kovalev Sergey | done |  |deprecated? |
|`getScreenY`| Function | Kovalev Sergey | done |  |deprecated? |
|`setScreenY`| Function | Kovalev Sergey | done |  |deprecated? |
|`getX`| Function | Kovalev Sergey | done |  | |
|`setX`| Function | Kovalev Sergey | done |  | |
|`getY`| Function | Kovalev Sergey | done |  | |
|`setY`| Function | Kovalev Sergey | done |  | |
|`getStopPropagation`| Function | Samarin Sergey | in progress | | done for feature branch |
|`setStopPropagation`| Function | Kovalev Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBK1OL |
|`getRawDeltaX`| Function | | blocked IDL| noFB | doesn't present in Upstream and FB |
|`setRawDeltaX`| Function | | blocked IDL| noFB | doesn't present in Upstream and FB |
|`getRawDeltaY`| Function | | blocked IDL| noFB | doesn't present in Upstream and FB |
|`setRawDeltaY`| Function | | blocked IDL| noFB | doesn't present in Upstream and FB |
|`getPressedButtons`| Function | | | | |
|`setPressedButtons`| Function | | blocked IDL| noFB | doesn't present in Upstream and FB |
|*AccessibilityHoverEvent*| *Class* | Pavelyev Ivan | blocked IDL |  | |
|`getType`| Function | Pavelyev Ivan, Samarin Sergey | in progress |  | done for feature branch https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setType`| Function | Pavelyev Ivan | done |  | |
|`getX`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|`setX`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|`getY`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|`setY`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|`getDisplayX`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|`setDisplayX`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|`getDisplayY`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|`setDisplayY`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|`getWindowX`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|`setWindowX`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|`getWindowY`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|`setWindowY`| Function | Pavelyev Ivan | done |  | UT by Vadim Voronov |
|*TouchEvent*| *Class* | Tuzhilkin Ivan | blocked IDL |  | |
|`getHistoricalPoints`| Function | Tuzhilkin Ivan | blocked IDL |  | implementation and UT are ready on FB, but correct behavior is blocked https://gitee.com/nikolay-igotti/idlize/issues/IBKP3E |
|`getType`| Function | Tuzhilkin Ivan, Samarin Sergey | in progress |  | done for feature branch https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setType`| Function | Tuzhilkin Ivan | done |  | empty implementation |
|`getTouches`| Function | Tuzhilkin Ivan | in progress | | done for feature branch |
|`setTouches`| Function | Tuzhilkin Ivan | done |  | empty implementation |
|`getChangedTouches`| Function | Tuzhilkin Ivan | in progress | | done for feature branch|
|`setChangedTouches`| Function | Tuzhilkin Ivan | done |  | empty implementation |
|`getStopPropagation`| Function | Samarin Sergey | in progress | | done for feature branch |
|`setStopPropagation`| Function | Tuzhilkin Ivan | done |  | empty implementation |
|`getPreventDefault`| Function | Samarin Sergey | in progress | | done for feature branch |
|`setPreventDefault`| Function | Tuzhilkin Ivan | done |  | empty implementation |
|*PixelMapMock*| *Class* | Maksimov Nikita | done |  | |
|`release`| Function | Maksimov Nikita | done |  | |
|*DragEvent*| *Class* | Evstigneev Roman | in progress |  | |
|`getDisplayX`| Function | Tuzhilkin Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getDisplayY`| Function | Tuzhilkin Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getWindowX`| Function | Evstigneev Roman | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getWindowY`| Function | Evstigneev Roman | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getX`| Function | Evstigneev Roman | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0, deprecated? |
|`getY`| Function | Evstigneev Roman | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0, deprecated? |
|`setData`| Function | Evstigneev Roman | done |  | |
|`getData`| Function | Evstigneev Roman | in progress |  |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0, done for feature branch |
|`getSummary`| Function | Tuzhilkin Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setResult`| Function | Evstigneev Roman | done |  | |
|`getResult`| Function | Evstigneev Roman | in progress |  |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0, done for feature branch |
|`getPreviewRect`| Function | Evstigneev Roman | in progress |  |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0, done for feature branch |
|`getVelocityX`| Function | Tuzhilkin Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getVelocityY`| Function | Tuzhilkin Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getVelocity`| Function | Tuzhilkin Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getModifierKeyState`| Function | Tuzhilkin Ivan | done |  | |
|`executeDropAnimation`| Function | Tuzhilkin Ivan | in progress |  | wait MASTER upmerge |
|`getDragBehavior`| Function | Tuzhilkin Ivan | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0, done for feature branch |
|`setDragBehavior`| Function | Tuzhilkin Ivan | done |  | |
|`getUseCustomDropAnimation`| Function | Evstigneev Roman | done |  | |
|`setUseCustomDropAnimation`| Function | Evstigneev Roman | done |  | |
|*KeyEvent*| *Class* | Maksimov Nikita | blocked IDL |  | |
|`getModifierKeyState`| Function | Maksimov Nikita | done | | |
|`getType`| Function | Maksimov Nikita, Samarin Sergey | in progress |  | done for feature branch https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setType`| Function | Maksimov Nikita | done | | |
|`getKeyCode`| Function | Maksimov Nikita | done | | |
|`setKeyCode`| Function | Maksimov Nikita | done | | |
|`getKeyText`| Function | Maksimov Nikita | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setKeyText`| Function | Maksimov Nikita | done | | |
|`getKeySource`| Function | Maksimov Nikita, Samarin Sergey | in progress | | done for feature branch https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setKeySource`| Function | Maksimov Nikita | done | | |
|`getDeviceId`| Function | Maksimov Nikita | done | | |
|`setDeviceId`| Function | Maksimov Nikita | done | | |
|`getMetaKey`| Function | Maksimov Nikita | done | | |
|`setMetaKey`| Function | Maksimov Nikita | done | | |
|`getTimestamp`| Function | Maksimov Nikita | blocked IDL | | https://gitee.com/nikolay-igotti/idlize/issues/IBKP3E |
|`setTimestamp`| Function | Maksimov Nikita | blocked IDL | | https://gitee.com/nikolay-igotti/idlize/issues/IBKP3E |
|`getStopPropagation`| Function | Samarin Sergey | in progress | | done for feature branch |
|`setStopPropagation`| Function | Maksimov Nikita | done | | |
|`setIntentionCode`| Function | Maksimov Nikita | done | | |
|`getUnicode`| Function | Maksimov Nikita | done | | |
|`setUnicode`| Function | Maksimov Nikita | done | | |
|*FocusAxisEvent*| *Class* | | | | |
|`setAxisMap`| Function | | | | |
|`getStopPropagation`| Function | Samarin Sergey | blocked IDL | | feature: API not present, upstream: https://gitee.com/nikolay-igotti/idlize/issues/IBK1OL |
|`setStopPropagation`| Function | | in progress |  | https://gitee.com/nikolay-igotti/idlize/issues/IBK1OL |
|*ProgressMask*| *Class* | Maksimov Nikita | done |  | |
|`updateProgress`| Function | Maksimov Nikita | done |  | |
|`updateColor`| Function | Maksimov Nikita | done |  | |
|`enableBreathingAnimation`| Function | Maksimov Nikita | done |  | |
|*Measurable*| *Class* | | | | |
|`measure`| Function | | | | |
|`getMargin`| Function | | | | |
|`getPadding`| Function | | | | |
|`getBorderWidth`| Function | | | | |
|*View*| *Class* | Skroba Gleb | blocked IDL |  | |
|`create`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|*TextContentControllerBase*| *Class* | Morozov Sergey | blocked IDL |  | |
|`getCaretOffset`| Function | Dudkin Sergey | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED |
|`getTextContentRect`| Function | Morozov Sergey | in progress |  |  |
|`getTextContentLineCount`| Function | Morozov Sergey | done |  | |
|`addText`| Function | | | | |
|`deleteText`| Function | | | | |
|`getSelection`| Function | | | | |
|*DynamicNode*| *Class* | Skroba Gleb | blocked ID |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`onMove`| Function | Skroba Gleb | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|*ChildrenMainSize*| *Class* | Morozov Sergey | blocked IDL |  |
|`splice`| Function | Morozov Sergey | done |  |  |
|`update`| Function | Morozov Sergey | done |  |  |
|`getChildDefaultSize`| Function | Morozov Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IBLA1J |
|`setChildDefaultSize`| Function | Morozov Sergey| done |  | |
|*UICommonEvent*| *Class* | Andrey Khudenkikh | in progress | | |
|`setOnClick`| Function | Andrey Khudenkikh | done | | |
|`setOnTouch`| Function | Andrey Khudenkikh | done | | |
|`setOnAppear`| Function | Andrey Khudenkikh | done | | |
|`setOnDisappear`| Function | Andrey Khudenkikh | done | | |
|`setOnKeyEvent`| Function | Andrey Khudenkikh | in progress | | |
|`setOnFocus`| Function | Andrey Khudenkikh | done | | |
|`setOnBlur`| Function | Andrey Khudenkikh | done | | |
|`setOnHover`| Function | Andrey Khudenkikh | done | | |
|`setOnMouse`| Function |  Andrey Khudenkikh| done | | |
|`setOnSizeChange`| Function | Andrey Khudenkikh | in progress | | |
|`setOnVisibleAreaApproximateChange`| Function | Andrey Khudenkikh | done | | |
|*GestureModifier*| *Class* | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG |
|`applyGesture`| Function | Tuzhilkin Ivan | blocked IDL |  |  https://gitee.com/nikolay-igotti/idlize/issues/IAU9SG |
|*GlobalScope_common*| *Class* | Erokhin Ilya | blocked IDL |  | |
|`getContext`| Function | Erokhin Ilya | blocked IDL |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IBJXAR |
|`postCardAction`| Function | Erokhin Ilya | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB6H50 |
|`dollar_r`| Function | Erokhin Ilya | blocked IDL |  | NEED TO BE REMOVED https://gitee.com/nikolay-igotti/idlize/issues/IBJVPN |
|`dollar_rawfile`| Function | Erokhin Ilya | blocked IDL |  | NEED TO BE REMOVED https://gitee.com/nikolay-igotti/idlize/issues/IBJVPN |
|`animateTo`| Function | Erokhin Ilya | done |  | |
|`animateToImmediately`| Function | Erokhin Ilya | done |  | |
|`vp2px`| Function | Erokhin Ilya | done |  | |
|`px2vp`| Function | Erokhin Ilya | done |  | |
|`fp2px`| Function | Erokhin Ilya | done |  | |
|`px2fp`| Function | Erokhin Ilya | done |  | |
|`lpx2px`| Function | Erokhin Ilya | done |  | |
|`px2lpx`| Function | Erokhin Ilya | done |  | |
|`setCursor`| Function | Erokhin Ilya | done |  | |
|`restoreDefault`| Function | Erokhin Ilya | done |  | |
|*ContextMenu*| *Class* | Tuzhilkin Ivan | blocked IDL |  | |
|`close`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBB84T |
|*CustomDialogController*| *Class* | Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAX81Q & https://gitee.com/nikolay-igotti/idlize/issues/IBECPG & https://gitee.com/nikolay-igotti/idlize/issues/IBGU8S + |
|`open`| Function | Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAX81Q & https://gitee.com/nikolay-igotti/idlize/issues/IBECPG + |
|`close`| Function | Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAX81Q & https://gitee.com/nikolay-igotti/idlize/issues/IBECPG + |
|*LinearGradient*| *Class* | Morozov Sergey | in progress | done on feature branch | |
|*DatePickerDialog*| *Class* | Ekaterina Stepanova | blocked IDL |  | |
|`show`| Function | Ekaterina Stepanova | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBHHWA |
|*BaseGestureEvent*| *Class* | Maksimov Nikita | done |  | |
|`getFingerList`| Function | Kovalev Sergey | in progress | done on FB| |
|`setFingerList`| Function | Maksimov Nikita | done |  | implementation and UT Vadim Voronov |
|*TapGestureEvent*| *Class* | Samarin Sergey | done | | nothing to do |
|*LongPressGestureEvent*| *Class* | | | | |
|`getRepeat`| Function | | | | |
|`setRepeat`| Function | | | | |
|*PanGestureEvent*| *Class* | Morozov Sergey | blocked IDL | | |
|`getOffsetX`| Function | Morozov Sergey | in progress | |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setOffsetX`| Function | Morozov Sergey | in progress | | |
|`getOffsetY`| Function | Morozov Sergey | in progress | |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setOffsetY`| Function | Morozov Sergey | in progress | | |
|`getVelocityX`| Function | Morozov Sergey | in progress | |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setVelocityX`| Function | Morozov Sergey | in progress | | |
|`getVelocityY`| Function | Morozov Sergey | in progress | |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setVelocityY`| Function | Morozov Sergey | in progress | | |
|`getVelocity`| Function | Morozov Sergey | in progress | |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setVelocity`| Function | Morozov Sergey | in progress | | |
|*PinchGestureEvent*| *Class* | Vadim Voronov | done | | |
|`getScale`| Function | Vadim Voronov | done | | |
|`setScale`| Function | Vadim Voronov | done | | |
|`getPinchCenterX`| Function | Vadim Voronov | done | | |
|`setPinchCenterX`| Function | Vadim Voronov | done | | |
|`getPinchCenterY`| Function | Vadim Voronov | done | | |
|`setPinchCenterY`| Function | Vadim Voronov | done | | |
|*RotationGestureEvent*| *Class* | | | | |
|`getAngle`| Function | | | | |
|`setAngle`| Function | | | | |
|*SwipeGestureEvent*| *Class* | Evstigneev Roman | in progress | | |
|`getAngle`| Function | Evstigneev Roman | in progress | |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setAngle`| Function | Evstigneev Roman | in progress | | |
|`getSpeed`| Function | Evstigneev Roman | in progress | |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setSpeed`| Function | Evstigneev Roman | in progress | | |
|*GestureEvent*| *Class* | Samarin Sergey | blocked IDL |  | |
|`getRepeat`| Function | Samarin Sergey | done |  | |
|`setRepeat`| Function | Samarin Sergey | done |  | |
|`getFingerList`| Function | Kovalev Sergey | in progress | | |
|`setFingerList`| Function | Samarin Sergey | done |  | |
|`getOffsetX`| Function | Samarin Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setOffsetX`| Function | Samarin Sergey | done |  | |
|`getOffsetY`| Function | Samarin Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setOffsetY`| Function | Samarin Sergey | done |  | |
|`getAngle`| Function | Samarin Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setAngle`| Function | Samarin Sergey | done |  | |
|`getSpeed`| Function | Samarin Sergey | in progress|  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setSpeed`| Function | Samarin Sergey | done |  | |
|`getScale`| Function | Samarin Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setScale`| Function | Samarin Sergey | done |  | |
|`getPinchCenterX`| Function | Samarin Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setPinchCenterX`| Function | Samarin Sergey | done |  | |
|`getPinchCenterY`| Function | Samarin Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setPinchCenterY`| Function | Samarin Sergey | done |  | |
|`getVelocityX`| Function | Samarin Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setVelocityX`| Function | Samarin Sergey | done |  | |
|`getVelocityY`| Function | Samarin Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setVelocityY`| Function | Samarin Sergey | done |  | |
|`getVelocity`| Function | Samarin Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setVelocity`| Function | Samarin Sergey | blocked IDL|  | not applicable https://gitee.com/nikolay-igotti/idlize/issues/IBQEWR |
|*PanGestureOptions*| *Class* | Politov Mikhail | blocked IDL |  | |
|`setDirection`| Function | Politov Mikhail | done |  | |
|`setDistance`| Function | Politov Mikhail | done |  | |
|`setFingers`| Function | Politov Mikhail | done |  | |
|`getDirection`| Function | Politov Mikhail | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|*ScrollableTargetInfo*| *Class* | Maksimov Nikita | done |  | |
|`isBegin`| Function | Maksimov Nikita | done |  | |
|`isEnd`| Function | Maksimov Nikita | done |  | |
|*EventTargetInfo*| *Class* | Maksimov Nikita | blocked IDL |  | |
|`getId`| Function | Maksimov Nikita | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|*GestureRecognizer*| *Class* | Kovalev Sergey | blocked IDL |  | |
|`getTag`| Function | Kovalev Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|`getType`| Function | Kovalev Sergey, Samarin Sergey | in progress |  | done for feature branch https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|`isBuiltIn`| Function | Kovalev Sergey | done |  | |
|`setEnabled`| Function | Kovalev Sergey | done |  | |
|`isEnabled`| Function | Kovalev Sergey | done |  | |
|`getState`| Function | Kovalev Sergey | done |  | |
|`getEventTargetInfo`| Function | Maksimov Nikita | done |  | |
|`isValid`| Function | Kovalev Sergey | done |  | |
|*PanRecognizer*| *Class* | Politov Mikhail | done |  | |
|`getPanGestureOptions`| Function | Politov Mikhail | done |  | |
|*ImageAnalyzerController*| *Class* |Vadim Voronov|  blocked IDL|  | |
|`getImageAnalyzerSupportTypes`| Function |Vadim Voronov|  blocked Ace_Engine |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/openharmony/arkui_ace_engine/issues/IBPTCE |
|*ListScroller*| *Class* |Morozov Sergey | blocked IDL|  |  |
|`getItemRectInGroup`| Function |Morozov Sergey | in progress |  | |
|`scrollToItemInGroup`| Function |Morozov Sergey | done |  |  |
|`closeAllSwipeActions`| Function |Morozov Sergey | done |  |  |
|`getVisibleListContentInfo`| Function |Morozov Sergey | in progress|  |RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|*Matrix2D*| *Class* | Vadim Voronov | in progress |  | https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`identity`| Function | Vadim Voronov | done |  | |
|`invert`| Function | Vadim Voronov | done |  | |
|`multiply`| Function | Vadim Voronov | deprecated |  | deprecated |
|`rotate`| Function | Vadim Voronov | done |  | rotate0 deprecated, rotate1 done |
|`translate`| Function | Vadim Voronov | done |  | |
|`scale`| Function | Vadim Voronov | done |  | |
|`getScaleX`| Function | Vadim Voronov | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED implemented without return val,https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setScaleX`| Function | Vadim Voronov | done |  | |
|`getRotateY`| Function | Vadim Voronov | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED implemented without return val,https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setRotateY`| Function | Vadim Voronov | done |  | |
|`getRotateX`| Function | Vadim Voronov | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED implemented without return val,https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setRotateX`| Function | Vadim Voronov | done |  | |
|`getScaleY`| Function | Vadim Voronov | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED implemented without return val,https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setScaleY`| Function | Vadim Voronov | done |  | |
|`getTranslateX`| Function | Vadim Voronov | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED implemented without return val,https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setTranslateX`| Function | Vadim Voronov | done |  | |
|`getTranslateY`| Function | Vadim Voronov | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED implemented without return val,https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setTranslateY`| Function | Vadim Voronov | done |  | |
|*NavDestinationContext*| *Class* | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`getConfigInRouteMap`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`setPathInfo`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, https://gitee.com/nikolay-igotti/idlize/issues/IB7ZKX |
|`setPathStack`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, https://gitee.com/nikolay-igotti/idlize/issues/IB7ZKX |
|`getNavDestinationId`| Function | Morozov Sergey | blocked IDL |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`setNavDestinationId`| Function | Morozov Sergey | done |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|*NavPathInfo*| *Class* | Skroba Gleb | in progress | | done on feature |
|`getName`| Function | Skroba Gleb | in progress | | done on feature |
|`setName`| Function | Skroba Gleb | in progress | | done on feature |
|`setParam`| Function | Skroba Gleb | in progress | | done on feature |
|`getOnPop`| Function | Skroba Gleb | in progress | | done on feature |
|`setOnPop`| Function | Skroba Gleb | in progress | | done on feature |
|`getIsEntry`| Function | Skroba Gleb | in progress | | done on feature |
|`setIsEntry`| Function | Skroba Gleb | in progress | | done on feature |
|*NavPathStack*| *Class* | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`pushPath`| Function | Skroba Gleb | done |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`pushDestination`| Function | Morozov Sergey | in progress |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, can be implemented without return val - https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`pushPathByName`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`pushDestinationByName`| Function | Morozov Sergey | in progress |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, can be implemented without return val - https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 
|`replacePath`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`replaceDestination`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`replacePathByName`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`removeByIndexes`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`removeByName`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`removeByNavDestinationId`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`pop`| Function | Skroba Gleb | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, implemented without return val - https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 
|`popToName`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`popToIndex`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`moveToTop`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`moveIndexToTop`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`clear`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`getAllPathName`| Function | Morozov Sergey | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, can be implemented without return val - https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 
|`getParamByIndex`| Function | Morozov Sergey | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, can be implemented without return val - https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 
|`getParamByName`| Function | Morozov Sergey, Skroba Gleb | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, can be implemented without return val - https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0
|`getIndexByName`| Function | Morozov Sergey | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, can be implemented without return val - https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 
|`getParent`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`size`| Function | Skroba Gleb | done |  |  |
|`disableAnimation`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`setInterception`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|*NavigationTransitionProxy*| *Class* | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y |
|`finishTransition`| Function | Morozov Sergey | done |  |  |
|`cancelTransition`| Function | Morozov Sergey | done |  |  |
|`updateTransition`| Function | Morozov Sergey | done |  |  |
|`setFrom`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, https://gitee.com/nikolay-igotti/idlize/issues/IB7ZKX |
|`setTo`| Function | Morozov Sergey | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBAY4Y, https://gitee.com/nikolay-igotti/idlize/issues/IB7ZKX |
|`getIsInteractive`| Function | Morozov Sergey | done |  |  |
|`setIsInteractive`| Function | Morozov Sergey | done |  |  |
|*PatternLockController*| *Class* |Dmitry A Smirnov| done |  |  |
|`reset`| Function |Dmitry A Smirnov| done |  |  |
|`setChallengeResult`| Function |Dmitry A Smirnov| done |  |  |
|*RichEditorBaseController*| *Class*|Dudkin Sergey| blocked IDL|  | |
|`getCaretOffset`| Function|Dudkin Sergey| done |  | |
|`setCaretOffset`| Function|Dudkin Sergey| done |  | |
|`closeSelectionMenu`| Function|Dudkin Sergey| done |  | |
|`getTypingStyle`| Function|Dudkin Sergey| in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IBAXVH + |
|`setTypingStyle`| Function|Dudkin Sergey| done |  | |
|`setSelection`| Function|Dudkin Sergey| done |  | |
|`isEditing`| Function|Dudkin Sergey| done |  | |
|`stopEditing`| Function|Dudkin Sergey| done |  | |
|`getLayoutManager`| Function|Dudkin Sergey| done |  | |
|`getPreviewText`| Function|Dudkin Sergey| in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IBAXVH + |
|`getCaretRect`| Function | | | | |
|*RichEditorController*| *Class* |Dudkin Sergey| blocked IDL|  |  |
|`addTextSpan`| Function |Dudkin Sergey, Samarin Sergey | in progress |  | EVENT |
|`addImageSpan`| Function |Dudkin Sergey, Maksimov Nikita, Tuzhilkin Ivan, Samarin Sergey| in progress |  | EVENT |
|`addBuilderSpan`| Function | Lobah Mikhail | done |  | UT done Lobah Mikhail |
|`addSymbolSpan`| Function | Dudkin Sergey| done |  |  |
|`updateSpanStyle`| Function | Dudkin Sergey| done |  |  |
|`updateParagraphStyle`| Function | Dudkin Sergey| done |  |  |
|`deleteSpans`| Function |Dudkin Sergey| done |  |  |
|`getSpans`| Function |Dudkin Sergey| done |  |  |
|`getParagraphs`| Function |Dudkin Sergey| in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IB1LVZ + |
|`getSelection`| Function |Dudkin Sergey| done |  |  |
|`fromStyledString`| Function | Dudkin Sergey| done |  |  |
|`toStyledString`| Function | Dudkin Sergey| done |  |  |
|*RichEditorStyledStringController*| *Class* |Dudkin Sergey| blocked IDL |  | |
|`setStyledString`| Function |Dudkin Sergey| done |  | |
|`getStyledString`| Function | Maksimov Nikita | done |  | |
|`getSelection`| Function |Dudkin Sergey| in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|`onContentChanged`| Function | Dudkin Sergey| blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB944G + |
|*Scroller*| *Class* | Erokhin Ilya | blocked IDL|  |  |
|`scrollTo`| Function | Erokhin Ilya | done |  |  |
|`scrollEdge`| Function | Erokhin Ilya | done |  |  |
|`fling`| Function | Erokhin Ilya | done |  |  |
|`scrollPage`| Function | Erokhin Ilya | done |  |  |
|`currentOffset`| Function | Erokhin Ilya | in progress |  | |
|`scrollToIndex`| Function | Erokhin Ilya | done |  | |
|`scrollBy`| Function | Erokhin Ilya | done |  |  |
|`isAtEnd`| Function | Erokhin Ilya | done |  |  |
|`getItemRect`| Function | Erokhin Ilya | in progress |  | |
|`getItemIndex`| Function | Erokhin Ilya | done |  |  |
|*SearchController*| *Class* |Evstigneev Roman | done |  |  |
|`caretPosition`| Function |Evstigneev Roman | done |  |  |
|`stopEditing`| Function |Evstigneev Roman | done |  |  |
|`setTextSelection`| Function |Evstigneev Roman | done |  |  |
|*SwiperController*| *Class* | Skroba Gleb | done |  |  |
|`showNext`| Function | Skroba Gleb | done |  |  |
|`showPrevious`| Function | Skroba Gleb | done |  |  |
|`changeIndex`| Function | Skroba Gleb | done |  |  |
|`finishAnimation`| Function | Skroba Gleb | done |  |  |
|`preloadItems`| Function | | | | |
|*SwiperContentTransitionProxy*| *Class* | Skroba Gleb | blocked IDL |  | |
|`finishTransition`| Function | Skroba Gleb | done |  | |
|`getSelectedIndex`| Function | Skroba Gleb | in progress |  |Looks like it's done. RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setSelectedIndex`| Function | Skroba Gleb | done |  | |
|`getIndex`| Function | Skroba Gleb | in progress |  |Looks like it's done. RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setIndex`| Function | Skroba Gleb | done |  | |
|`getPosition`| Function | Skroba Gleb | in progress |  |Looks like it's done. RETURN_VALUE https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setPosition`| Function | Skroba Gleb | done |  | |
|`getMainAxisLength`| Function | Skroba Gleb | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setMainAxisLength`| Function | Skroba Gleb | done |  | |
|*IndicatorComponentController*| *Class* | Andrey Khudenkikh | done |  |  |
|`showNext`| Function | Andrey Khudenkikh | done |  |  |
|`showPrevious`| Function |  Andrey Khudenkikh | done |  |  |
|`changeIndex`| Function |  Andrey Khudenkikh | done |  |  |
|*HierarchicalSymbolEffect*| *Class* | | | | |
|`getFillStyle`| Function | | | | |
|`setFillStyle`| Function | | | | |
|*AppearSymbolEffect*| *Class* | | | | |
|`getScope`| Function | | | | |
|`setScope`| Function | | | | |
|*DisappearSymbolEffect*| *Class* | | | | |
|`getScope`| Function | | | | |
|`setScope`| Function | | | | |
|*BounceSymbolEffect*| *Class* | | | | |
|`getScope`| Function | | | | |
|`setScope`| Function | | | | |
|`getDirection`| Function | | | | |
|`setDirection`| Function | | | | |
|*TabsController*| *Class* | Skroba Gleb | done |  | |
|`changeIndex`| Function | Skroba Gleb | done |  | |
|`preloadItems`| Function | Skroba Gleb | done |  | |
|`setTabBarTranslate`| Function | Skroba Gleb | done |  | |
|`setTabBarOpacity`| Function | Skroba Gleb | done |  | |
|*TabContentTransitionProxy*| *Class* | Dudkin Sergey | done |  | |
|`finishTransition`| Function | Dudkin Sergey | done |  | |
|`getFrom`| Function | Dudkin Sergey | done |  | |
|`setFrom`| Function | Dudkin Sergey | done |  | |
|`getTo`| Function | Dudkin Sergey | done |  | |
|`setTo`| Function | Dudkin Sergey | done |  | |
|*TextController*| *Class* | Samarin Sergey | done |  | |
|`closeSelectionMenu`| Function | Samarin Sergey | done |  | |
|`setStyledString`| Function | Samarin Sergey | done |  | |
|`getLayoutManager`| Function | Samarin Sergey | done |  | |
|*TextAreaController*| *Class* | Tuzhilkin Ivan | done |  |  |
|`caretPosition`| Function | Tuzhilkin Ivan | done |  |  |
|`setTextSelection`| Function | Tuzhilkin Ivan | done |  |  |
|`stopEditing`| Function | Tuzhilkin Ivan | done |  |  |
|*TextClockController*| *Class* |Pavelyev Ivan| done |  |  |
|`start`| Function |Pavelyev Ivan| done |  |  |
|`stop`| Function |Pavelyev Ivan| done |  |  |
|*TextBaseController*| *Class* | Morozov Sergey | done |  | |
|`setSelection`| Function | Morozov Sergey | done |  | |
|`closeSelectionMenu`| Function | Morozov Sergey | done |  | |
|`getLayoutManager`| Function | Morozov Sergey | done |  | |
|*TextEditControllerEx*| *Class* | Morozov Sergey | blocked IDL|  | |
|`isEditing`| Function | Morozov Sergey | done |  | |
|`stopEditing`| Function | Morozov Sergey | done |  | |
|`setCaretOffset`| Function | Morozov Sergey | done |  | |
|`getCaretOffset`| Function | Morozov Sergey | done |  | |
|`getPreviewText`| Function | Morozov Sergey | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|*StyledStringController*| *Class* | Pavelyev Ivan | done |  | |
|`setStyledString`| Function | Pavelyev Ivan | done |  | |
|`getStyledString`| Function | Pavelyev Ivan | done |  | |
|*LayoutManager*| *Class* | Andrey Khudenkikh | blocked IDL|  | |
|`getLineCount`| Function | Andrey Khudenkikh | done |  | |
|`getGlyphPositionAtCoordinate`| Function | Andrey Khudenkikh | blocked IDL |  | |
|`getLineMetrics`| Function | Andrey Khudenkikh | blocked IDL |  | |
|`getRectsForRange`| Function | Andrey Khudenkikh | blocked IDL |  | |
|*TextMenuItemId*| *Class* | Maksimov Nikita | done |  | |
|`of`| Function | Maksimov Nikita | done |  | |
|`equals`| Function | Maksimov Nikita | done |  | |
|`getCUT`| Function | | | | |
|`getCOPY`| Function | | | | |
|`getPASTE`| Function | | | | |
|`getSELECT_ALL`| Function | | | | |
|`getCOLLABORATION_SERVICE`| Function | | | | |
|`getCAMERA_INPUT`| Function | | | | |
|`getAI_WRITER`| Function | | | | |
|`getTRANSLATE`| Function | | | | |
|`getSEARCH`| Function | | | | |
|`getSHARE`| Function | | | | |
|*EditMenuOptions*| *Class* | Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N + |
|`onCreateMenu`| Function | Skroba Gleb | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N, https://gitee.com/nikolay-igotti/idlize/issues/IAPDBZ |
|`onMenuItemClick`| Function | Skroba Gleb | in progress |RETURN_VALUE, RET_VAL_UNBLOCKED  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N, https://gitee.com/nikolay-igotti/idlize/issues/IAPDBZ |
|*SubmitEvent*| *Class* | Tuzhilkin Ivan | blocked IDL|  | |
|`keepEditableState`| Function | Tuzhilkin Ivan | done |  | |
|`getText`| Function | Tuzhilkin Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`setText`| Function | Tuzhilkin Ivan | done |  | |
|*TextInputController*| *Class* | Spirin Andrey | done |  |  |
|`caretPosition`| Function | Spirin Andrey | done |  |  |
|`setTextSelection`| Function | Spirin Andrey | done |  |  |
|`stopEditing`| Function |  Spirin Andrey | done |  |  |
|*TextPickerDialog*| *Class* | Ekaterina Stepanova | blocked IDL |  | |
|`show`| Function | Ekaterina Stepanova | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBHHWA |
|*TextTimerController*| *Class* |Ekaterina Stepanova| done |  |  |
|`start`| Function |Ekaterina Stepanova| done |  |  |
|`pause`| Function |Ekaterina Stepanova| done |  |  |
|`reset`| Function |Ekaterina Stepanova| done |  |  |
|*TimePickerDialog*| *Class* | Ekaterina Stepanova | blocked IDL|  | |
|`show`| Function | Ekaterina Stepanova | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBHHWA |
|*ColorFilter*| *Class* | Evstigneev Roman | in progress | | |
|*VideoController*| *Class* | Erokhin Ilya | done |  |  |
|`start`| Function | Erokhin Ilya | done |  |  |
|`pause`| Function | Erokhin Ilya | done |  |  |
|`stop`| Function | Erokhin Ilya | done |  |  |
|`setCurrentTime`| Function | Erokhin Ilya | done |  |  |
|`requestFullscreen`| Function | Erokhin Ilya | done |  |  |
|`exitFullscreen`| Function | Erokhin Ilya | done |  |  |
|`reset`| Function | Erokhin Ilya | done |  |  |
|*WebKeyboardController*| *Class* | Erokhin Ilya | done |  |  |
|`insertText`| Function | Erokhin Ilya | done |  |  |
|`deleteForward`| Function | Erokhin Ilya | done |  |  |
|`deleteBackward`| Function | Erokhin Ilya | done |  |  |
|`sendFunctionKey`| Function | Erokhin Ilya | done |  |  |
|`close`| Function | Erokhin Ilya | done |  |  |
|*FullScreenExitHandler*| *Class* | Erokhin Ilya | done |  |  |
|`exitFullScreen`| Function | Erokhin Ilya | done |  |  |
|*FileSelectorParam*| *Class* | Erokhin Ilya | blocked IDL|  |  |
|`getTitle`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getMode`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getAcceptType`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`isCapture`| Function | Erokhin Ilya | done |  |  |
|`getMimeTypes`| Function | | | | |
|*JsResult*| *Class* | Erokhin Ilya | done |  |  |
|`handleCancel`| Function | Erokhin Ilya | done |  |  |
|`handleConfirm`| Function | Erokhin Ilya | done |  |  |
|`handlePromptConfirm`| Function | Erokhin Ilya | done |  |  |
|*FileSelectorResult*| *Class* | Erokhin Ilya | done |  |  |
|`handleFileList`| Function | Erokhin Ilya | done |  |  |
|*HttpAuthHandler*| *Class* | Erokhin Ilya | done |  |  |
|`confirm`| Function | Erokhin Ilya | done |  |  |
|`cancel`| Function | Erokhin Ilya | done |  |  |
|`isHttpAuthInfoSaved`| Function | Erokhin Ilya | done |  |  |
|*SslErrorHandler*| *Class* | Erokhin Ilya | done |  |  |
|`handleConfirm`| Function | Erokhin Ilya | done |  |  |
|`handleCancel`| Function | Erokhin Ilya | done |  |  |
|*ClientAuthenticationHandler*| *Class* | Erokhin Ilya | done |  |  |
|`confirm`| Function | Erokhin Ilya | done |  |  |
|`cancel`| Function | Erokhin Ilya | done |  |  |
|`ignore`| Function | Erokhin Ilya | done |  |  |
|*PermissionRequest*| *Class* | Erokhin Ilya | blocked IDL|  |  |
|`deny`| Function | Erokhin Ilya | done |  |  |
|`getOrigin`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getAccessibleResource`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`grant`| Function | Erokhin Ilya | done |  |  |
|*ScreenCaptureHandler*| *Class* | Erokhin Ilya | blocked IDL|  |  |
|`getOrigin`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`grant`| Function | Erokhin Ilya | done |  |  |
|`deny`| Function | Erokhin Ilya | done |  |  |
|*DataResubmissionHandler*| *Class* | Erokhin Ilya | done |  |  |
|`resend`| Function | Erokhin Ilya | done |  |  |
|`cancel`| Function | Erokhin Ilya | done |  |  |
|*ControllerHandler*| *Class* | Erokhin Ilya | blocked IDL|  |  |
|`setWebController`| Function | Erokhin Ilya | blocked IDL |  | Ark_CustomObject https://gitee.com/nikolay-igotti/idlize/issues/IAU9SX |
|*WebContextMenuParam*| *Class* | Erokhin Ilya | blocked IDL|  |  |
|`x`| Function | Erokhin Ilya | done |  |  |
|`y`| Function | Erokhin Ilya | done |  |  |
|`getLinkUrl`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getUnfilteredLinkUrl`| Function | Erokhin Ilya |in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getSourceUrl`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`existsImageContents`| Function | Erokhin Ilya | done |  |  |
|`getMediaType`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getSelectionText`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getSourceType`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getInputFieldType`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`isEditable`| Function | Erokhin Ilya | done |  |  |
|`getEditStateFlags`| Function | Erokhin Ilya | done |  |  |
|`getPreviewWidth`| Function | Erokhin Ilya | done |  | UT by Vadim Voronov |
|`getPreviewHeight`| Function | Erokhin Ilya | done |  | UT by Vadim Voronov |
|*WebContextMenuResult*| *Class* | Erokhin Ilya | done |  |  |
|`closeContextMenu`| Function | Erokhin Ilya | done |  |  |
|`copyImage`| Function | Erokhin Ilya | done |  |  |
|`copy`| Function | Erokhin Ilya | done |  |  |
|`paste`| Function | Erokhin Ilya | done |  |  |
|`cut`| Function | Erokhin Ilya | done |  |  |
|`selectAll`| Function | Erokhin Ilya | done |  |  |
|*ConsoleMessage*| *Class* | Erokhin Ilya | blocked IDL|  |  |
|`getMessage`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getSourceId`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getLineNumber`| Function | Erokhin Ilya | done |  |  |
|`getMessageLevel`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|*WebResourceRequest*| *Class* | Erokhin Ilya | blocked IDL|  |  |
|`getRequestHeader`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getRequestUrl`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`isRequestGesture`| Function | Erokhin Ilya | done |  |  |
|`isMainFrame`| Function | Erokhin Ilya | done |  |  |
|`isRedirect`| Function | Erokhin Ilya | done |  |  |
|`getRequestMethod`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|*WebResourceResponse*| *Class* | Erokhin Ilya | blocked IDL|  |  |
|`getResponseData`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getResponseDataEx`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getResponseEncoding`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getResponseMimeType`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getReasonMessage`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getResponseHeader`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getResponseCode`| Function | Erokhin Ilya | done |  |  |
|`setResponseData`| Function | Erokhin Ilya | done |  | UT by Vadim Voronov |
|`setResponseEncoding`| Function | Erokhin Ilya | done |  |  |
|`setResponseMimeType`| Function | Erokhin Ilya | done |  |  |
|`setReasonMessage`| Function | Erokhin Ilya | done |  |  |
|`setResponseHeader`| Function | Erokhin Ilya | done |  |  |
|`setResponseCode`| Function | Erokhin Ilya | done |  |  |
|`setResponseIsReady`| Function | Erokhin Ilya | done |  |  |
|`getResponseIsReady`| Function | Erokhin Ilya | done |  |  |
|*WebResourceError*| *Class* | Erokhin Ilya | blocked IDL |  |  |
|`getErrorInfo`| Function | Erokhin Ilya | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`getErrorCode`| Function | Erokhin Ilya | done |  |  |
|*JsGeolocation*| *Class* | Erokhin Ilya | done |  |  |
|`invoke`| Function | Erokhin Ilya | done |  |  |
|*WebCookie*| *Class* | Erokhin Ilya | done |  | |
|`setCookie`| Function | Erokhin Ilya | done |  |deprecated |
|`saveCookie`| Function | Erokhin Ilya | done |  |deprecated |
|*EventResult*| *Class* | Erokhin Ilya | done |  |  |
|`setGestureEventResult`| Function | Erokhin Ilya | done |  |  |
|*WebController*| *Class* | Erokhin Ilya | deprecated |  | |
|`onInactive`| Function | Erokhin Ilya | done |  |deprecated |
|`onActive`| Function | Erokhin Ilya | done |  |deprecated |
|`zoom`| Function | Erokhin Ilya | done |  |deprecated |
|`clearHistory`| Function | Erokhin Ilya | done |  |deprecated |
|`runJavaScript`| Function | Erokhin Ilya | deprecated |  | done |
|`loadData`| Function | Erokhin Ilya | done |  |deprecated |
|`loadUrl`| Function | Erokhin Ilya | deprecated |  | done |  |
|`refresh`| Function | Erokhin Ilya | done |  |deprecated |
|`stop`| Function | Erokhin Ilya | done |  |deprecated |
|`registerJavaScriptProxy`| Function | Erokhin Ilya | deprecated |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9OV |
|`deleteJavaScriptRegister`| Function | Erokhin Ilya | done |  |deprecated |
|`getHitTest`| Function | Erokhin Ilya | deprecated |  | blocked IDL https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 |
|`requestFocus`| Function | Erokhin Ilya | done |  | deprecated |
|`accessBackward`| Function | Erokhin Ilya | done |  | deprecated |
|`accessForward`| Function | Erokhin Ilya | done |  |deprecated |
|`accessStep`| Function | Erokhin Ilya | done |  | deprecated |
|`backward`| Function | Erokhin Ilya | done |  | deprecated |
|`forward`| Function | Erokhin Ilya | done |  | deprecated |
|`getCookieManager`| Function | Erokhin Ilya | deprecated |  | done |
|*XComponentController*| *Class* | Tuzhilkin Ivan | blocked IDL |  | |
|`getXComponentSurfaceId`| Function | Tuzhilkin Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|`getXComponentContext`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAYQZF + |
|`setXComponentSurfaceSize`| Function | Tuzhilkin Ivan | done |  | |
|`setXComponentSurfaceRect`| Function | Tuzhilkin Ivan | done |  | |
|`getXComponentSurfaceRect`| Function | Tuzhilkin Ivan | in progress |  | done on FB, RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|`setXComponentSurfaceRotation`| Function | Tuzhilkin Ivan | done |  | |
|`getXComponentSurfaceRotation`| Function | Tuzhilkin Ivan | in progress |  |done on FB, RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|`onSurfaceCreated`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9OV + |
|`onSurfaceChanged`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9OV + |
|`onSurfaceDestroyed`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IAU9OV + |
|`startImageAnalyzer`| Function | Tuzhilkin Ivan | done |  | |
|`stopImageAnalyzer`| Function | Tuzhilkin Ivan | done |  | |
|*WaterFlowSections*| *Class* | Kovalev Sergey | in progress |  | |
|`splice`| Function | Kovalev Sergey | done |  | |
|`push`| Function | Kovalev Sergey | done |  | |
|`update`| Function | Kovalev Sergey | done |  | |
|`values`| Function | Kovalev Sergey | in progress|  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|`length`| Function | Kovalev Sergey | done |  | |
|*UIExtensionProxy*| *Class* | Tuzhilkin Ivan | blocked IDL|  | |
|`send`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBCGB5 , https://gitee.com/nikolay-igotti/idlize/issues/IAYQZF + |
|`sendSync`| Function | Tuzhilkin Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBCGB5 , https://gitee.com/nikolay-igotti/idlize/issues/IAYQZF + |
|`onAsyncReceiverRegister`| Function | Tuzhilkin Ivan | done |  | |
|`onSyncReceiverRegister`| Function | Tuzhilkin Ivan | done |  | |
|`offAsyncReceiverRegister`| Function | Tuzhilkin Ivan | done |  | |
|`offSyncReceiverRegister`| Function | Tuzhilkin Ivan | done |  | |
|*StyledString*| *Class* | Pavelyev Ivan | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N & PixelMap on https://gitee.com/nikolay-igotti/idlize/issues/IAU9UR |
|`getString`| Function | Pavelyev Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|`getStyles`| Function | Pavelyev Ivan | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|`equals`| Function | Pavelyev Ivan | done |  | |
|`subStyledString`| Function | Pavelyev Ivan | done |  | |
|`fromHtml`| Function | Pavelyev Ivan | done |  | |
|`toHtml`| Function | Pavelyev Ivan | in progress |  |RETURN_VALUE, RET_VAL_BLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|`marshalling`| Function | Pavelyev Ivan | in progress |  |RETURN_VALUE, RET_VAL_UNBLOCKED https://gitee.com/nikolay-igotti/idlize/issues/IAUYD0 + |
|`unmarshalling`| Function | Pavelyev Ivan | done |  | |
|`getLength`| Function | Pavelyev Ivan | done |  | |
|*TextStyle_styled_string*| *Class* | Erokhin Ilya | in progress |  | done on feature branch |
|`getFontFamily`| Function | Erokhin Ilya | in progress |  | done on feature branch |
|`getFontSize`| Function | Erokhin Ilya | in progress |  | done on feature branch |
|`getFontWeight`| Function | Erokhin Ilya | in progress |  | done on feature branch |
|`getFontStyle`| Function | Erokhin Ilya | in progress |  | done on feature branch |
|*DecorationStyle*| *Class* | Tuzhilkin Ivan | in progress |  | done on feature_branch |
|`getType`| Function | Tuzhilkin Ivan | in progress |  | done on feature_branch |
|`getStyle`| Function | Tuzhilkin Ivan | in progress |  | done on feature_branch |
|*BaselineOffsetStyle*| *Class* | Tuzhilkin Ivan | in progress |  | done on feature_branch |
|`getBaselineOffset`| Function | Tuzhilkin Ivan | in progress |  | done on feature_branch |
|*LetterSpacingStyle*| *Class* | Tuzhilkin Ivan | in progress |  | done on feature_branch |
|`getLetterSpacing`| Function | Tuzhilkin Ivan | in progress |  | done on feature_branch |
|*TextShadowStyle*| *Class* |  Politov Mikhail | in progress |  | |
|`getTextShadow`| Function |  Politov Mikhail | in progress | | |
|*BackgroundColorStyle*| *Class* | Politov Mikhail | in progress |  | |
|*GestureStyle*| *Class* | Dudkin Sergey| in progress |  | |
|*ParagraphStyle*| *Class* |Dudkin Sergey |in progress |  | |
|`getTextAlign`| Function |Dudkin Sergey |in progress |  | |
|`getTextIndent`| Function |Dudkin Sergey |in progress |  | |
|`getMaxLines`| Function |Dudkin Sergey |in progress |  | |
|`getOverflow`| Function |Dudkin Sergey |in progress |  | |
|`getWordBreak`| Function |Dudkin Sergey |in progress |  | |
|*LineHeightStyle*| *Class* |Dudkin Sergey |in progress |  | |
|`getLineHeight`| Function |Dudkin Sergey |in progress |  | |
|*UrlStyle*| *Class* | Politov Mikhail | in progress |  | |
|`getUrl`| Function | Politov Mikhail | in progress |  | |
|*MutableStyledString*| *Class* | Maksimov Nikita | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IBBYJE + |
|`replaceString`| Function | Maksimov Nikita | done |  | |
|`insertString`| Function | Maksimov Nikita | done |  | |
|`removeString`| Function | Maksimov Nikita | done |  | |
|`replaceStyle`| Function | Maksimov Nikita | done |  | |
|`setStyle`| Function | Maksimov Nikita | done |  | |
|`removeStyle`| Function | Maksimov Nikita | done |  | |
|`removeStyles`| Function | Maksimov Nikita | done |  | |
|`clearStyles`| Function | Maksimov Nikita | done |  | |
|`replaceStyledString`| Function | Maksimov Nikita | done |  | |
|`insertStyledString`| Function | Maksimov Nikita | done |  | |
|`appendStyledString`| Function | Maksimov Nikita | done |  | |
|*ImageAttachment*| *Class* | | |  | |
|`getValue`| Function | | | | |
|`getVerticalAlign`| Function | | |  | |
|`getObjectFit`| Function | | |  | |
|*CustomSpan*| *Class* | Politov Mikhail | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N |
|`onMeasure`| Function | Politov Mikhail | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N |
|`onDraw`| Function | Politov Mikhail | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N |
|`invalidate`| Function | Politov Mikhail | blocked IDL |  | https://gitee.com/nikolay-igotti/idlize/issues/IB4H0N |
|*LinearIndicatorController*| *Class* | Kovalev Sergey | done |  | depricated |
|`setProgress`| Function | Kovalev Sergey | done |  | depricated |
|`start`| Function | Kovalev Sergey | done |  | depricated |
|`pause`| Function | Kovalev Sergey | done |  | depricated |
|`stop`| Function | Kovalev Sergey | done |  | depricated |