/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { generatorConfiguration, generatorTypePrefix } from "../config"
import { asPromise, IDLType } from "../idl"
import { IdlNameConvertor } from "../LanguageWriters"
import { ArgConvertor } from "../LanguageWriters/ArgConvertors"
import { mangleMethodName, Method, MethodModifier } from "../LanguageWriters/LanguageWriter"
import { capitalize, isDefined } from "../util"
import { PrimitiveTypesInstance } from "./PrimitiveType"
import { ReferenceResolver } from "./ReferenceResolver"

export class PeerMethod {
    private overloadIndex?: number
    constructor(
        public originalParentName: string,
        public argConvertors: ArgConvertor[],
        public returnType: IDLType,
        public isCallSignature: boolean,
        public method: Method,
        public outArgConvertor?: ArgConvertor,
    ) { }

    get overloadedName(): string {
        return mangleMethodName(this.method, this.overloadIndex)
    }
    get fullMethodName(): string {
        return this.isCallSignature ? this.overloadedName : this.peerMethodName
    }
    get peerMethodName() {
        const name = this.overloadedName
        if (!this.hasReceiver()) return name
        if (name.startsWith("set") ||
            name.startsWith("get")
        ) return name
        return `set${capitalize(name)}`
    }
    get implNamespaceName(): string {
        return `${capitalize(this.originalParentName)}Modifier`
    }
    get implName(): string {
        return `${capitalize(this.overloadedName)}Impl`
    }
    get toStringName(): string {
        return this.method.name
    }
    dummyReturnValue(resolver: ReferenceResolver): string | undefined {
        return undefined
    }
    get receiverType(): string {
        return "Ark_NodeHandle"
    }
    get apiCall(): string {
        return "GetNodeModifiers()"
    }
    get apiKind(): string {
        return "Modifier"
    }
    get argAndOutConvertors(): ArgConvertor[] {
        return this.argConvertors.concat(this.outArgConvertor ?? [])
    }

    hasReceiver(): boolean {
        return !this.method.modifiers?.includes(MethodModifier.STATIC)
    }

    generateAPIParameters(converter:IdlNameConvertor): string[] {
        const args = this.argAndOutConvertors.map(it => {
            let isPointer = it.isPointerType()
            return `${isPointer ? "const ": ""}${converter.convert(it.nativeType())}${isPointer ? "*": ""} ${it.param}`
        })
        const receiver = this.generateReceiver()
        if (receiver)
            args.unshift(`${receiver.argType} ${receiver.argName}`)
        if (!!asPromise(this.method.signature.returnType))
            args.unshift(`${generatorTypePrefix()}AsyncWorkerPtr asyncWorker`)
        if (!!asPromise(this.method.signature.returnType) || this.method.modifiers?.includes(MethodModifier.THROWS))
            args.unshift(`${generatorTypePrefix()}VMContext vmContext`)
        return args
    }

    generateReceiver(): {argName: string, argType: string} | undefined {
        if (!this.hasReceiver()) return undefined
        return {
            argName: "node",
            argType: PrimitiveTypesInstance.NativePointer.getText()
        }
    }

    getImplementationName(): string {
        return this.originalParentName
    }

    static markAndGroupOverloads(methods: PeerMethod[]): PeerMethod[] {
        let groupedMethods: PeerMethod[] = []
        for (const peerMethod of methods) {
            if (isDefined(peerMethod.overloadIndex)) continue
            const sameNamedMethods = methods.filter(it => it.method.name === peerMethod.method.name)
            if (sameNamedMethods.length > 1)
                sameNamedMethods.forEach((it, index) => it.overloadIndex = index)
            groupedMethods = groupedMethods.concat(sameNamedMethods)
        }
        return groupedMethods
    }

    setSameOverloadIndex(copyFrom: PeerMethod) {
        this.overloadIndex = copyFrom.overloadIndex
    }
}
