declare type HelloType = number | boolean

declare interface Hello {
  hello(value: HelloType): void
}
