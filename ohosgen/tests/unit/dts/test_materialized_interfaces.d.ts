export interface InterfaceWithMethods {

    propBoolean: boolean
    propNumber: number

    isUsed(value: number): boolean
}
