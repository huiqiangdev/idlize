import { init, runEventLoop } from "./compat"
import { run } from "../app"

init()
runEventLoop()
run()