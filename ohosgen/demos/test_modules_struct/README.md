# Usage

```
npm run generate:arkts
npm run compile:arkts  # ERROR message see below
```

# Current behavior

Part of error output:

```
Fatal error: Failed to compile from ...idlize/ohosgen/demos/test_modules_struct/generated/arkts/index.ts to ...idlize/ohosgen/demos/test_modules_struct/build/panda/out/generated/arkts/index.abc
Warning: Property 'fooA' might not have been initialized. [OHBarInterfaces.ts:21:5]
TypeError: There were errors during assign analysis (2) [OHBarInterfaces.ts:22:5]
Warning: Property 'fooB' might not have been initialized. [OHBarInterfaces.ts:22:5]
```
