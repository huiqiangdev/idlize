export interface Callback<T> {
    (x: T): void;
}
