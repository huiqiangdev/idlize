import { int32 } from "@koalaui/common"
import { KPointer, KInt, KStringPtr, pointer, nullptr, RuntimeType, runtimeType, NativeBuffer } from "@koalaui/interop"
import { Serializer } from "%SERIALIZER_PATH%"
import { Finalizable } from "%FINALIZABLE_PATH%"

%PEER_IMPORTS%

%PEER_CONTENT%
