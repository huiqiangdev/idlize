@memo
@BuildLambda("%COMPONENT_NAME%")
export function %COMPONENT_NAME%(
    %FUNCTION_PARAMETERS%
    @memo
    content?: () => void,
): %COMPONENT_CLASS_NAME% {
    throw new Error("This function should only be called through a BuilderLambda redirect")
}

@memo
export function %COMPONENT_NAME%Impl(
    @memo
    style: ((attributes: %COMPONENT_CLASS_NAME%) => void) | undefined,
    %FUNCTION_PARAMETERS%
    @memo
    content?: () => void,
): void {
    const receiver = remember(() => {
        return new %COMPONENT_CLASS_NAME%()
    })
    NodeAttach<%PEER_CLASS_NAME%>((): %PEER_CLASS_NAME% => %PEER_CLASS_NAME%.create(receiver), (_: %PEER_CLASS_NAME%) => {
        %PEER_CALLABLE_INVOKE%
        style?.(receiver)
        content?.()
        receiver.applyAttributesFinish()
    })
}
