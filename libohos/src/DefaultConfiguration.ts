
import * as fs from "fs"
import * as path from "path"
import {
    CoreConfiguration,
    defaultCoreConfuguration,
    generatorConfiguration,
    isDefined,
    Language,
} from "@idlizer/core";
import { deepMergeConfig } from "./configMerge";

export interface PeerGeneratorConfiguration extends CoreConfiguration {
    readonly GenerateUnused: boolean
    readonly ApiVersion: number
    readonly dumpSerialized: boolean
    readonly boundProperties: Map<string, string[]>

    readonly cppPrefix: string
    readonly components: {
        readonly ignoreComponents: string[],
        readonly ignorePeerMethod: string[],
        readonly invalidAttributes: string[],
        readonly customNodeTypes: string[],
        readonly ignoreEntry: string[],
        readonly ignoreEntryJava: string[],
        readonly ignoreMethodArkts: string[],
        readonly custom: string[],
        readonly handWritten: string[],
        readonly replaceThrowErrorReturn: string[],
    }
    readonly dummy: {
        readonly ignoreMethods: Map<string, string[]>
    }
    readonly materialized: {
        readonly ignoreReturnTypes: string[]
    }
    readonly serializer: {
        readonly ignore: string[]
    }
    readonly constants: Map<string, string>
    readonly patchMaterialized: Map<string, Record<string, string>>
    readonly CollapseOverloadsARKTS: boolean

    mapComponentName(originalName: string): string
    ignoreEntry(name: string, language: Language): boolean
    ignoreMethod(name: string, language: Language) : boolean
    isHandWritten(component: string): boolean
    isKnownParametrized(name: string | undefined): boolean
    isShouldReplaceThrowingError(name: string) : boolean
    noDummyGeneration(component: string, method?: string) : boolean
}

export const defaultPeerGeneratorConfiguration: PeerGeneratorConfiguration = {
    ...defaultCoreConfuguration,
    TypePrefix: "Ark_",
    LibraryPrefix: "",
    OptionalPrefix: "Opt_",
    GenerateUnused: false,
    ApiVersion: 9999,
    dumpSerialized: false,
    boundProperties: new Map(),
    cppPrefix: '',
    serializer: {
        ignore: [],
    },
    materialized: {
        ignoreReturnTypes: [],
    },
    components: {
        ignoreComponents: [],
        ignorePeerMethod: [],
        invalidAttributes: [],
        customNodeTypes: [],
        ignoreEntry: [],
        ignoreEntryJava: [],
        ignoreMethodArkts: [],
        custom: [],
        handWritten: [],
        replaceThrowErrorReturn: []
    },
    dummy: {
        ignoreMethods: new Map(),
    },
    constants: new Map(),
    patchMaterialized: new Map(),
    CollapseOverloadsARKTS: true,
    mapComponentName(originalName: string): string {
        if (originalName.endsWith("Attribute"))
            return originalName.substring(0, originalName.length - 9)
        return originalName
    },
    ignoreEntry(name: string, language: Language): boolean {
        return this.components.ignoreEntry.includes(name) ||
            language === Language.JAVA && this.components.ignoreEntryJava.concat(this.components.custom).includes(name)
    },
    ignoreMethod(name: string, language: Language): boolean {
        return language === Language.ARKTS && this.components.ignoreMethodArkts.includes(name)
    },
    isHandWritten(component: string): boolean {
        return this.components.handWritten.concat(this.components.custom).includes(component)
    },
    isKnownParametrized(name: string | undefined): boolean {
        return name != undefined && this.parameterized.includes(name)
    },
    isShouldReplaceThrowingError(name: string): boolean {
        for (const ignore of this.components.replaceThrowErrorReturn) {
            if (name.endsWith(ignore)) return true
        }
        return false
    },
    noDummyGeneration(component: string, method?: string): boolean {
        const ignoreMethods = this.dummy.ignoreMethods.get(component)
        if (!isDefined(ignoreMethods)) return false
        if (isWhole(ignoreMethods)) return true
        if (method && ignoreMethods.includes(method)) return true
        return false
    },
}

function isWhole(methods: string[]): boolean {
    return methods.includes("*")
}

function parseConfigFile(configurationFile: string): any {
    if (!fs.existsSync(configurationFile)) return undefined

    const data = fs.readFileSync(path.resolve(configurationFile)).toString()
    return JSON.parse(data)
}

export function readConfigFiles(configurationFiles?: string, overrideConfigurationFiles?: string): [string, unknown][] {
    let files = [path.join(__dirname, "..", "generation-config", "config.json")]
    if (configurationFiles) files.push(...configurationFiles.split(","))
    if (overrideConfigurationFiles) {
        files = overrideConfigurationFiles.split(",")
    }

    return files.map(file => [file, parseConfigFile(file)])
}

export function parseConfigFiles<T extends object>(defaultConfiguration: T, configurationFiles?: string, overrideConfigurationFiles?: string): T {
    const files = readConfigFiles(configurationFiles, overrideConfigurationFiles)

    let result: T = defaultConfiguration
    files.forEach(([file, nextConfiguration]) => {
        if (nextConfiguration) {
            console.log(`Using options from ${file}`)
            result = deepMergeConfig<T>(result, nextConfiguration)
        } else {
            throw new Error(`file ${file} does not exist or cannot parse`)
        }
    })

    return result
}

export function loadPeerConfiguration(configurationFiles?: string, overrideConfigurationFiles?: string): PeerGeneratorConfiguration {
    return parseConfigFiles(defaultPeerGeneratorConfiguration, configurationFiles, overrideConfigurationFiles)
}

export function peerGeneratorConfiguration(): PeerGeneratorConfiguration {
    return generatorConfiguration<PeerGeneratorConfiguration>()
}