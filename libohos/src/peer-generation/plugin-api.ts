/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { PeerLibrary } from "@idlizer/core";

export interface PluginHost {
    log(message: string): void
}

export type PluginOptions = any

class PluginHostImpl implements PluginHost {
    log(message: string): void {
        console.log(`PLUGIN: ${message}`)
    }
}

export interface Plugin {
    process(options: PluginOptions, idl: PeerLibrary): void
}

export async function loadPlugin(path: string): Promise<Plugin> {
    let host = new PluginHostImpl()
    return import(path)
        .then(plugin => plugin.default(host))
}